### 项目命名约定

> PS:java语言的命名规范不在本文档的说明范围内。

#### maven groupId、artifactId命名
groupId: *com.gsafety.xsystem*  
artifactId: *xsystem-[modulename]-[featurename]*，如：*xsystem-duty-services,xsystem-dutyconfig-webapi*

#### java工程命名
不管使用idea还是eclipse，工程的名称应与artifactId一致。如果artifactId的名称为*xsystem-duty-services*，那么java工程名也应该为
*xsystem-duty-services*

#### jar文件命名
按照maven的约定，构件的名称规则是：*artifactId-version[-classifier].packaging*，如：achilles.duty-web-1.0.0-RELEASES.jar。注意：
如果使用gradle maven插件,我们可以不需要手工指定jar文件名，构件上传时会自动按照这个约定命名，所以我们需要严格遵守groupId、artifactId的命名。

#### java package名字空间的命名
1. // 数据实体：*com.gsafety.xsystem.[modulename].services.entity* （未确定）
*  // DTO：*com.gsafety.xsystem.[modulename].contract.model* （未确定）
* 数据访问：*com.gsafety.xsystem.[modulename].services.reposiotory*
* 服务接口：*com.gsafety.xsystem.[modulename].contract.service*
* 控制器：*com.gsafety.xsystem.[modulename].webapi.controller*