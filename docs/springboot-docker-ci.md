# Spring Boot基于Docker的自动化部署  

* [1、持续集成结构示意图]()
* [2、Spring Boot工程搭建](#2Spring Boot工程搭建)  
	- [2.1 种子工程clone](#21-种子工程clone)
	- [2.2 工程名及包名修改](#22-工程名及包名修改)
	- [2.3 Dockerfile修改](#23-Dockerfile修改)
	- [2.4 docker.gradle说明](#24-docker.gradle说明)
* [3、Jenkins项目及任务配置](#3Jenkins项目及任务配置)
	- [3.1 任务配置](#31-任务配置)
	- [3.2 Gitlab Hook配置](#32-Gitlab Hook配置)
	- [3.3 自动构建及推送镜像](#33-自动构建及推送镜像)
* [4、Rancher项目发布](#4Rancher项目发布)
	- [4.1 项目及命名空间建立](#41-项目及命名空间建立)
	- [4.2 服务发布](#42-服务发布) 
	- [4.3 统一配置管理-ConfigMap](#43-统一配置管理-ConfigMap)
	
## 1、持续集成结构示意图      
![newjob](./images/ci.png)   

## 2、Spring Boot工程搭建  

### 2.1 种子工程clone    
* 种子工程地址: `http://172.22.3.103/vNextDevTechs/school-manager-workspace`       
* 阅读种子工程相关介绍及约定

### 2.2 工程名及包名修改 
* 使用分支：dev-2.0 
* 参见种子工程首页的MD文件中`八、种子工程应用`进行业务工程名及包名配置    


### 2.3 Dockerfile修改    
Dockerfile中的`school-manager-backend-2.0.0.jar`、`boot-seed.jar`按实际项目的jar名称进行修改即可，前者是开发编译出的`jar`名称,后者是容器内运行的`jar`名称 

```  shell  

# 基础镜像
FROM 172.22.3.109/library/openjdk:8-jre-alpine

# VOLUME命令用于让你的容器访问宿主机上的目录。
VOLUME /tmp

# ADD命令有两个参数，源和目标。它的基本作用是从源系统的文件系统上复制文件到目标容器的文件系统。如果源是一个URL，那该URL的内容将被下载并复制到容器中。
ADD school-manager-backend-2.0.0.jar boot-seed.jar


# RUN命令是Dockerfile执行命令的核心部分。它接受命令作为参数并用于创建镜像。
RUN sh -c 'touch /boot-seed.jar'

# ENV命令用于设置环境变量。这些变量以”key=value”的形式存在，并可以在容器内被脚本或者程序调用。
ENV JAVA_OPTS=""

# ENTRYPOINT 帮助你配置一个容器使之可执行化
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /boot-seed.jar" ]  
```  

### 2.4 docker.gradle说明  
docker.gradle是种子工程内新增的构建脚本，主要用来进行远端docker镜像构建及发布。  
这是gradle构建docker镜像的核心脚本，基本不需要修改(如果远端docker环境变更、docker镜像仓库变更才需要修改)。 
如果是非clone的种子工程，即如果其他特殊项目也需要集成docker构建，将此文件脚本脚本拷入后引入即可。
```  java
  buildscript {
    repositories {
        maven {
            url 'http://172.22.3.5/repository/maven-public/'
        }
    }
    dependencies {
        classpath("com.bmuschko:gradle-docker-plugin:3.0.3")
    }
}

apply plugin: com.bmuschko.gradle.docker.DockerRemoteApiPlugin

// docker远端配置信息
docker {
    // docker远端构建进程配置地址
    url = 'http://172.22.24.202:3557/'
    // docker发布的仓库地址及认证信息
    registryCredentials {
        url = 'http://172.22.3.109'
        username = 'admin'
        password = 'Admin123'
        email = 'admin@example.com'
    }
}

import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage
import com.bmuschko.gradle.docker.tasks.image.Dockerfile

// 拷贝jar包到docker发布目录
task copyJarToDockerFD(type: Copy) {
    dependsOn ":$jarProjectName:build"
    from "$jarProjectName/build/libs/$jarArchivesBaseName" + "-" + project.version + ".jar"
    into "build/docker/"
}

// 拷贝dockerfile到docker发布目录
task copyDockerFile(type: Copy) {
    from './Dockerfile'
    into 'build/docker/'
}

// 构建镜像
task buildImage(type: DockerBuildImage) {
    dependsOn copyDockerFile, copyJarToDockerFD
    group = 'Docker'
    inputDir = project.file('build/docker/')
    tag = "172.22.3.109/pdd/$jarProjectName:" + project.version
}

// 发布镜像
task pushImage(type: DockerPushImage) {
    group = 'Docker'
    dependsOn buildImage
    conventionMapping.imageName = { buildImage.getTag() }
}
```

## 3、Jenkins项目及任务配置     

### 3.1 任务配置
Jenkins上每个大型项目都有一个对应的视图，如`AG01/AG02/PDD`,比如目前产品部相关任务放在`PDD`视图内.   
* 新建一个`Job`:  
![newjob](./images/jks-new.png)   

* 任务配置:   
![newjob](./images/jks-job-cfg1.png)     
![newjob](./images/jks-job-cfg2.png)   
![newjob](./images/jks-job-cfg3.png)     

### 3.2 Gitlab Hook配置
* 上方Jekins任务配置时有一个触发器的回调url:  
![newjob](./images/jks-job-hookurl.png)    

* 将此`url copy`后, 打开`GitLab`项目，在图示`Webhooks Url`内填入`Jenkins`配置内刚拷贝的`url`:
![newjob](./images/gitlab-hook.png)      


### 3.3 自动构建及推送镜像
项目内文件修改后，当执行push成功后，jenkins会自动进行构建，打开harbor镜像仓库，即可看到成功的镜像文件：  
![newjob](./images/harbor-images-sus.png)      


## 4、Rancher项目发布  
`Rancher`可理解为是`Kubernetes`（容器编排）的运维管理工具。    

### 4.1 项目及命名空间建立  
如果是一个新的项目，我们需要在Rancher内建立项目名称及名称空间，便于不同项目的管理。  
如图，我们建立了`PDD_GAuth`项目，并在项目下建立了`pdd-gauth`名称空间：  
![newjob](./images/rancher-nm.png)       


### 4.2 服务发布  
站点左侧下拉框，选择对应的项目名称 ，如这里选择`pdd-lab1`, 然后点击右侧`部署服务`开始发布服务：  
![newjob](./images/rancher-new-s.png)     
进入具体服务发布界面，依次填写：    
* 服务名称  
* 工作负载类型  
* Docker镜像名称  
* 对应的名称空间
![newjob](./images/rancher-new-s2.png)    
提交后即完成一个服务的简单发布  

### 4.3 统一配置管理-ConfigMap    
如果项目的配置信息需要由Kubernetes的配置中心进行管理，需要进行配置管理，以及配置挂载  
* 新增配置    
选择`资源 => 配置映射`    
填写对应的配置信息：  
![newjob](./images/configmap-1.png)     

* 挂载配置到服务  
这一步可以在上面服务发布的时候进行配置即可（即：正常情况4.3的步骤应该在4.2之前完成）  
![s-config](./images/rancher-new-s2.png)  
![newjob](./images/configmap-2.png)   




  







