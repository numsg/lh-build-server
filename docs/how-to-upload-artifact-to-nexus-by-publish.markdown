### 发布jar包到Nexus

要将我们自己的编写的组件，发布到Nexus上提供给被人使用，有几种方式。
1. 使用Nexus的管理界面直接上传。
* 使用maven命令生成pom，再使用命令上传到Nexus。
* 利用构建工具自动上传至nexus。

这里我们只介绍如何使用gradle上传构件（jar）包到Nexus。

``` gradle
// build.gradle文件

apply plugin:'java'      // 使用java插件
//apply plugin:'maven'     // 使用maven插件，用于上传构件（jar包）
apply plugin: 'maven-publish'

// 定义groupId和version，artefactId会自动使用工程名。
// 注意：如果版本号为*.*.*-RELEASES将会自动上传至releases库,如果版本号为*.*.*-SNAPSHOT将会自动上传至snapshot库。
group = 'com.gsafety.xseed.xsystem'
version = '1.0.0-SNAPSHOT'

task sourceJar(type: Jar) {
    from sourceSets.main.allJava
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            from components.java

            artifact sourceJar {
                classifier "sources"
            }
        }
    }
    repositories {
        maven {
            if (project.version.endsWith("SNAPSHOT")) {
                url "http://172.22.24.51:8081/nexus/content/repositories/snapshots/"
            } else {
                url "http://172.22.24.51:8081/nexus/content/repositories/Releases/"
            }
            credentials {
                username 'deployment'
                password '123456'
            }
        }
    }
}
```

