package com.lh.server.common.constant;

public class LogMsg {
    public static final String SUCCESS = "success";
    public static final String MESSAGE = "message";
    public static final String ERROR_CODE = "errorCode";
    public static final String PARAM_IS_EMPTY = "param is empty";
    public static final String SERVER_INNER_EXCEPTION = "server inner exception";
    public static final String FIND_BY_ID_IS_EMPTY = "find by id is empty";
    public static final String TITLE_EXIST = "title exist";
    public static final String NAME_EXIST = "name exist";
    public static final String USERNAME_NOT_EXIST = "username does not exist";
    public static final String RESULT_DATA = "resultData";

    private LogMsg() {
    }
}
