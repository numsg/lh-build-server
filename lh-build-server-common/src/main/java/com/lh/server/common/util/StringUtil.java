package com.lh.server.common.util;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class StringUtil {

    private StringUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 判断字符串是否为空,为空则返回true,不为空则返回false
     *
     * @param str 任意字符串
     * @return 是否为空
     */
    public static boolean isEmpty(String str) {
        return StringUtils.isBlank(str);
    }

    /**
     * 判断字符串是否为空,不为空则返回true,为空则返回false
     *
     * @param str 任意字符串
     * @return 是否不为空
     */
    public static boolean isNotEmpty(String str) {
        return StringUtils.isNotBlank(str);
    }

    /**
     * 将字符串的首字母改为小写
     *
     * @param str 任意字符串
     * @return 首字母改为小写后的字符串
     */
    public static String unCapitalize(String str) {
        return StringUtils.uncapitalize(str);
    }

    /**
     * 判断字符串是否是数值类型
     *
     * @param str 任意字符串
     * @return 是否是数值
     */
    public static boolean isNumber(String str) {
        return NumberUtils.isNumber(str);
    }
}
