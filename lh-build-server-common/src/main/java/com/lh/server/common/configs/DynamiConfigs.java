package com.lh.server.common.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


@Component
public class DynamiConfigs implements EnvironmentPostProcessor {

    Logger logger = LoggerFactory.getLogger(DynamiConfigs.class);

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        try {
            InputStream is = new FileInputStream("application");
            Properties pro = new Properties();
            pro.load(is);
            PropertiesPropertySource propertySource = new PropertiesPropertySource("dynamic", pro);
            environment.getPropertySources().addLast(propertySource);
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
    }
}
