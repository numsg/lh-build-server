package com.lh.server.common.annotations;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ResponseEntity HttpHeader自定义
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface HttpHeaderAnnotation {
    String contentType() default "application/json;charset=UTF-8";

    String date() default "2020-09-10 00:00:00";

    String transferEncoding() default "chunked";
}
