package com.lh.server.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {
    private DateUtil() {
        throw new IllegalStateException("Utility class");
    }
    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);
    private  static  final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 获取当前格式化时间字符串yyyy-MM-dd HH:mm:ss
     *
     * @return now date string
     */
    public static String getNowDateString()
    {
        // DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return  df.format(new Date());
    }

    /**
     * Convert now date timestamp.
     *
     * @return the timestamp
     */
    public static Timestamp convertNowDate(){
        //  DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String dateNow = df.format(calendar.getTime());
        return Timestamp.valueOf(dateNow);
    }


    /**
     * Gets start time.
     *
     * @return the start time
     */
    public static Timestamp getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);

        //  DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(todayStart.getTime());
        return   Timestamp.valueOf(format);
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public static Timestamp getEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);

        //   DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(todayEnd.getTime());
        return   Timestamp.valueOf(format);
    }


    /**
     * Gets start time date.
     *
     * @return the start time date
     */
    public static Date getStartTimeDate() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * Gets end time date.
     *
     * @return the end time date
     */
    public static Date getEndTimeDate() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * Get day start date date.
     *
     * @return the date
     */
    public  static Date getDayStartDate(){
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH),
                0, 0, 0);
        return calendar1.getTime();
    }

    /**
     * Get day end date date.
     *
     * @return the date
     */
    public  static Date getDayEndDate(){
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DAY_OF_MONTH),
                23, 59, 59);
        return calendar1.getTime();
    }

    /**
     * String format date.
     *
     * @param date the date
     * @return the date
     */
    public static Date stringFormat(String date){
        Date result=null;
        try {
            result = df.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Date format date.
     *
     * @param date the date
     * @return the date
     */
    public static Date dateFormat(Date date){
        Date result=null;
        try {
            result = df.parse(df.format(date));
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    public static String dateFormatToStr(Date date){
        String result=null;
        if (date !=null){
            result = df.format(date);
        }
        return result;
    }
}
