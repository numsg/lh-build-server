package com.lh.server.controller;

import com.lh.server.contract.service.LoginService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.testng.Assert.assertEquals;

/**
 * Created by xiaodiming on 2017/3/7.
 */
public class AppUserControllerTest {

    @Mock
    private LoginService userService;

    @InjectMocks
    private SchoolLoginController appUserController;

    @BeforeMethod
    private void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterMethod
    private void afterMethod() {
        reset(userService);
    }

    @Test
    public void testCreate() {

    }

    @Test
    public void testFindAll() {


    }
}