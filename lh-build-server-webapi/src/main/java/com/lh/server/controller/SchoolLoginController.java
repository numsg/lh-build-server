package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolLoginInfo;
import com.lh.server.contract.service.LoginService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-用户管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLoginController {
    /**
     * The User service.
     */
    @Autowired
    LoginService loginService;

    /**
     * 通过用户名密码登录
     *
     * @param user 用户
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "通过用户名密码登录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/login")
    public ResponseEntity<Map<String, Object>> login(@RequestBody SchoolLoginInfo user) {
        return ResponseEntity.status(HttpStatus.OK).body(loginService.login(user));
    }

    /**
     * 找到所有登录用户信息
     *
     * @return {@link ResponseEntity<List< SchoolLoginInfo >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "获取所有用户")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @GetMapping(value = "/users")
    public ResponseEntity<List<SchoolLoginInfo>> findAll() {
        return new ResponseEntity<>(loginService.findAll(), HttpStatus.OK);
    }

}
