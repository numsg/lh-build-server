package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolRoleInfo;
import com.lh.server.contract.model.SchoolUserRoleRelationInfo;
import com.lh.server.contract.service.SchoolRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-角色管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolRoleAuthorityController {
    /**
     * The Role service.
     */
    @Autowired
    SchoolRoleService roleService;

    /**
     * 获取sigt角色信息
     *
     * @return roles structures
     */
    @ApiOperation(value = "获取角色信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolRoleInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/roles")
    public ResponseEntity<List<SchoolRoleInfo>> getStructures() {
        List<SchoolRoleInfo> result = roleService.getAllRoles();
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * 配置用户角色
     *
     * @param userRoleRelations the user role relations
     * @return response entity
     */
    @ApiOperation(value = "配置用户角色", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "添加成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/configRoles")
    public ResponseEntity<Boolean> configUserRoles(@RequestBody List<SchoolUserRoleRelationInfo> userRoleRelations) {
        Boolean result = roleService.configUserRoles(userRoleRelations);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * 添加角色
     *
     * @param role the role
     * @return response entity
     */
    @ApiOperation(value = "添加角色", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "添加成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/roles")
    public ResponseEntity<Map<String, Object>> addRole(@RequestBody SchoolRoleInfo role) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.addRole(role));
    }

    /**
     * 修改角色
     *
     * @param role the role
     * @return response entity
     */
    @ApiOperation(value = "修改角色", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "修改成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping(value = "/roles")
    public ResponseEntity<Map<String, Object>> updateRole(@RequestBody SchoolRoleInfo role) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.updateRole(role));
    }

    /**
     * 删除角色
     *
     * @param ids the ids
     * @return response entity
     */
    @ApiOperation(value = "删除角色", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/roles/multiDelete")
    public ResponseEntity<Boolean> deleteRole(@RequestBody String[] ids) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.deleteRole(ids));
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色集合 roles by user id
     */
    @ApiOperation(value = "根据用户ID查询角色", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolRoleInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/getRolesByUserId/{userId}")
    public ResponseEntity<List<SchoolRoleInfo>> getRolesByUserId(@PathVariable("userId") String userId) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.getRolesByUserId(userId));
    }

    /**
     * 根据角色ID查询用户角色关联关系
     *
     * @param roleId 角色id
     * @return user role relation by role id
     */
    @ApiOperation(value = "根据角色ID查询用户角色关联关系", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/getUserRoleRelationByRoleId/{roleId}")
    public ResponseEntity<Boolean> getUserRoleRelationByRoleId(@PathVariable String roleId) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.getUserRoleRelationByRoleId(roleId));
    }
}
