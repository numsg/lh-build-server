package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolSettingInfo;
import com.lh.server.contract.model.SchoolSettingTypeInfo;
import com.lh.server.contract.service.SchoolSettingService;
import com.lh.server.common.util.CheckUtils;
import com.lh.server.common.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-配置管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolSettingController {
    /**
     * 系统设置服务
     */
    @Autowired
    private SchoolSettingService settingService;

    /**
     * Find by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @ApiOperation(value = "根据ID查找配置项", notes = "findById", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/settings/{settingId}")
    public ResponseEntity<SchoolSettingInfo> findById(@PathVariable("settingId") String id) {
        SchoolSettingInfo result = settingService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Find all response entity.
     *
     * @return the response entity
     */
    @ApiOperation(value = "查找所有配置项", notes = "findAll", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings")
    public ResponseEntity<List<SchoolSettingInfo>> findAll() {
        List<SchoolSettingInfo> result = settingService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Save response entity.
     *
     * @param settingInfo   the setting info
     * @param bindingResult the binding result
     * @return the response entity
     */
    @ApiOperation(value = "保存配置项", notes = "save", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "保存成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping("/settings")
    public ResponseEntity<Map<String, Object>> save(@RequestBody @Valid SchoolSettingInfo settingInfo, BindingResult bindingResult) {
        // 参数校验
        CheckUtils.checkBindingResult(bindingResult);

        return ResponseEntity.status(HttpStatus.CREATED).body(settingService.save(settingInfo));
    }

    /**
     * Update response entity.
     *
     * @param id            the id
     * @param settingInfo   the setting info
     * @param bindingResult the binding result
     * @return the response entity
     */
    @ApiOperation(value = "更新配置项", notes = "update", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功", response = SchoolSettingInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping("/settings/{settingId}")
    public ResponseEntity<Map<String, Object>> update(@PathVariable("settingId") String id,
                                                      @RequestBody @Valid SchoolSettingInfo settingInfo,
                                                      BindingResult bindingResult) {
        // 参数校验
        CheckUtils.checkBindingResult(bindingResult);

        settingInfo.setSettingId(id);
        return ResponseEntity.status(HttpStatus.OK).body(settingService.update(settingInfo));
    }

    /**
     * Delete by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @ApiOperation(value = "根据ID删除配置项", notes = "deleteById", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 204, message = "删除成功", response = boolean.class),
            @ApiResponse(code = 400, message = "参数非法,指定ID的配置项不存在", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping("/settings/{settingId}")
    public ResponseEntity<Boolean> deleteById(@PathVariable("settingId") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(settingService.deleteById(id));
    }

    /**
     * Find by setting name response entity.
     *
     * @param settingName the setting name
     * @return the response entity
     */
    @ApiOperation(value = "根据配置项名称查询配置项", notes = "findBySettingQueryInfo",
            produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings/name/{settingName}")
    public ResponseEntity<List<SchoolSettingInfo>> findBySettingName(@PathVariable String settingName) {
        List<SchoolSettingInfo> result = settingService.findBySettingName(settingName);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Find enable by param type response entity.
     *
     * @param paramType the param type
     * @return the response entity
     */
    @ApiOperation(value = "根据参数类型查找已启用配置项——供外部系统调用", notes = "findEnableByParamType",
            produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings/param-type/{paramType}")
    public ResponseEntity<List<SchoolSettingInfo>> findEnableByParamType(@PathVariable Integer paramType) {
        List<SchoolSettingInfo> result = settingService.findEnableByParamType(paramType);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Find by param type response entity.
     *
     * @param paramType the param type
     * @return the response entity
     */
    @ApiOperation(value = "根据参数类型查找配置项", notes = "findByParamType",
            produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings/param-type/findByParamType/{paramType}")
    public ResponseEntity<List<SchoolSettingInfo>> findByParamType(@PathVariable Integer paramType) {
        List<SchoolSettingInfo> result = settingService.findByParamType(paramType);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Find obj by param type response entity.
     *
     * @param paramType the param type
     * @return the response entity
     */
    @ApiOperation(value = "根据参数类型查找配置项(对象格式)", notes = "findObjByParamType",
            produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings/param-type/{paramType}/obj")
    public ResponseEntity<Map<String, String>> findObjByParamType(@PathVariable Integer paramType) {
        Map<String, String> obj = new HashMap<>();
        List<SchoolSettingInfo> result = settingService.findByParamType(paramType);
        result.forEach(settingInfo -> {
            String val = settingInfo.getValue();
            if (StringUtil.isEmpty(val)) {
                val = settingInfo.getDefaultValue();
            }
            obj.put(settingInfo.getName(), val);
        });
        return ResponseEntity.status(HttpStatus.OK).body(obj);
    }

    /**
     * Find by param types response entity.
     *
     * @param paramTypes the param types
     * @return the response entity
     */
    @ApiOperation(value = "根据参数类型集合批量查找配置项", notes = "findByParamTypes",
            produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "找不到任何配置项", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping("/settings/param-type/batch")
    public ResponseEntity<List<SchoolSettingInfo>> findByParamTypes(@RequestBody List<Integer> paramTypes) {
        List<SchoolSettingInfo> result = settingService.findByParamTypes(paramTypes);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Find all setting type response entity.
     *
     * @return the response entity
     */
    @ApiOperation(value = "查询所有设置的type名称", notes = "findByParamTypes", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功", response = SchoolSettingTypeInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping("/settings/types")
    public ResponseEntity<List<SchoolSettingTypeInfo>> findAllSettingType() {
        return ResponseEntity.status(HttpStatus.OK).body(settingService.findAllSettingType());
    }

    /**
     * Save setting type response entity.
     *
     * @param settingTypeInfo the setting type info
     * @param bindingResult   the binding result
     * @return the response entity
     */
    @ApiOperation(value = "保存设置的type", notes = "saveSettingType", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 201, message = "保存成功", response = SchoolSettingTypeInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping("/settings/types")
    public ResponseEntity<Map<String, Object>> saveSettingType(@RequestBody @Valid SchoolSettingTypeInfo settingTypeInfo,
                                                               BindingResult bindingResult) {
        // 参数校验
        CheckUtils.checkBindingResult(bindingResult);
        return ResponseEntity.status(HttpStatus.CREATED).body(settingService.saveSettingType(settingTypeInfo));
    }

    /**
     * Delete type by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @ApiOperation(value = "根据ID删除配置类型", notes = "deleteById", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除成功", response = boolean.class),
            @ApiResponse(code = 400, message = "参数非法,指定ID的配置项不存在", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping("/settings/types/{typeId}")
    public ResponseEntity<Boolean> deleteTypeById(@PathVariable("typeId") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(settingService.deleteTypeById(id));
    }
}
