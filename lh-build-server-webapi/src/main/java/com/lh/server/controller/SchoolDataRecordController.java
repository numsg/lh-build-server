package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolDataRecordInfo;
import com.lh.server.contract.service.SchoolDataRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-数据记录管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolDataRecordController {

    @Autowired
    private SchoolDataRecordService schoolDataRecordService;

    /**
     * 获取某个设备对应最新的sno
     *
     * @return response   最新的sno
     */
    @ApiOperation(value = "获取某个设备对应最新的sno", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/datarecord/{deviceid}")
    public ResponseEntity<String> getByDataType(@PathVariable String deviceid) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolDataRecordService.getByDeviceId(deviceid));
    }

    /**
     * 第三方系统推送记录数据到本系统
     *
     * @return  response   是否成功
     */
    @ApiOperation(value = "第三方系统推送记录数据到本系统", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/datarecord/sendrecord")
    public ResponseEntity<Boolean> sendRecord(@RequestBody SchoolDataRecordInfo dataRecordInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolDataRecordService.save(dataRecordInfo));
    }


    /**
     * 查询所有数据记录
     *
     * @return  response   是否成功
     */
    @ApiOperation(value = "查询所有数据记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/datarecord/")
    public ResponseEntity<List<SchoolDataRecordInfo>> getAllSchoolDataRecordInfo() {
        return ResponseEntity.status(HttpStatus.OK).body(schoolDataRecordService.getAllSchoolDataRecordInfo());
    }

    /**
     * 分页获取当前日期数据记录
     *
     * @return  response   是否成功
     */
    @ApiOperation(value = "分页获取当前日期数据记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/datarecord/{datatype}/page")
    public ResponseEntity<PageInfo<SchoolDataRecordInfo>> getLatestSchoolDataRecordInfoList(@PathVariable int datatype,@RequestParam  int pageNum , @RequestParam int pageSize, @RequestParam String deviceId) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolDataRecordService.getLatestSchoolDataRecordInfoList(datatype,pageNum,pageSize,deviceId));
    }

    /**
     * 批量修改数据记录
     *
     * @return  response   是否成功
     */
    @ApiOperation(value = "批量修改数据记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/datarecord/")
    public ResponseEntity<Boolean> updateDataRecords(@RequestBody List<SchoolDataRecordInfo> schoolDataRecordInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolDataRecordService.updateDataRecords(schoolDataRecordInfo));
    }
}
