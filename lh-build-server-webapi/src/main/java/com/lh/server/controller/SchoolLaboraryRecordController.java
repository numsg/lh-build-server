package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.*;
import com.lh.server.contract.service.SchoolLaboraryRecordService;
import com.lh.server.contract.service.SchoolLaboraryResultTemplateService;
import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * The type School laborary record controller.
 */
@Api(value = "/api/v1", tags = {"实验过程-记录管理-实验记录列表"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboraryRecordController {
    @Autowired
    private SchoolLaboraryRecordService recordService;

    /**
     * Gets all records.
     *
     * @return the all records
     */
    @ApiOperation(value = "分页查询实验记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/records/page")
    public ResponseEntity<PageQueryResultInfo<SchoolLaboraryRecordInfo>> pageQueryRecords(@RequestBody @ApiParam(value = "分页查询model，页码(pageNumber)从1开始", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.pageQueryRecords(statusConditionQueryInfo, "part"));
    }

    /**
     * Gets all records.
     *
     * @return the all records
     */
    @ApiOperation(value = "分页查询实验变更记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/records/pagechange")
    public ResponseEntity<PageQueryResultInfo<SchoolLaboraryRecordInfo>> pageChangeQueryRecords(@RequestBody @ApiParam(value = "分页查询model，页码(pageNumber)从1开始", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.pageQueryRecords(statusConditionQueryInfo, "all"));
    }

    /**
     * Gets one record by id.
     *
     * @param id the id
     *u @return the one record by id
     */
    @ApiOperation(value = "根据id查询实验记录及其项目详情", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/records/one/{id}")
    public ResponseEntity<SchoolLaboraryRecordInfo> getOneRecordById(@PathVariable @ApiParam(value = "id", required = true) String id) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.getOneRecordById(id));
    }

    /**
     * Judge is repeat when add response entity.
     *
     * @param centerCode  the center code
     * @param projectName the project name
     * @return the response entity
     */
    @ApiOperation(value = "当新增时判断记录是否重复", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/records/is-repeat/{centerCode}/{projectName}")
    public ResponseEntity<Boolean> judgeIsRepeatWhenAdd(@PathVariable @ApiParam(value = "中心编号", required = true) String centerCode,
                                                        @PathVariable @ApiParam(value = "实验项目名称", required = true) String projectName) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.judgeIsRepeatWhenAdd(centerCode, projectName));
    }

    /**
     * Judge is repeat when add response entity.
     *
     * @param recordId  the center code
     * @param projectName the project name
     * @return the response entity
     */
    @ApiOperation(value = "根据recordid当新增时判断记录是否重复", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/records/is-repeat-record/{recordId}/{projectName}")
    public ResponseEntity<Boolean> judgeIsRepeatRecordWhenAdd(@PathVariable @ApiParam(value = "recordid", required = true) String recordId,
                                                        @PathVariable @ApiParam(value = "实验项目名称", required = true) String projectName) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.judgeIsRepeatWhenAddRecord(recordId, projectName));
    }


    /**
     * Add one record response entity.
     *
     * @param addRecordInfo the add record info
     * @return the response entity
     */
    @ApiOperation(value = "新增一条实验记录及其项目详情", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = AddRecordInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/records")
    public ResponseEntity<AddRecordInfo> addOneRecord(@RequestBody @ApiParam(value = "模板model", required = true) AddRecordInfo addRecordInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.addOneRecord(addRecordInfo));
    }


    /**
     * Update one record response entity.
     *
     * @param recordInfo the record info
     * @return the response entity
     */
    @ApiOperation(value = "修改记录的中心编号", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/update/record")
    public ResponseEntity<SchoolLaboraryRecordInfo> updateOneRecord(@RequestBody @ApiParam(value = "记录model", required = true) SchoolLaboraryRecordInfo recordInfo) {
        recordInfo.setModifyTime(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(recordService.updateOneRecord(recordInfo));
    }

    /**
     * Judge center code is repeat response entity.
     *
     * @param centerCode the center code
     * @return the response entity
     */
    @ApiOperation(value = "当修改中心编号时判断是否重复", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/records/center-code/{centerCode}/is-repeat")
    public ResponseEntity<Boolean> judgeCenterCodeIsRepeat(@PathVariable @ApiParam(value = "中心编号", required = true) String centerCode) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.judgeCenterCodeIsRepeat(centerCode));
    }

    /**
     * Delete one record response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @ApiOperation(value = "根据id删除一条实验记录及其项目详情", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping(value = "/records/{id}")
    public ResponseEntity<Boolean> deleteOneRecord(@PathVariable @ApiParam(value = "id", required = true) String id) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.deleteOneRecord(id));
    }

    /**
     * Gets records by verify status.
     *
     * @param statusConditionQueryInfo the status condition query info
     * @return the records by verify status
     */
    @ApiOperation(value = "根据审核的状态分页查询实验记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/records/verify-status")
    public ResponseEntity<PageQueryResultInfo> getRecordsByVerifyStatus(@RequestBody @ApiParam(value = "分页查询model，页码(pageNumber)从1开始", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.getRecordsByVerifyStatus(statusConditionQueryInfo));
    }

//    /**
//     * Gets verified records.
//     *
//     * @param statusConditionQueryInfo the status condition query info
//     * @return the verified records
//     */
//    @ApiOperation(value = "查询已审核的所有实验记录", produces = "application/json;charset=UTF-8")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
//            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
//            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
//    })
//    @PostMapping(value = "/records/verified")
//    public ResponseEntity<PageQueryResultInfo> getVerifiedRecords(@RequestBody @ApiParam(value = "查询model，一页查询所有", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
//        return ResponseEntity.status(HttpStatus.OK).body(recordService.getVerifiedRecords(statusConditionQueryInfo));
//    }


    /**
     * Gets not approval records.
     *
     * @param statusConditionQueryInfo the status condition query info
     * @return the not approval records
     */
    @ApiOperation(value = "根据批准的状态分页查询的实验记录", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/records/approval-status")
    public ResponseEntity<PageQueryResultInfo> getRecordsByApprovalStatus(@RequestBody @ApiParam(value = "分页查询model，页码(pageNumber)从1开始", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(recordService.getRecordsByApprovalStatus(statusConditionQueryInfo));
    }

//    @ApiOperation(value = "查询已批准的所有实验记录", produces = "application/json;charset=UTF-8")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "获取成功", response = PageQueryResultInfo.class),
//            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
//            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
//    })
//    @PostMapping(value = "/records/approved")
//    public ResponseEntity<PageQueryResultInfo> getApprovedRecords(@RequestBody @ApiParam(value = "查询model，一页查询所有", required = true) StatusConditionQueryInfo statusConditionQueryInfo) {
//        return ResponseEntity.status(HttpStatus.OK).body(recordService.getApprovedRecords(statusConditionQueryInfo));
//    }


}
