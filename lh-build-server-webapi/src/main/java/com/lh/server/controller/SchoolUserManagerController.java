package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolLoginInfo;
import com.lh.server.contract.model.SchoolUserInfo;
import com.lh.server.contract.service.SchoolUserManagerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-用户管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolUserManagerController {


    @Autowired
    private SchoolUserManagerService schoolUserManagerService;

    /**
     * 获取用户集合
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "获取用户集合")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @GetMapping(value = "/user/getall")
    public ResponseEntity<List<SchoolUserInfo>> findAll() {
        return new ResponseEntity<>(schoolUserManagerService.getAllUsers(), HttpStatus.OK);
    }

    /**
     * 获取用户集合
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "根据id获取用户")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @GetMapping(value = "/getUserInfoByUserId/{userId}")
    public ResponseEntity<SchoolUserInfo> findByUserId(@PathVariable @ApiParam(value = "userId", required = true) String userId) {
        return new ResponseEntity<SchoolUserInfo>(schoolUserManagerService.getById(userId), HttpStatus.OK);
    }


    /**
     * 增加用户
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "增加用户")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @PostMapping(value = "/user/add")
    public ResponseEntity<Boolean> addUser(@RequestBody SchoolUserInfo schoolUserInfo) {
        return new ResponseEntity<>(schoolUserManagerService.addUser(schoolUserInfo), HttpStatus.OK);
    }

    /**
     * 修改用户名
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "修改用户名")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @PostMapping(value = "/user/modify")
    public ResponseEntity<Boolean> modifyUser(@RequestBody SchoolUserInfo schoolUserInfo) {
        return new ResponseEntity<>(schoolUserManagerService.updateUser(schoolUserInfo), HttpStatus.OK);
    }

    /**
     * 重置密码
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "重置密码")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @PostMapping(value = "/user/reset")
    public ResponseEntity<Boolean> resetUserPassword(@RequestBody SchoolUserInfo schoolUserInfo) {
        return new ResponseEntity<>(schoolUserManagerService.resetPassword(schoolUserInfo), HttpStatus.OK);
    }

    /**
     * 删除用户
     *
     * @return {@link ResponseEntity < List <  SchoolLoginInfo  >>}
     */
    @ApiOperation(value = "LoginController controller", notes = "删除用户")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal Server Error", response = HttpError.class),
            @ApiResponse(code = 406, message = "Not Acceptable", response = HttpError.class)})
    @PostMapping(value = "/user/delete")
    public ResponseEntity<Boolean> deleteUser(@RequestBody SchoolUserInfo schoolUserInfo) {
        return new ResponseEntity<>(schoolUserManagerService.deleteUser(schoolUserInfo), HttpStatus.OK);
    }

}
