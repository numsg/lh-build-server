package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboraryDeviceGroupInfo;
import com.lh.server.contract.model.SchoolLaboraryLogInfo;
import com.lh.server.contract.model.SchoolLaboratoryInfo;
import com.lh.server.contract.service.SchoolLaboraryLogService;
import com.lh.server.service.entity.SchoolLaboraryLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-操作日志"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboraryLogController {

    @Autowired
    private SchoolLaboraryLogService schoolLaboraryLogService;

    /**
     * 保存操作日志
     *
     * @param schoolLaboraryLogInfo 日志对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "保存操作日志", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/log/add")
    public ResponseEntity<Boolean> AddOperationLog(@RequestBody SchoolLaboraryLogInfo schoolLaboraryLogInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryLogService.addLaboraryLog(schoolLaboraryLogInfo));
    }


    /**
     * 分页获取日志记录信息
     *
     * @return response   分页获取日志记录信息
     */
    @ApiOperation(value = "分页获取日志记录信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/log/page")
    public ResponseEntity<PageInfo<SchoolLaboraryLogInfo>> getLogByPage(@RequestParam int pageNum, @RequestParam int pageSize, @RequestParam String keyWord) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryLogService.getLogByPage(pageNum, pageSize, keyWord));
    }
}
