package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolLaboraryRecordProjectInfo;
import com.lh.server.contract.service.SchoolLaboraryRecordProjectService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api(value = "/api/v1", tags = {"实验过程-记录管理-实验记录项目详情"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboraryRecordProjectController {
    @Autowired
    private SchoolLaboraryRecordProjectService recordProjectService;

    @ApiOperation(value = "根据实验记录id查询项目详情", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordProjectInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/records/project-detail/record-id/{recordId}")
    public ResponseEntity<List<SchoolLaboraryRecordProjectInfo>> getRecordProjectsByRecordId(@PathVariable @ApiParam(value = "实验记录id", required = true) String recordId) {
        return ResponseEntity.status(HttpStatus.OK).body(recordProjectService.getRecordProjectsByRecordId(recordId));
    }

    @ApiOperation(value = "修改一条项目详情内容", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordProjectInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping(value = "/records/project-detail/{id}")
    public ResponseEntity<SchoolLaboraryRecordProjectInfo> updateOneRecordProject(@PathVariable @ApiParam(value = "id", required = true) String id,
                                                                    @RequestBody @ApiParam(value = "项目详情model", required = true) SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        recordProjectInfo.setModifyTime(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(recordProjectService.updateOneRecordProject(id, recordProjectInfo));
    }

    @ApiOperation(value = "修改一条项目详情审核状态（0-未审核，1-审核中，2-已审核）", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordProjectInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping(value = "/records/project-detail/verify/{id}")
    public ResponseEntity<SchoolLaboraryRecordProjectInfo> verifyOneRecordProject(@PathVariable @ApiParam(value = "id", required = true) String id,
                                                                                  @RequestBody @ApiParam(value = "项目详情model", required = true) SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        recordProjectInfo.setVerifyTime(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(recordProjectService.updateOneRecordProject(id, recordProjectInfo));
    }

    @ApiOperation(value = "修改一条项目详情批准状态（0-未批准，1-批准中，2-已批准）", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryRecordProjectInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping(value = "/records/project-detail/approval/{id}")
    public ResponseEntity<SchoolLaboraryRecordProjectInfo>approvalOneRecordProject(@PathVariable @ApiParam(value = "id", required = true) String id,
                                                                                  @RequestBody @ApiParam(value = "项目详情model", required = true) SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        recordProjectInfo.setApprovalTime(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(recordProjectService.updateOneRecordProject(id, recordProjectInfo));
    }

    @ApiOperation(value = "根据id删除一条项目详情", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping(value = "/records/project-detail/{id}")
    public ResponseEntity<Boolean> deleteOneRecordProject(@PathVariable @ApiParam(value = "id", required = true) String id) {
        return ResponseEntity.status(HttpStatus.OK).body(recordProjectService.deleteOneRecordProject(id));
    }
}
