package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolMenuInfo;
import com.lh.server.contract.model.SchoolRoleMenuRelationInfo;
import com.lh.server.contract.service.SchoolMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-菜单管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolMenuController {
    /**
     * 菜单服务
     */
    @Autowired
    private SchoolMenuService menuService;


    /**
     * 获取所有菜单
     *
     * @return all menu
     */
    @ApiOperation(value = "获取所有菜单", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/menu")
    public ResponseEntity<List<SchoolMenuInfo>> getAllMenu() {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.getAllMenu());
    }

    /**
     * 获取树结构菜单
     *
     * @return menu tree
     */
    @ApiOperation(value = "获取树结构菜单", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/getMenuTree")
    public ResponseEntity<List<SchoolMenuInfo>> getMenuTree() {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.getMenuTree());
    }

    /**
     * 根据角色ID获取树形菜单集合
     *
     * @param roleId 角色ID
     * @return tree menu by role id
     */
    @ApiOperation(value = "根据角色ID获取树形菜单集合", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/getTreeMenuByRoleId/{roleId}")
    public ResponseEntity<List<SchoolMenuInfo>> getTreeMenuByRoleId(@PathVariable String roleId) {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.getTreeMenuByRoleId(roleId));
    }

    /**
     * 根据角色ID获取菜单集合
     *
     * @param roleId 角色ID
     * @return menu by role id
     */
    @ApiOperation(value = "根据角色ID获取菜单集合", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/menu/{roleId}")
    public ResponseEntity<List<SchoolMenuInfo>> getMenuByRoleId(@PathVariable String roleId) {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.getMenuByRoleId(roleId));
    }

    /**
     * 根据角色ID删除菜单关联
     *
     * @param roleId 角色ID
     * @return response entity
     */
    @ApiOperation(value = "根据角色ID删除菜单关联", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping(value = "/menu/{roleId}")
    public ResponseEntity<Boolean> deleteRelationsByRoleId(@PathVariable String roleId) {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.deleteRelationsByRoleId(roleId));
    }

    /**
     * 配置角色菜单
     *
     * @param roleMenuRelations 角色菜单关系
     * @return response entity
     */
    @ApiOperation(value = "配置角色菜单", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "配置成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/configMenus")
    public ResponseEntity<Boolean> configRoleMenus(@RequestBody List<SchoolRoleMenuRelationInfo> roleMenuRelations) {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.configRoleMenus(roleMenuRelations));
    }

    /**
     * Add menu response entity.
     *
     * @param menu the menu
     * @return the response entity
     */
    @ApiOperation(value = "添加菜单", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "添加成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/menu")
    public ResponseEntity<Boolean> addMenu(@RequestBody SchoolMenuInfo menu) {
        return ResponseEntity.status(HttpStatus.OK).body(menuService.addMenu(menu));
    }
}
