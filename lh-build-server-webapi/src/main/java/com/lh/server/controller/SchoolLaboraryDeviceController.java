package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolLaboraryDeviceGroupInfo;
import com.lh.server.contract.model.SchoolLaboraryDeviceInfo;
import com.lh.server.contract.service.SchoolLaboraryDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-设备管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboraryDeviceController {


    @Autowired
    private SchoolLaboraryDeviceService schoolLaboraryDeviceService;


    /**
     * 获取当前访问ip所属的实验室下对应的设备分组信息
     *
     * @return all menu
     */
    @ApiOperation(value = "获取当前访问ip所属的实验室下对应的设备分组信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/devicegroup/all")
    public ResponseEntity<List<SchoolLaboraryDeviceGroupInfo>> getAllDeviceGroup() {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.getALLDeviceGroup());
    }

    /**
     * 增加设备分组
     *
     * @param schoolLaboraryDeviceGroupInfo 分组对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "增加设备分组", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/devicegroup/add")
    public ResponseEntity<Boolean> AddDeviceGroup(@RequestBody SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.addDeviceGroup(schoolLaboraryDeviceGroupInfo));
    }


    /**
     * 修改设备分组
     *
     * @param schoolLaboraryDeviceGroupInfo 分组对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "修改设备分组", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/devicegroup/modify")
    public ResponseEntity<Boolean> ModifyDeviceGroup(@RequestBody SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.modifyDeviceGroup(schoolLaboraryDeviceGroupInfo));
    }


    /**
     * 删除设备分组
     *
     * @param schoolLaboraryDeviceGroupInfo 分组对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "删除设备分组", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/devicegroup/delete")
    public ResponseEntity<Boolean> DeleteDeviceGroup(@RequestBody SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.deleteDeviceGrouop(schoolLaboraryDeviceGroupInfo));
    }


    /**
     * 根据设备组ID获取所有设备集合信息
     *
     * @return all menu
     */
    @ApiOperation(value = "根据设备组ID获取所有设备集合信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/device/all/{devicegroupid}")
    public ResponseEntity<List<SchoolLaboraryDeviceInfo>> getAllDeviceByDeviceId(@PathVariable String devicegroupid) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.GetAllDeviceInfoByDeviceGroupId(devicegroupid));
    }


    /**
     * 增加设备信息
     *
     * @param schoolLaboraryDeviceInfo 设备对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "增加设备信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/device/add")
    public ResponseEntity<Boolean> AddDevice(@RequestBody SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.addDevice(schoolLaboraryDeviceInfo));
    }


    /**
     * 修改设备信息
     *
     * @param schoolLaboraryDeviceInfo 分组对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "修改设备信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/device/modify")
    public ResponseEntity<Boolean> ModifyDevice(@RequestBody SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.modifyDevice(schoolLaboraryDeviceInfo));
    }


    /**
     * 删除设备信息
     *
     * @param schoolLaboraryDeviceInfo 分组对象
     * @return 登录信息 response entity
     */
    @ApiOperation(value = "删除设备信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/device/delete")
    public ResponseEntity<Boolean> DeleteDevice(@RequestBody SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboraryDeviceService.deleteDevice(schoolLaboraryDeviceInfo));
    }
}
