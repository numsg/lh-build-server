package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.SchoolLaboraryResultTemplateInfo;
import com.lh.server.contract.service.SchoolLaboraryResultTemplateService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * The type School laborary result template controller.
 */
@Api(value = "/api/v1", tags = {"实验过程-系统管理-结果录入模板"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboraryResultTemplateController {
    @Autowired
    private SchoolLaboraryResultTemplateService resultTemplateService;

    /**
     * Gets all result templates.
     *
     * @return the all result templates
     */
    @ApiOperation(value = "获取所有结果模板", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryResultTemplateInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/result-templates/all")
    public ResponseEntity<List<SchoolLaboraryResultTemplateInfo>> getAllResultTemplates() {
        return ResponseEntity.status(HttpStatus.OK).body(resultTemplateService.getAllResultTemplates());
    }

    /**
     * Gets one result template by id.
     *
     * @param id the id
     * @return the one result template by id
     */
    @ApiOperation(value = "根据id查询一个模板", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryResultTemplateInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/result-templates/one/{id}")
    public ResponseEntity<SchoolLaboraryResultTemplateInfo> getOneResultTemplateById(@PathVariable @ApiParam(value = "id", required = true) String id) {
        return ResponseEntity.status(HttpStatus.OK).body(resultTemplateService.getOneResultTemplateById(id));
    }

    /**
     * Add one result template response entity.
     *
     * @param resultTemplateInfo the result template info
     * @return the response entity
     */
    @ApiOperation(value = "新增一个模板", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryResultTemplateInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/result-templates")
    public ResponseEntity<SchoolLaboraryResultTemplateInfo> addOneResultTemplate(@RequestBody @ApiParam(value = "模板model", required = true) SchoolLaboraryResultTemplateInfo resultTemplateInfo) {
        resultTemplateInfo.setId(UUID.randomUUID().toString());
        resultTemplateInfo.setCreateTime(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(resultTemplateService.addOneResultTemplate(resultTemplateInfo));
    }

    /**
     * Update one result template response entity.
     *
     * @param id                 the id
     * @param resultTemplateInfo the result template info
     * @return the response entity
     */
    @ApiOperation(value = "修改一个模板", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = SchoolLaboraryResultTemplateInfo.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PutMapping(value = "/result-templates/{id}")
    public ResponseEntity<SchoolLaboraryResultTemplateInfo> updateOneResultTemplate(@PathVariable @ApiParam(value = "id", required = true) String id,
                                                                                    @RequestBody @ApiParam(value = "模板model", required = true) SchoolLaboraryResultTemplateInfo resultTemplateInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(resultTemplateService.updateOneResultTemplate(id, resultTemplateInfo));
    }

    /**
     * Delete one result template response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @ApiOperation(value = "根据id删除一个模板", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功", response = Boolean.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @DeleteMapping(value = "/result-templates/{id}")
    public ResponseEntity<Boolean> deleteOneResultTemplate(@PathVariable @ApiParam(value = "id", required = true) String id) {
        return ResponseEntity.status(HttpStatus.OK).body(resultTemplateService.deleteOneResultTemplate(id));
    }
}
