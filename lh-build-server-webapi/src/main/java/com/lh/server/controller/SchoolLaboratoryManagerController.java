package com.lh.server.controller;

import com.gsafety.java.common.exception.HttpError;
import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboratoryInfo;
import com.lh.server.contract.model.SchoolLoginInfo;
import com.lh.server.contract.model.SchoolMenuInfo;
import com.lh.server.contract.service.LoginService;
import com.lh.server.contract.service.SchoolLaboratoryManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(value = "/api/v1", tags = {"实验过程-系统管理-实验室管理"})
@RestController
@RequestMapping("/api/v1")
public class SchoolLaboratoryManagerController {
    /**
     * The User service.
     */
    @Autowired
    SchoolLaboratoryManagerService schoolLaboratoryManagerService;

    /**
     * 获取所有实验室
     *
     * @return  response   实验室列表
     */
    @ApiOperation(value = "获取所有实验室", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/laboratory")
    public ResponseEntity<List<SchoolLaboratoryInfo>> getAllLaboratory() {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboratoryManagerService.getAllSchoolLaboratory());
    }

    /**
     * 添加实验室
     *
     * @return  response   实验室列表
     */
    @ApiOperation(value = "添加实验室信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/laboratory/add")
    public ResponseEntity<Boolean> addLaboratory(@RequestBody SchoolLaboratoryInfo schoolLaboratoryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboratoryManagerService.addSchoolLaboratory(schoolLaboratoryInfo));
    }

    /**
     * 删除实验室信息
     *
     * @return  response   实验室列表
     */
    @ApiOperation(value = "删除实验室信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/laboratory/delete/{id}")
    public ResponseEntity<Boolean> addLaboratory(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboratoryManagerService.deleteById(id));
    }

    /**
     * 添加实验室
     *
     * @return  response   实验室列表
     */
    @ApiOperation(value = "添加实验室信息", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @PostMapping(value = "/laboratory/modify")
    public ResponseEntity<Boolean> modifyLaboratory(@RequestBody SchoolLaboratoryInfo schoolLaboratoryInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboratoryManagerService.modifySchoolLaboratory(schoolLaboratoryInfo));
    }

    /**
     * 根据搜索关键字分页获取实验室列表
     *
     * @return  response   实验室列表
     */
    @ApiOperation(value = "根据搜索关键字分页获取实验室列表", produces = "application/json;charset=UTF-8")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功", response = Map.class),
            @ApiResponse(code = 400, message = "参数非法", response = HttpError.class),
            @ApiResponse(code = 500, message = "服务器异常", response = HttpError.class)
    })
    @GetMapping(value = "/laboratory/page")
    public ResponseEntity<PageInfo<SchoolLaboratoryInfo>> modifyLaboratory(@RequestParam  int pageNum , @RequestParam int pageSize,@RequestParam String keyWord) {
        return ResponseEntity.status(HttpStatus.OK).body(schoolLaboratoryManagerService.getSchoolLaboratoryByPage(pageNum,pageSize,keyWord));
    }
}
