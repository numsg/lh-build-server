package com.lh.server.backend.configs.odata;

import com.gsafety.odata.service.datasource.NumsgDataProvider;
import com.gsafety.odata.service.edm.NumsgEdmProvider;
import com.gsafety.odata.service.processor.NumsgEntityCollectionProcessor;
import com.gsafety.odata.service.processor.NumsgEntityProcessor;
import com.gsafety.odata.service.processor.NumsgErrorProcessor;
import com.gsafety.odata.service.processor.NumsgPrimitiveProcessor;
import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataHttpHandler;
import org.apache.olingo.server.api.ServiceMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * @ClassName ODataServlet
 * @Description
 * @Author ChenCong
 * @Date 2020/5/2
 */
@WebServlet(urlPatterns = "/OdataService.svc/*")
public class ODataServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ODataServlet.class);

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            ServletContext servletContext = getServletContext();
            WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            NumsgEdmProvider numsgEdmProvider = wac.getBean(NumsgEdmProvider.class);
            NumsgDataProvider numsgDataProvider = wac.getBean(NumsgDataProvider.class);

            OData odata = OData.newInstance();
            ServiceMetadata edm = odata.createServiceMetadata(numsgEdmProvider, new ArrayList<>());
            ODataHttpHandler handler = odata.createHandler(edm);
            handler.register(new NumsgEntityProcessor(numsgDataProvider));
            handler.register(new NumsgEntityCollectionProcessor(numsgDataProvider));
            handler.register(new NumsgPrimitiveProcessor(numsgDataProvider));
            handler.register(new NumsgErrorProcessor());
            /**
             * for ajax-cross-domain request
             * config(ajax-cross-domain.properties)
             */
            resp.setHeader("Access-Control-Allow-Origin", "*");
            resp.setDateHeader("Expires", 0); // Proxies.
            handler.process(req, resp);
        } catch (RuntimeException e) {
            LOG.error(e.getMessage());
        }
    }
}

