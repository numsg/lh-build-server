package com.lh.server.backend.configs;

import com.lh.server.common.jdbc.JdbcProperties;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 数据库注入
 */
@Configuration
@EnableConfigurationProperties(JdbcProperties.class)
public class jdbcConfiguration {
    @Autowired
    private JdbcProperties jdbcProperties;

    @Bean
    public DataSource dataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(jdbcProperties.getDriverClassName());
        hikariDataSource.setJdbcUrl(jdbcProperties.getUrl());
        hikariDataSource.setUsername(jdbcProperties.getUsername());
        hikariDataSource.setPassword(jdbcProperties.getPassword());
        hikariDataSource.setMaximumPoolSize(jdbcProperties.getMaximumPoolSize());
        hikariDataSource.setMinimumIdle(jdbcProperties.getMinimumIdle());
        hikariDataSource.setMaxLifetime(jdbcProperties.getMaxLifetime());
        hikariDataSource.setIdleTimeout(jdbcProperties.getIdleTimeout());
        return hikariDataSource;
    }
}
