package com.lh.server.backend.aspectj;

import com.alibaba.fastjson.JSON;
import com.lh.server.common.annotations.HttpHeaderAnnotation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * Headers 切面
 */
@Aspect
@Component
public class HttpHeadersAspect {
    private Logger log = LoggerFactory.getLogger(HttpHeadersAspect.class);


    @Before("execution(* com.lh.server.controller.*.*(..)) && @annotation(com.lh.server.common.annotations.HttpHeaderAnnotation)")
    public void getAnnotation(JoinPoint joinPoint) {
        try {
            // 获取HttpRequest
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            HttpHeaderAnnotation httpHeaderAnnotation = this.getAnnotationPorpertyInfo(joinPoint);
            if (httpHeaderAnnotation == null) {
                return;
            }
            String contentType = httpHeaderAnnotation.contentType();
            String date = new Date().toString();
            String transferEncoding = httpHeaderAnnotation.transferEncoding();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("content-type", contentType);
            httpHeaders.add("date", date.toString());
            httpHeaders.add("transfer-encoding", transferEncoding);
            log.info(JSON.toJSONString(httpHeaders));

        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    private HttpHeaderAnnotation getAnnotationPorpertyInfo(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(HttpHeaderAnnotation.class);
        }
        return null;
    }
}
