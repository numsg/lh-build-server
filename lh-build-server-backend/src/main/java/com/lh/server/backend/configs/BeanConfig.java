package com.lh.server.backend.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by xiaodiming on 2017/3/7.
 */
@Configuration
public class BeanConfig {
    @Bean
    public AppInit appInitBean() {
        return new AppInit();
    }
}
