package com.lh.server.contract.model;

public class SchoolUserRoleRelationInfo {
    private String id;
    private String userId;
    private String roleId;
    private SchoolLoginInfo user;
    private SchoolRoleInfo role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public SchoolLoginInfo getUser() {
        return user;
    }

    public void setUser(SchoolLoginInfo user) {
        this.user = user;
    }

    public SchoolRoleInfo getRole() {
        return role;
    }

    public void setRole(SchoolRoleInfo role) {
        this.role = role;
    }
}
