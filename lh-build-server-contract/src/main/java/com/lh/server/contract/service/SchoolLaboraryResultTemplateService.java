package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolLaboraryResultTemplateInfo;

import java.util.List;

/**
 * The interface School laborary result template service.
 */
public interface SchoolLaboraryResultTemplateService {
    /**
     * Gets all result templates.
     *
     * @return the all result templates
     */
    List<SchoolLaboraryResultTemplateInfo> getAllResultTemplates();

    /**
     * Gets one result template by id.
     *
     * @param id the id
     * @return the one result template by id
     */
    SchoolLaboraryResultTemplateInfo getOneResultTemplateById(String id);

    /**
     * Add one result template school laborary result template info.
     *
     * @param resultTemplateInfo the result template info
     * @return the school laborary result template info
     */
    SchoolLaboraryResultTemplateInfo addOneResultTemplate(SchoolLaboraryResultTemplateInfo resultTemplateInfo);

    /**
     * Update one result template school laborary result template info.
     *
     * @param id                 the id
     * @param resultTemplateInfo the result template info
     * @return the school laborary result template info
     */
    SchoolLaboraryResultTemplateInfo updateOneResultTemplate(String id, SchoolLaboraryResultTemplateInfo resultTemplateInfo);

    /**
     * Delete one result template boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean deleteOneResultTemplate(String id);
}
