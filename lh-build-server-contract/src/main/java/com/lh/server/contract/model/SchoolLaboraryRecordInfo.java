package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

public class SchoolLaboraryRecordInfo {
    private String id;
    // 中心编号
    private String centerCode;
    // 中心名称
    private String centerName;
    // 实验室管理主键
    private String laboraryId;
    // 实验室管理名称
    private String laboraryName;
    // 项目数量
    private int projectNum;
    // 修改中心编号时
    private Date modifyTime;
    // 修改中心编号人
    private String modifyName;
    // 提交时间
    private Date submitTime;

    @ApiModelProperty(value = "审核状态（0-未审核，1-审核中，2-已审核,当所有项目都是已审核状态，记录才是已审核状态）")
    private int verifyResult;

    private Date verifyTime;

    @ApiModelProperty(value = "批准状态0-未批准，1-批准中，2-已批准，当所有项目都是已批准状态，记录才是已批准状态）")
    private int approvalResult;

    private Date approvalTime;

    // 记录项目详情
    private List<SchoolLaboraryRecordProjectInfo> recordProjectInfos;


    public SchoolLaboraryRecordInfo() {
        //无参构造
    }

    public SchoolLaboraryRecordInfo(String id, String centerCode, String centerName, String laboraryId, String laboraryName, int projectNum, Date modifyTime, String modifyName, Date submitTime, int verifyResult, Date verifyTime, int approvalResult, Date approvalTime, List<SchoolLaboraryRecordProjectInfo> recordProjectInfos) {
        this.id = id;
        this.centerCode = centerCode;
        this.centerName = centerName;
        this.laboraryId = laboraryId;
        this.laboraryName = laboraryName;
        this.projectNum = projectNum;
        this.modifyTime = modifyTime;
        this.modifyName = modifyName;
        this.submitTime = submitTime;
        this.verifyResult = verifyResult;
        this.verifyTime = verifyTime;
        this.approvalResult = approvalResult;
        this.approvalTime = approvalTime;
        this.recordProjectInfos = recordProjectInfos;
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getLaboraryId() {
        return laboraryId;
    }

    public void setLaboraryId(String laboraryId) {
        this.laboraryId = laboraryId;
    }

    public String getLaboraryName() {
        return laboraryName;
    }

    public void setLaboraryName(String laboraryName) {
        this.laboraryName = laboraryName;
    }

    public int getProjectNum() {
        return projectNum;
    }

    public void setProjectNum(int projectNum) {
        this.projectNum = projectNum;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyName() {
        return modifyName;
    }

    public void setModifyName(String modifyName) {
        this.modifyName = modifyName;
    }

    public List<SchoolLaboraryRecordProjectInfo> getRecordProjectInfos() {
        return recordProjectInfos;
    }

    public void setRecordProjectInfos(List<SchoolLaboraryRecordProjectInfo> recordProjectInfos) {
        this.recordProjectInfos = recordProjectInfos;
    }

    public int getVerifyResult() {
        return verifyResult;
    }

    public void setVerifyResult(int verifyResult) {
        this.verifyResult = verifyResult;
    }

    public int getApprovalResult() {
        return approvalResult;
    }

    public void setApprovalResult(int approvalResult) {
        this.approvalResult = approvalResult;
    }


}
