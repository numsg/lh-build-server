package com.lh.server.contract.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;


/**
 * Login Info
 */
@ApiModel
public class SchoolLoginInfo {
    @ApiModelProperty(value = "主键")
    private String userId;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "密码")
    private String password;
    // 所属人
    private String name;
    // 角色ID
    private String roleid;
    // 角色名
    private String rolename;
    // 账户状态
    private int status;
    // 登录用户IP
    private String strIp;
    // 创建时间
    private Date createtime;
    @ApiModelProperty(value = "角色信息")
    private List<SchoolRoleInfo> roles;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleid() {
        return roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public List<SchoolRoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<SchoolRoleInfo> roles) {
        this.roles = roles;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getStrIp() {
        return strIp;
    }

    public void setStrIp(String strIp) {
        this.strIp = strIp;
    }
}
