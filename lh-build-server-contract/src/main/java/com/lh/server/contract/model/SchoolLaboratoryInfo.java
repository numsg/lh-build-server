package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * 实验室信息DTO
 */
public class SchoolLaboratoryInfo {

    /**
     *  主键
     */
    @ApiModelProperty(value = "主键")
    private String id;

    /**
     * 实验室名称
     */
    @ApiModelProperty(value = "实验室名称")
    private String laborary_name;

    /**
     * 权重
     */
    @ApiModelProperty(value = "权重")
    private int weight;

    /**
     实验室说明
     */
    @ApiModelProperty(value = "说明")
    private String laborary_desc;

    /**
     *  创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createtime;

    @ApiModelProperty(value = "实验室电脑Ip")
    private String schoolLaboaryComputerIps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLaborary_name() {
        return laborary_name;
    }

    public void setLaborary_name(String laborary_name) {
        this.laborary_name = laborary_name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getLaborary_desc() {
        return laborary_desc;
    }

    public void setLaborary_desc(String laborary_desc) {
        this.laborary_desc = laborary_desc;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }


    public String getSchoolLaboaryComputerIps() {
        return schoolLaboaryComputerIps;
    }

    public void setSchoolLaboaryComputerIps(String schoolLaboaryComputerIps) {
        this.schoolLaboaryComputerIps = schoolLaboaryComputerIps;
    }
}

