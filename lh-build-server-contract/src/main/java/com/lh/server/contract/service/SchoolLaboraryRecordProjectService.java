package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolLaboraryRecordProjectInfo;

import java.util.List;

/**
 * The interface School laborary record project service.
 */
public interface SchoolLaboraryRecordProjectService {
    /**
     * Gets record projects by record id.
     *
     * @param recordId the record id
     * @return the record projects by record id
     */
    List<SchoolLaboraryRecordProjectInfo>  getRecordProjectsByRecordId(String recordId);

    /**
     * Update one record project school laborary record project info.
     *
     * @param id                the id
     * @param recordProjectInfo the record project info
     * @return the school laborary record project info
     */
    SchoolLaboraryRecordProjectInfo updateOneRecordProject(String id ,SchoolLaboraryRecordProjectInfo recordProjectInfo);

    /**
     * Delete one record project boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean deleteOneRecordProject(String id);
}
