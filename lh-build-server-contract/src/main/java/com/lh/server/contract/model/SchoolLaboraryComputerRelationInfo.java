package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

public class SchoolLaboraryComputerRelationInfo {
    @ApiModelProperty(value = "主键Id")
    private String id;
    @ApiModelProperty(value = "电脑ip")
    private String computer_ip;
    @ApiModelProperty(value = "实验室id")
    private String laboraryId;

//    private SchoolLaboratoryInfo laboratoryInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public SchoolLaboratoryInfo getLaboratoryInfo() {
//        return laboratoryInfo;
//    }
//
//    public void setLaboratoryInfo(SchoolLaboratoryInfo laboratoryInfo) {
//        this.laboratoryInfo = laboratoryInfo;
//    }


    public String getComputer_ip() {
        return computer_ip;
    }

    public void setComputer_ip(String computer_ip) {
        this.computer_ip = computer_ip;
    }

    public String getLaboraryId() {
        return laboraryId;
    }

    public void setLaboraryId(String laboraryId) {
        this.laboraryId = laboraryId;
    }
}
