package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolUserInfo;

import java.util.List;

public interface SchoolUserManagerService {
    boolean addUser(SchoolUserInfo userInfo);
    boolean updateUser(SchoolUserInfo userInfo);
    boolean resetPassword(SchoolUserInfo userInfo);
    boolean deleteUser(SchoolUserInfo userInfo);
    List<SchoolUserInfo> getAllUsers();
    SchoolUserInfo getById(String userId);
}
