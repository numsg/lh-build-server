package com.lh.server.contract.model;

import java.util.Date;

public class SchoolLaboraryDeviceGroupInfo {
    /**
     * 主键
     */
    private String id;
    // 监控组名
    private String group_name;
    // 实验室ID
    private String laborary_id;
    // 实验室名称
    private String laborary_name;
    // 监控区域
    private String monitor_region;
    // 权重
    private int weight;
    // 模板ID
    private int template_id;
    // 数据同步接口
    private String data_interface_url;
    // 同步接口类型
    private String interface_type;
    // 创建时间
    private Date createtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getLaborary_id() {
        return laborary_id;
    }

    public void setLaborary_id(String laborary_id) {
        this.laborary_id = laborary_id;
    }

    public String getLaborary_name() {
        return laborary_name;
    }

    public void setLaborary_name(String laborary_name) {
        this.laborary_name = laborary_name;
    }

    public String getMonitor_region() {
        return monitor_region;
    }

    public void setMonitor_region(String monitor_region) {
        this.monitor_region = monitor_region;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    public String getData_interface_url() {
        return data_interface_url;
    }

    public void setData_interface_url(String data_interface_url) {
        this.data_interface_url = data_interface_url;
    }

    public String getInterface_type() {
        return interface_type;
    }

    public void setInterface_type(String interface_type) {
        this.interface_type = interface_type;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}
