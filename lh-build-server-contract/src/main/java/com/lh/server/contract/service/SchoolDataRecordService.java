package com.lh.server.contract.service;

import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolDataRecordInfo;
import com.lh.server.contract.model.SchoolLaboratoryInfo;

import java.util.List;

public interface SchoolDataRecordService {
    /**
     * 添加
     *
     * @param dataRecordInfo
     * @return 是否添加成功
     */
    Boolean save(SchoolDataRecordInfo dataRecordInfo);

    /**
     *  获取某个类型模型对应最新的sno
     * @param deviceId  设备Id
     * @return  最新的sno
     */
    String getByDeviceId(String deviceId);




    /**
     *  数据记录列表
     *
     * @return
     */
    List<SchoolDataRecordInfo> getAllSchoolDataRecordInfo();

    /**
     *  分页获取某种类型的结果
     * @return
     */
    PageInfo<SchoolDataRecordInfo> getLatestSchoolDataRecordInfoList(int dataType,int pageNum, int size, String deviceId);

    /**
     *  批量修改状态
     * @param schoolDataRecordInfos
     * @return
     */
    boolean updateDataRecords(List<SchoolDataRecordInfo> schoolDataRecordInfos);
}
