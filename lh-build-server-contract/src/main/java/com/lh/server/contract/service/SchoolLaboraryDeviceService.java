package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolLaboraryDeviceGroupInfo;
import com.lh.server.contract.model.SchoolLaboraryDeviceInfo;

import java.util.List;

public interface SchoolLaboraryDeviceService {

    // 增加设备分组
    boolean addDeviceGroup(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo);

    // 编辑设备组信息
    boolean modifyDeviceGroup(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo);

    // 删除设备组信息
    boolean deleteDeviceGrouop(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo);

    // 获取当前访问ip所属的实验室下对应的设备分组信息
    List<SchoolLaboraryDeviceGroupInfo> getALLDeviceGroup();

    // 根据设备组ID获取所有设备集合信息
    List<SchoolLaboraryDeviceInfo> GetAllDeviceInfoByDeviceGroupId(String deviceGroupId);

    // 增加设备信息
    boolean addDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo);

    // 编辑设备信息
    boolean modifyDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo);

    // 删除设备信息
    boolean deleteDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo);
}
