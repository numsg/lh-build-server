package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

public class SchoolBizTypeInfo {
    @ApiModelProperty(value = "主键")
    private Integer id;
    @ApiModelProperty(value = "业务类型名称")
    private String name;
    @ApiModelProperty(value = "是否启用（0：否，1：是）")
    private Integer enable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
}
