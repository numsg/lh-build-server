package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolSettingInfo;
import com.lh.server.contract.model.SchoolSettingTypeInfo;

import java.util.List;
import java.util.Map;

public interface SchoolSettingService {
    /**
     * 保存配置项
     *
     * @param settingInfo 配置项DTO对象
     * @return 配置项DTO对象 map
     */
    Map<String, Object> save(SchoolSettingInfo settingInfo);

    /**
     * 查找所有配置项数据
     *
     * @return 配置项集合 list
     */
    List<SchoolSettingInfo> findAll();

    /**
     * 根据ID查找配置项
     *
     * @param id 配置项ID
     * @return 配置项DTO对象 setting info
     */
    SchoolSettingInfo findById(String id);

    /**
     * 根据配置项ID删除配置项
     *
     * @param id 配置项ID
     */
    Boolean deleteById(String id);

    /**
     * 根据配置类型ID删除配置类型
     *
     * @param id 配置类型ID
     */
    Boolean deleteTypeById(String id);

    /**
     * 更新配置项
     *
     * @param settingInfo 待更新配置项DTO对象
     * @return 更新后的配置项DTO对象 map
     */
    Map<String, Object> update(SchoolSettingInfo settingInfo);

    /**
     * 根据参数类型查找配置项集合
     *
     * @param paramType 配置参数类型
     * @return 配置项集合 list
     */
    List<SchoolSettingInfo> findEnableByParamType(Integer paramType);

    /**
     * 根据参数类型查找已启用配置项集合
     *
     * @param paramType 配置参数类型
     * @return 配置项集合 list
     */
    List<SchoolSettingInfo> findByParamType(Integer paramType);

    /**
     * 根据参数类型集合批量查找配置项集合
     *
     * @param paramTypes 配置参数类型集合
     * @return 配置项集合 list
     */
    List<SchoolSettingInfo> findByParamTypes(List<Integer> paramTypes);

    /**
     * 根据配置项名称查询配置项
     *
     * @param settingName 配置项名称
     * @return 查到的配置项集合 list
     */
    List<SchoolSettingInfo> findBySettingName(String settingName);

    /**
     * 查询所有设置的type名称
     *
     * @return list list
     */
    List<SchoolSettingTypeInfo> findAllSettingType();

    /**
     * 保存设置类型
     *
     * @param settingTypeInfo the setting type info
     * @return map map
     */
    Map<String, Object> saveSettingType(SchoolSettingTypeInfo settingTypeInfo);
}
