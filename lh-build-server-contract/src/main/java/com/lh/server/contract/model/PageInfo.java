package com.lh.server.contract.model;

import com.gsafety.java.common.page.PageBean;

import java.util.List;

/**
 * 分页结果信息
 * @param <T>
 */
public class PageInfo<T> {
    /**
     *  数据集合
     */
    private  List<T> dataList;

    /**
     * 结果个数
     */
    private  long elementCount;

    /**
     * 总页数
     */
    private  int pageCount;

    public PageInfo(List<T> dataList, long elementCount,int pageCount) {

        this.dataList = dataList;
        this.elementCount = elementCount;
        this.pageCount=pageCount;
    }

    public List<T>  getDataList() {
        return dataList;
    }


    public long getElementCount() {
        return elementCount;
    }

    public void setElementCount(int elementCount) {
        this.elementCount = elementCount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
