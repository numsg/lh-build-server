package com.lh.server.contract.model;

public class SchoolSettingTypeInfo {
    private String id;

    /**
     * setting表中的type
     */
    private Integer typeId;

    /**
     * type的名称
     */
    private String typeName;

    /**
     * 启用
     */
    private Integer enable;

    /**
     * 是否可编辑
     */
    private Integer editable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }
}
