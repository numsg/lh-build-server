package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolMenuInfo;
import com.lh.server.contract.model.SchoolRoleMenuRelationInfo;

import java.util.List;

public interface SchoolMenuService {
    /**
     * 获取所有菜单列表
     *
     * @return 菜单列表 all menu
     */
    List<SchoolMenuInfo> getAllMenu();

    /**
     * Gets menu tree.
     *
     * @return the menu tree
     */
    List<SchoolMenuInfo> getMenuTree();

    /**
     * 根据角色ID获取菜单列表
     *
     * @param roleId 角色ID
     * @return 菜单列表 menu by role id
     */
    List<SchoolMenuInfo> getMenuByRoleId(String roleId);


    /**
     * 根据角色ID获取树形菜单列表
     *
     * @param roleId 角色ID
     * @return 树形菜单列表 tree menu by role id
     */
    List<SchoolMenuInfo> getTreeMenuByRoleId(String roleId);

    /**
     * 根据角色ID删除配置关系
     *
     * @param roleId 角色ID
     * @return 成功或失败 boolean
     */
    boolean deleteRelationsByRoleId(String roleId);

    /**
     * 配置角色菜单
     *
     * @param roleMenuRelations 角色菜单关系
     * @return boolean
     */
    boolean configRoleMenus(List<SchoolRoleMenuRelationInfo> roleMenuRelations);

    /**
     * Add menu boolean.
     *
     * @param menu the menu
     * @return the boolean
     */
    boolean addMenu(SchoolMenuInfo menu);
}
