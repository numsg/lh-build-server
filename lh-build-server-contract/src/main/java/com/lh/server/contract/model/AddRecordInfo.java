package com.lh.server.contract.model;

/**
 * The type Add record info.
 */
public class AddRecordInfo {
    private SchoolLaboraryRecordInfo recordInfo;
    private SchoolLaboraryRecordProjectInfo recordProjectInfo;

    public AddRecordInfo() {
        // 无参构造
    }

    public AddRecordInfo(SchoolLaboraryRecordInfo recordInfo, SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        this.recordInfo = recordInfo;
        this.recordProjectInfo = recordProjectInfo;
    }

    public SchoolLaboraryRecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(SchoolLaboraryRecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    public SchoolLaboraryRecordProjectInfo getRecordProjectInfo() {
        return recordProjectInfo;
    }

    public void setRecordProjectInfo(SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        this.recordProjectInfo = recordProjectInfo;
    }

}
