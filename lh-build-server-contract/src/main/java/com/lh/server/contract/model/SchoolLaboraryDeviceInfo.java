package com.lh.server.contract.model;

import java.util.Date;

public class SchoolLaboraryDeviceInfo {
    /**
     * 主键
     */
    private String id;
    // 监控名
    private String monitor_name;
    // 权重
    private int weight;
    // 监控编号
    private String monitor_num;
    // 监控IP
    private String monitor_ip;
    // 监控账户
    private String monitor_username;
    // 监控密码
    private String monitor_password;
    // 流媒体地址
    private String monitor_url;
    // 创建时间
    private Date createtime;
    //  设备组主键ID
    private String device_group_id;


    public String getMonitor_name() {
        return monitor_name;
    }

    public void setMonitor_name(String monitor_name) {
        this.monitor_name = monitor_name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getMonitor_num() {
        return monitor_num;
    }

    public void setMonitor_num(String monitor_num) {
        this.monitor_num = monitor_num;
    }

    public String getMonitor_ip() {
        return monitor_ip;
    }

    public void setMonitor_ip(String monitor_ip) {
        this.monitor_ip = monitor_ip;
    }

    public String getMonitor_username() {
        return monitor_username;
    }

    public void setMonitor_username(String monitor_username) {
        this.monitor_username = monitor_username;
    }

    public String getMonitor_password() {
        return monitor_password;
    }

    public void setMonitor_password(String monitor_password) {
        this.monitor_password = monitor_password;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getMonitor_url() {
        return monitor_url;
    }

    public void setMonitor_url(String monitor_url) {
        this.monitor_url = monitor_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice_group_id() {
        return device_group_id;
    }

    public void setDevice_group_id(String device_group_id) {
        this.device_group_id = device_group_id;
    }
}
