package com.lh.server.contract.service;

import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboratoryInfo;



import java.util.List;

public interface SchoolLaboratoryManagerService {
    /**
     * 返回所以实验室列表，有可能需要分页
     *
     * @return
     */
    List<SchoolLaboratoryInfo> getAllSchoolLaboratory();

    boolean addSchoolLaboratory(SchoolLaboratoryInfo schoolLaboratoryInfo);

    /**
     * 根据实验室ID删除实验室信息
     *
     * @param id 实验室ID
     */
    boolean deleteById(String id);

    /**
     * 修改实验信息
     *
     * @param schoolLaboratoryInfo
     * @return
     */
    boolean modifySchoolLaboratory(SchoolLaboratoryInfo schoolLaboratoryInfo);

    PageInfo<SchoolLaboratoryInfo> getSchoolLaboratoryByPage(int pageNum, int size, String keyword);
}
