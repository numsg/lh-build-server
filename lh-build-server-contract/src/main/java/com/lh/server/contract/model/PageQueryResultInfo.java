package com.lh.server.contract.model;

import java.util.List;

public class PageQueryResultInfo<T> {
    private List<T> records;
    private int total;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
