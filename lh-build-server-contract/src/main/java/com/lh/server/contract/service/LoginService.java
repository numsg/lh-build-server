package com.lh.server.contract.service;


import com.lh.server.contract.model.SchoolLoginInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/3.
 */
public interface LoginService {
    /**
     * 查询所有登录用户
     *
     * @return
     */
    List<SchoolLoginInfo> findAll();

    /**
     * 用户登录验证
     *
     * @param schoolLoginInfo
     * @return
     */
    Map<String, Object> login(SchoolLoginInfo schoolLoginInfo);
}
