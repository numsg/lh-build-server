package com.lh.server.contract.model;

import java.util.Date;


public class SchoolLaboraryLogInfo {
    /**
     * 主键
     */
    private String id;
    /**
     * 中心编号
     */
    private String centercode;
    /**
     * 项目名称
     */
    private String projectname;

    /**
     * 项目Id
     */
    private String proojectid;
    /**
     * 操作页面
     */
    private String opeationpage;
    /**
     * 操作描述
     */
    private String operationdesc;
    /**
     * 操作IP
     */
    private String operationip;
    /**
     * 操作时间
     */
    private Date operationtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCentercode() {
        return centercode;
    }

    public void setCentercode(String centercode) {
        this.centercode = centercode;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getOpeationpage() {
        return opeationpage;
    }

    public void setOpeationpage(String opeationpage) {
        this.opeationpage = opeationpage;
    }

    public String getOperationdesc() {
        return operationdesc;
    }

    public void setOperationdesc(String operationdesc) {
        this.operationdesc = operationdesc;
    }

    public String getOperationip() {
        return operationip;
    }

    public void setOperationip(String operationip) {
        this.operationip = operationip;
    }

    public Date getOperationtime() {
        return operationtime;
    }

    public void setOperationtime(Date operationtime) {
        this.operationtime = operationtime;
    }

    public String getProojectid() {
        return proojectid;
    }

    public void setProojectid(String proojectid) {
        this.proojectid = proojectid;
    }
}
