package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class SchoolRoleInfo {
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "角色名称")
    private String roleName;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "编码")
    private Integer code;
    @ApiModelProperty(value = "业务类型对象")
    private SchoolBizTypeInfo bizType;
    @ApiModelProperty(value = "是否可删除（0：否，1：是）")
    private Integer deletable = 1;
    @ApiModelProperty(value = "角色菜单关联关系")
    private List<SchoolRoleMenuRelationInfo> roleMenuRelations;
    @ApiModelProperty(value = "用户角色关联关系")
    private List<SchoolUserRoleRelationInfo> userRoleRelations;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getDeletable() {
        return deletable;
    }

    public void setDeletable(Integer deletable) {
        this.deletable = deletable;
    }

    public SchoolBizTypeInfo getBizType() {
        return bizType;
    }

    public void setBizType(SchoolBizTypeInfo bizType) {
        this.bizType = bizType;
    }

    public List<SchoolRoleMenuRelationInfo> getRoleMenuRelations() {
        return roleMenuRelations;
    }

    public void setRoleMenuRelations(List<SchoolRoleMenuRelationInfo> roleMenuRelations) {
        this.roleMenuRelations = roleMenuRelations;
    }

    public List<SchoolUserRoleRelationInfo> getUserRoleRelations() {
        return userRoleRelations;
    }

    public void setUserRoleRelations(List<SchoolUserRoleRelationInfo> userRoleRelations) {
        this.userRoleRelations = userRoleRelations;
    }
}
