package com.lh.server.contract.model;

import java.util.Date;

/**
 * The type School laborary result template info.
 */
public class SchoolLaboraryResultTemplateInfo {
    private String id;
    // 名称
    private String resultName;
    // 权重
    private int weight;
    // 状态
    private String resultStatus;
    // 描述
    private String resultDesc;
    // 创建时间
    private Date createTime;
    // 模板内容
    private String content;
    // 模板数据
    private String data;
    // 对应项目类型
    private String projectType;

    public SchoolLaboraryResultTemplateInfo() {
        //无参构造
    }

    public SchoolLaboraryResultTemplateInfo(String id, String resultName, int weight, String resultStatus, String resultDesc, Date createTime, String content, String data, String projectType) {
        this.id = id;
        this.resultName = resultName;
        this.weight = weight;
        this.resultStatus = resultStatus;
        this.resultDesc = resultDesc;
        this.createTime = createTime;
        this.content = content;
        this.data = data;
        this.projectType = projectType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }
}
