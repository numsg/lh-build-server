package com.lh.server.contract.service;

import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboraryLogInfo;


public interface SchoolLaboraryLogService {

    // 增加日志
    boolean addLaboraryLog(SchoolLaboraryLogInfo schoolLaboraryLogInfo);

    // 分页获取日志记录
    PageInfo<SchoolLaboraryLogInfo> getLogByPage(int pageNum, int size, String keyword);

}
