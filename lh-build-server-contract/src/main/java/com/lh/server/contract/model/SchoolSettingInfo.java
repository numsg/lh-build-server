package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class SchoolSettingInfo {
    /**
     * 参数Id
     */
    private String settingId;

    /**
     * 配置类型ID
     */
    private Integer paramType;

    /**
     * 具体配置项名称
     */
    @ApiModelProperty(required = true)
    @NotNull
    private String name;

    /**
     * 具体配置项的值
     */
    private String value;

    /**
     * 配置默认值
     */
    private String defaultValue;

    private String description;

    private Integer rowVersion;

    public String getSettingId() {
        return settingId;
    }

    public void setSettingId(String settingId) {
        this.settingId = settingId;
    }

    public Integer getParamType() {
        return paramType;
    }

    public void setParamType(Integer paramType) {
        this.paramType = paramType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
}
