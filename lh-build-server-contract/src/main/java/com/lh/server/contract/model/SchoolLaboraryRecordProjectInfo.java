package com.lh.server.contract.model;

import java.util.Date;

/**
 * The type School laborary record project info.
 */
public class SchoolLaboraryRecordProjectInfo {
    private String id;
    // 实验室记录主键id
    private String recordId;
    // 项目名称
    private String projectName;
    // 修改时间
    private Date modifyTime;
    // 修改人
    private String modifyName;
    // 提交时间
    private Date submitTime;
    // 提交人
    private String submitName;
    // 审核时间
    private Date verifyTime;
    // 审核状态（0-未审核，1-审核中，2-已审核）
    private int verifyResult;
    // 审核姓名
    private String verifyName;
    // 批准时间
    private Date approvalTime;
    // 批准状态（0-未批准，1-批准中，2-已批准）
    private int approvalResult;
    // 批准人姓名l
    private String approvalName;
    // 结果模板主键id
    private String resultId;
    // 监控分组id
    private String deviceGroupId;
    // 视频路径数组
    private String historyVideoUrls;
    // 真实实验室模板内容
    private String templateRecordContent;
    // 真实实验室模板数据
    private String templateRecordData;
    /**
     * Instantiates a new School laborary record project info.
     */
    public SchoolLaboraryRecordProjectInfo() {
        // 无参构造
    }

    public SchoolLaboraryRecordProjectInfo(String id, String recordId, String projectName, Date modifyTime, String modifyName, Date submitTime, String submitName, Date verifyTime, int verifyResult, String verifyName, Date approvalTime, int approvalResult, String approvalName, String resultId, String deviceGroupId, String historyVideoUrls, String templateRecordContent, String templateRecordData) {
        this.id = id;
        this.recordId = recordId;
        this.projectName = projectName;
        this.modifyTime = modifyTime;
        this.modifyName = modifyName;
        this.submitTime = submitTime;
        this.submitName = submitName;
        this.verifyTime = verifyTime;
        this.verifyResult = verifyResult;
        this.verifyName = verifyName;
        this.approvalTime = approvalTime;
        this.approvalResult = approvalResult;
        this.approvalName = approvalName;
        this.resultId = resultId;
        this.deviceGroupId = deviceGroupId;
        this.historyVideoUrls = historyVideoUrls;
        this.templateRecordContent = templateRecordContent;
        this.templateRecordData = templateRecordData;
    }

    public String getTemplateRecordData() {
        return templateRecordData;
    }

    public void setTemplateRecordData(String templateRecordData) {
        this.templateRecordData = templateRecordData;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets record id.
     *
     * @return the record id
     */
    public String getRecordId() {
        return recordId;
    }

    /**
     * Sets record id.
     *
     * @param recordId the record id
     */
    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    /**
     * Gets project name.
     *
     * @return the project name
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Sets project name.
     *
     * @param projectName the project name
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * Gets modify time.
     *
     * @return the modify time
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * Sets modify time.
     *
     * @param modifyTime the modify time
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * Gets modify name.
     *
     * @return the modify name
     */
    public String getModifyName() {
        return modifyName;
    }

    /**
     * Sets modify name.
     *
     * @param modifyName the modify name
     */
    public void setModifyName(String modifyName) {
        this.modifyName = modifyName;
    }

    /**
     * Gets submit time.
     *
     * @return the submit time
     */
    public Date getSubmitTime() {
        return submitTime;
    }

    /**
     * Sets submit time.
     *
     * @param submitTime the submit time
     */
    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    /**
     * Gets submit name.
     *
     * @return the submit name
     */
    public String getSubmitName() {
        return submitName;
    }

    /**
     * Sets submit name.
     *
     * @param submitName the submit name
     */
    public void setSubmitName(String submitName) {
        this.submitName = submitName;
    }

    /**
     * Gets verify time.
     *
     * @return the verify time
     */
    public Date getVerifyTime() {
        return verifyTime;
    }

    /**
     * Sets verify time.
     *
     * @param verifyTime the verify time
     */
    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    /**
     * Gets verify result.
     *
     * @return the verify result
     */
    public int getVerifyResult() {
        return verifyResult;
    }

    /**
     * Sets verify result.
     *
     * @param verifyResult the verify result
     */
    public void setVerifyResult(int verifyResult) {
        this.verifyResult = verifyResult;
    }

    /**
     * Gets verify name.
     *
     * @return the verify name
     */
    public String getVerifyName() {
        return verifyName;
    }

    /**
     * Sets verify name.
     *
     * @param verifyName the verify name
     */
    public void setVerifyName(String verifyName) {
        this.verifyName = verifyName;
    }

    /**
     * Gets approval time.
     *
     * @return the approval time
     */
    public Date getApprovalTime() {
        return approvalTime;
    }

    /**
     * Sets approval time.
     *
     * @param approvalTime the approval time
     */
    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    /**
     * Gets approval result.
     *
     * @return the approval result
     */
    public int getApprovalResult() {
        return approvalResult;
    }

    /**
     * Sets approval result.
     *
     * @param approvalResult the approval result
     */
    public void setApprovalResult(int approvalResult) {
        this.approvalResult = approvalResult;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    /**
     * Gets result id.
     *
     * @return the result id
     */
    public String getResultId() {
        return resultId;
    }

    /**
     * Sets result id.
     *
     * @param resultId the result id
     */
    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    /**
     * Gets device group id.
     *
     * @return the device group id
     */
    public String getDeviceGroupId() {
        return deviceGroupId;
    }

    /**
     * Sets device group id.
     *
     * @param deviceGroupId the device group id
     */
    public void setDeviceGroupId(String deviceGroupId) {
        this.deviceGroupId = deviceGroupId;
    }

    /**
     * Gets history video urls.
     *
     * @return the history video urls
     */
    public String getHistoryVideoUrls() {
        return historyVideoUrls;
    }

    /**
     * Sets history video urls.
     *
     * @param historyVideoUrls the history video urls
     */
    public void setHistoryVideoUrls(String historyVideoUrls) {
        this.historyVideoUrls = historyVideoUrls;
    }

    /**
     * Gets template record content.
     *
     * @return the template record content
     */
    public String getTemplateRecordContent() {
        return templateRecordContent;
    }

    /**
     * Sets template record content.
     *
     * @param templateRecordContent the template record content
     */
    public void setTemplateRecordContent(String templateRecordContent) {
        this.templateRecordContent = templateRecordContent;
    }
}
