package com.lh.server.contract.service;

import com.lh.server.contract.model.SchoolRoleInfo;
import com.lh.server.contract.model.SchoolUserRoleRelationInfo;

import java.util.List;
import java.util.Map;

public interface SchoolRoleService {
    /**
     * 获取所有角色信息
     *
     * @return all roles
     */
    List<SchoolRoleInfo> getAllRoles();

    /**
     * 配置用户角色信息
     *
     * @param userRoleRelations the user role relations
     * @return boolean
     */
    Boolean configUserRoles(List<SchoolUserRoleRelationInfo> userRoleRelations);

    /**
     * 添加角色
     *
     * @param role 角色
     * @return map
     */
    Map<String, Object> addRole(SchoolRoleInfo role);

    /**
     * 修改角色
     *
     * @param role 角色
     * @return map
     */
    Map<String, Object> updateRole(SchoolRoleInfo role);

    /**
     * 删除角色
     *
     * @param ids ID集合
     * @return boolean
     */
    Boolean deleteRole(String[] ids);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表 roles by user id
     */
    List<SchoolRoleInfo> getRolesByUserId(String userId);

    /**
     * Gets user role relation by role id.
     *
     * @param roleId the role id
     * @return the user role relation by role id
     */
    Boolean getUserRoleRelationByRoleId(String roleId);
}
