package com.lh.server.contract.service;

import com.lh.server.contract.model.AddRecordInfo;
import com.lh.server.contract.model.PageQueryResultInfo;
import com.lh.server.contract.model.SchoolLaboraryRecordInfo;
import com.lh.server.contract.model.StatusConditionQueryInfo;

import java.util.List;

/**
 * The interface School laborary record service.
 */
public interface SchoolLaboraryRecordService {
    /**
     * Gets all records.
     *
     * @return the all records
     */
    PageQueryResultInfo<SchoolLaboraryRecordInfo> pageQueryRecords(StatusConditionQueryInfo statusConditionQueryInfo, String prifex);

    /**
     * Gets one record by id.
     *
     * @param id the id
     * @return the one record by id
     */
    SchoolLaboraryRecordInfo getOneRecordById(String id);

    /**
     * Judge is repeat when add boolean.
     *
     * @param centerCode  the center code
     * @param projectName the project name
     * @return the boolean
     */
    Boolean judgeIsRepeatWhenAdd(String centerCode ,String projectName);

    Boolean judgeIsRepeatWhenAddRecord(String recordId, String projectName);

    /**
     * Add one record school laborary record info.
     *
     * @param addRecordInfo the add record info
     * @return the school laborary record info
     */
    AddRecordInfo addOneRecord(AddRecordInfo addRecordInfo);

    /**
     * Update one record school laborary record info.
     *
     * @param recordInfo the record info
     * @return the school laborary record info
     */
    SchoolLaboraryRecordInfo updateOneRecord(SchoolLaboraryRecordInfo recordInfo);


    /**
     * Judge center code is repeat boolean.
     *
     * @param centerCode the center code
     * @return the boolean
     */
    Boolean judgeCenterCodeIsRepeat(String centerCode);

    /**
     * Delete one record boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean deleteOneRecord(String id);

    /**
     * Gets records by verify status.
     *
     * @param conditionQueryInfo the condition query info
     * @return the records by verify status
     */
    PageQueryResultInfo<SchoolLaboraryRecordInfo> getRecordsByVerifyStatus(StatusConditionQueryInfo conditionQueryInfo);


    /**
     * Gets verified records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the verified records
     */
    PageQueryResultInfo<SchoolLaboraryRecordInfo> getVerifiedRecords(StatusConditionQueryInfo conditionQueryInfo);


    /**
     * Gets not approval records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the not approval records
     */
    PageQueryResultInfo<SchoolLaboraryRecordInfo> getRecordsByApprovalStatus(StatusConditionQueryInfo conditionQueryInfo);


    /**
     * Gets approved records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the approved records
     */
    PageQueryResultInfo<SchoolLaboraryRecordInfo>  getApprovedRecords(StatusConditionQueryInfo conditionQueryInfo);

}
