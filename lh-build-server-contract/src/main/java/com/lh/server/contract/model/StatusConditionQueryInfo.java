package com.lh.server.contract.model;

/**
 * The type Status condition query info.
 */
public class StatusConditionQueryInfo {
    private int status;
    private String centerCode;
    private String laboraryId;
    private int pageSize;
    private  int pageNumber;

    public StatusConditionQueryInfo() {
        // 无参构造
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getLaboraryId() {
        return laboraryId;
    }

    public void setLaboraryId(String laboraryId) {
        this.laboraryId = laboraryId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
