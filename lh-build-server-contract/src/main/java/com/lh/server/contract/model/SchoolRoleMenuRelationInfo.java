package com.lh.server.contract.model;

import io.swagger.annotations.ApiModelProperty;

public class SchoolRoleMenuRelationInfo {
    @ApiModelProperty(value = "ID")
    private String id;
    @ApiModelProperty(value = "角色ID")
    private String roleId;
    @ApiModelProperty(value = "菜单ID")
    private String menuId;
    @ApiModelProperty(value = "角色")
    private SchoolRoleInfo role;
    @ApiModelProperty(value = "菜单")
    private SchoolMenuInfo menu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public SchoolRoleInfo getRole() {
        return role;
    }

    public void setRole(SchoolRoleInfo role) {
        this.role = role;
    }

    public SchoolMenuInfo getMenu() {
        return menu;
    }

    public void setMenu(SchoolMenuInfo menu) {
        this.menu = menu;
    }
}
