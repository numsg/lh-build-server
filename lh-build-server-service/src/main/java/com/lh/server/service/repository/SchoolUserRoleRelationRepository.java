package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolUserRoleRelationRepository extends JpaRepository<SchoolUserRoleRelationEntity, String> {
    /**
     * Find by user id list.
     *
     * @param userId the user id
     * @return the list
     */
    List<SchoolUserRoleRelationEntity> findByUserId(String userId);

    /**
     * Find by role id list.
     *
     * @param roleId the role id
     * @return the list
     */
    List<SchoolUserRoleRelationEntity> findByRoleId(String roleId);

    /**
     * Find by role id in list.
     *
     * @param roleIds the role ids
     * @return the list
     */
    List<SchoolUserRoleRelationEntity> findByRoleIdIn(List<String> roleIds);
}
