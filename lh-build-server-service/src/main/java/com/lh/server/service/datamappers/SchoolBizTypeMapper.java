package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolBizTypeInfo;
import com.lh.server.service.entity.SchoolBizTypeEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolBizTypeMapper {
    /**
     * Entity to model biz type.
     *
     * @param entity the entity
     * @return the biz type
     */
    SchoolBizTypeInfo entityToModel(SchoolBizTypeEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolBizTypeInfo> entitiesToModels(List<SchoolBizTypeEntity> entities);

    /**
     * Model to entity biz type entity.
     *
     * @param model the model
     * @return the biz type entity
     */
    SchoolBizTypeEntity modelToEntity(SchoolBizTypeInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolBizTypeEntity> modelsToEntities(List<SchoolBizTypeInfo> models);
}
