package com.lh.server.service.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "school_role_menu", schema = "")
public class SchoolRoleMenuRelationEntity {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    /**
     * 角色ID
     */
    @Column(name = "role_id", length = 36)
    private String roleId;
    /**
     * 菜单ID
     */
    @Column(name = "menu_id", length = 36)
    private String menuId;

    /**
     * 外键关联菜单ID
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "menu_id", insertable = false, updatable = false)
    private SchoolMenuEntity menuEntity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }


    public SchoolMenuEntity getMenuEntity() {
        return menuEntity;
    }

    public void setMenuEntity(SchoolMenuEntity menuEntity) {
        this.menuEntity = menuEntity;
    }
}
