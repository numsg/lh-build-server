package com.lh.server.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "school_data_record", schema = "")
public class SchoolDataRecordEntity {

    /**
     * 主键
     */
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private String dataid;

    /**
     * 设备ID
     */
    @Column(name = "deviceId", nullable = false)
    private String deviceId;

    /**
     * 取值 1、2、3， 1代表压力机数据记录模板，2代表万能机数据记录模板 3代表导热系数数据记录模板）
     */
    @Column(name = "datatype", nullable = false)
    private int datatype;

    /**
     * 创建时间
     */
    @Column(name = "datatime", nullable = false)
    private Date datatime;

    /**
     * (数据标签，自增长)，
     */
    @Column(name = "sno", nullable = false)
    private String sno;
    /**
     * 记录数据 JSON 格式
     */
    @Column(name = "data", columnDefinition = "LONGTEXT",nullable = false)
    private String data;

    @Column(name = "status",nullable = false)
    private  int status;

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public int getDatatype() {
        return datatype;
    }

    public void setDatatype(int datatype) {
        this.datatype = datatype;
    }

    public Date getDatatime() {
        return datatime;
    }

    public void setDatatime(Date datatime) {
        this.datatime = datatime;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
