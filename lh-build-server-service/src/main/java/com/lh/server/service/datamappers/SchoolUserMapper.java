package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolUserInfo;
import com.lh.server.service.entity.SchoolLoginEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolUserMapper {
    /**
     * Entity to model setting type info.
     *
     * @param schoolLoginEntity the setting entity
     * @return the setting type info
     */
    SchoolUserInfo entityToModel(SchoolLoginEntity schoolLoginEntity);

    /**
     * Model to entity setting type entity.
     *
     * @param schoolUserInfo the setting info
     * @return the setting type entity
     */
    SchoolLoginEntity modelToEntity(SchoolUserInfo schoolUserInfo);

    /**
     * Entities to models list.
     *
     * @param settingEntity the setting entity
     * @return the list
     */
    List<SchoolUserInfo> entitiesToModels(List<SchoolLoginEntity> settingEntity);

    /**
     * Models to entities list.
     *
     * @param schoolUserInfos the setting entity
     * @return the list
     */
    List<SchoolLoginEntity> modelsToEntities(List<SchoolUserInfo> schoolUserInfos);
}
