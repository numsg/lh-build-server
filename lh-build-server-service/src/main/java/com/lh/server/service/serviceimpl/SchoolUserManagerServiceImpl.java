package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.SchoolUserInfo;
import com.lh.server.contract.model.SchoolUserRoleRelationInfo;
import com.lh.server.contract.service.SchoolUserManagerService;
import com.lh.server.service.datamappers.SchoolUserMapper;
import com.lh.server.service.datamappers.SchooleUserRoleRelationMapper;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import com.lh.server.service.repository.SchoolUserRepository;
import com.lh.server.service.repository.SchoolUserRoleRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SchoolUserManagerServiceImpl implements SchoolUserManagerService {


    @Autowired
    private SchoolUserRepository schoolUserRepository;

    @Autowired
    private SchoolUserMapper schoolUserMapper;

    @Autowired
    private SchoolUserRoleRelationRepository schoolUserRoleRelationRepository;

    @Autowired
    private SchooleUserRoleRelationMapper schooleUserRoleRelationMapper;

    @Override
    public boolean addUser(SchoolUserInfo userInfo) {
        String uuid = UUID.randomUUID().toString();
        // 1: 增加用户和角色的关联表数据
        SchoolUserRoleRelationEntity schoolUserRoleRelationEntity = new SchoolUserRoleRelationEntity();
        schoolUserRoleRelationEntity.setId(UUID.randomUUID().toString());
        schoolUserRoleRelationEntity.setRoleId(userInfo.getRoleid());
        schoolUserRoleRelationEntity.setUserId(uuid);
        schoolUserRoleRelationRepository.save(schoolUserRoleRelationEntity);
        // 2：增加用户表的数据
        SchoolLoginEntity schoolLoginEntity = schoolUserMapper.modelToEntity(userInfo);
        schoolLoginEntity.setUserId(uuid);
        schoolUserRepository.save(schoolLoginEntity);
        return true;
    }

    @Override
    public boolean updateUser(SchoolUserInfo userInfo) {
        SchoolLoginEntity schoolLoginEntity = schoolUserRepository.findById(userInfo.getUserId()).orElse(null);
        if (schoolLoginEntity == null) {
            return false;
        } else {
            schoolLoginEntity.setUsername(userInfo.getUsername());
            schoolLoginEntity.setRoleid(userInfo.getRoleid());
            schoolLoginEntity.setRolename(userInfo.getRolename());
            schoolLoginEntity.setPassword(userInfo.getPassword());
            schoolUserRepository.save(schoolLoginEntity);

            List<SchoolUserRoleRelationEntity> schoolUserRoleRelationEntity = schoolUserRoleRelationRepository.findByUserId(userInfo.getUserId());
            if (schoolUserRoleRelationEntity != null && schoolUserRoleRelationEntity.size() == 1) {
                schoolUserRoleRelationEntity.get(0).setRoleId(userInfo.getRoleid());
                schoolUserRoleRelationRepository.save(schoolUserRoleRelationEntity.get(0));
            }
            return true;
        }
    }

    @Override
    public boolean resetPassword(SchoolUserInfo userInfo) {
        SchoolLoginEntity schoolLoginEntity = schoolUserRepository.findById(userInfo.getUserId()).orElse(null);
        if (schoolLoginEntity == null) {
            return false;
        } else {
            schoolLoginEntity.setPassword(userInfo.getPassword());
            schoolUserRepository.save(schoolLoginEntity);
            return true;
        }
    }

    @Override
    public boolean deleteUser(SchoolUserInfo userInfo) {
        // 1: 删除用户和角色的关联表数据
        List<SchoolUserRoleRelationEntity> schoolUserRoleRelationEntity = schoolUserRoleRelationRepository.findByUserId(userInfo.getUserId());
        if (schoolUserRoleRelationEntity != null && schoolUserRoleRelationEntity.size() == 1) {
            // 2：删除用户表的数据
            schoolUserRoleRelationRepository.delete(schoolUserRoleRelationEntity.get(0));
            SchoolLoginEntity schoolLoginEntity = schoolUserRepository.findById(userInfo.getUserId()).orElse(null);
            if (schoolLoginEntity == null) {
                return false;
            } else {
                schoolUserRepository.delete(schoolLoginEntity);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<SchoolUserInfo> getAllUsers() {
        List<SchoolLoginEntity> loginEntityList = schoolUserRepository.findAll();
        if (loginEntityList == null) {
            return null;
        } else {
            List<SchoolUserInfo> schoolUserInfos = new ArrayList<>();
            schoolUserInfos = schoolUserMapper.entitiesToModels(loginEntityList);
            return schoolUserInfos;
        }
    }

    @Override
    public SchoolUserInfo getById(String id) {
        SchoolLoginEntity schoolLoginEntity = schoolUserRepository.findById(id).get();
        if (schoolLoginEntity == null) {
            return null;
        } else {
            SchoolUserInfo schoolUserInfo =new SchoolUserInfo();
            schoolUserInfo = schoolUserMapper.entityToModel(schoolLoginEntity);
            return schoolUserInfo;
        }
    }

}
