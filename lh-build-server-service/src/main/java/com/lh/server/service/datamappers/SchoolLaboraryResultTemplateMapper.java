package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryResultTemplateInfo;
import com.lh.server.service.entity.SchoolLaboraryResultTemplate;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * The interface School laborary result template mapper.
 */
@Mapper(componentModel = "spring")
public interface SchoolLaboraryResultTemplateMapper {
    /**
     * Model to entity school laborary result template.
     *
     * @param resultTemplateInfo the result template info
     * @return the school laborary result template
     */
    SchoolLaboraryResultTemplate modelToEntity(SchoolLaboraryResultTemplateInfo resultTemplateInfo);

    /**
     * Entity to model school laborary result template info.
     *
     * @param resultTemplate the result template
     * @return the school laborary result template info
     */
    SchoolLaboraryResultTemplateInfo entityToModel(SchoolLaboraryResultTemplate resultTemplate);

    /**
     * Models to entities list.
     *
     * @param resultTemplateInfos the result template infos
     * @return the list
     */
    List<SchoolLaboraryResultTemplate> modelsToEntities(List<SchoolLaboraryResultTemplateInfo> resultTemplateInfos);

    /**
     * Entities to models list.
     *
     * @param resultTemplates the result templates
     * @return the list
     */
    List<SchoolLaboraryResultTemplateInfo> entitiesToModels(List<SchoolLaboraryResultTemplate> resultTemplates);
}
