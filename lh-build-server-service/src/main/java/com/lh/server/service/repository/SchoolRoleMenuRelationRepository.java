package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolRoleMenuRelationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolRoleMenuRelationRepository extends JpaRepository<SchoolRoleMenuRelationEntity, String> {
    /**
     * Find by role id list.
     *
     * @param roleId the role id
     * @return the list
     */
    List<SchoolRoleMenuRelationEntity> findByRoleId(String roleId);

    /**
     * Delete by role id int.
     *
     * @param roleId the role id
     * @return the int
     */
    @Modifying
    @Query(value = "delete from SchoolRoleMenuRelationEntity where roleId = ?1")
    int deleteByRoleId(String roleId);

}
