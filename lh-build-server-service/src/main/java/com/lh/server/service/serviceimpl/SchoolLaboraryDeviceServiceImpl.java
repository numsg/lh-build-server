package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.SchoolLaboraryDeviceGroupInfo;
import com.lh.server.contract.model.SchoolLaboraryDeviceInfo;
import com.lh.server.contract.service.SchoolLaboraryDeviceService;
import com.lh.server.service.datamappers.SchoolLaboraryDeviceGroupMapper;
import com.lh.server.service.datamappers.SchoolLaboraryDeviceMapper;
import com.lh.server.service.entity.SchoolLaboraryDevice;
import com.lh.server.service.entity.SchoolLaboraryDeviceGroup;
import com.lh.server.service.entity.SchoolLaboraryEntity;
import com.lh.server.service.repository.SchoolLaboraryDeviceGroupRepository;
import com.lh.server.service.repository.SchoolLaboraryDeviceRepository;
import com.lh.server.service.repository.SchoolLaboratoryManagerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Service
public class SchoolLaboraryDeviceServiceImpl implements SchoolLaboraryDeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolMenuServiceImpl.class);


    @Autowired
    private SchoolLaboraryDeviceGroupRepository schoolLaboraryDeviceGroupRepository;

    @Autowired
    private SchoolLaboraryDeviceGroupMapper schoolLaboraryDeviceGroupMapper;

    @Autowired
    private SchoolLaboraryDeviceRepository schoolLaboraryDeviceRepository;

    @Autowired
    private SchoolLaboraryDeviceMapper schoolLaboraryDeviceMapper;

    @Autowired
    private SchoolLaboratoryManagerRepository schoolLaboratoryManagerRepository;

    // 增加设备分组
    @Override
    public boolean addDeviceGroup(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        SchoolLaboraryDeviceGroup schoolLaboraryDeviceGroup = schoolLaboraryDeviceGroupMapper.modelToEntity((schoolLaboraryDeviceGroupInfo));
        schoolLaboraryDeviceGroup.setId(UUID.randomUUID().toString());
        schoolLaboraryDeviceGroup.setCreatetime(new Date());
        schoolLaboraryDeviceGroup.setInterface_type("0");
        schoolLaboraryDeviceGroupRepository.save(schoolLaboraryDeviceGroup);
        return true;
    }

    // 编辑设备组信息
    @Override
    public boolean modifyDeviceGroup(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        SchoolLaboraryDeviceGroup schoolLaboraryDeviceGroup = schoolLaboraryDeviceGroupRepository.findById(schoolLaboraryDeviceGroupInfo.getId()).orElse(null);
        if (schoolLaboraryDeviceGroup == null) {
            return false;
        } else {
            schoolLaboraryDeviceGroup = schoolLaboraryDeviceGroupMapper.modelToEntity(schoolLaboraryDeviceGroupInfo);
            schoolLaboraryDeviceGroup.setCreatetime(new Date());
            schoolLaboraryDeviceGroupRepository.save(schoolLaboraryDeviceGroup);
            return true;
        }
    }

    // 删除设备组信息
    @Override
    public boolean deleteDeviceGrouop(SchoolLaboraryDeviceGroupInfo schoolLaboraryDeviceGroupInfo) {
        SchoolLaboraryDeviceGroup schoolLaboraryDeviceGroup = schoolLaboraryDeviceGroupRepository.findById(schoolLaboraryDeviceGroupInfo.getId()).orElse(null);
        if (schoolLaboraryDeviceGroup == null) {
            return false;
        } else {
            schoolLaboraryDeviceGroupRepository.delete(schoolLaboraryDeviceGroup);
            return true;
        }
    }

    // 获取分组消息
    @Override
    public List<SchoolLaboraryDeviceGroupInfo> getALLDeviceGroup() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String ip = request.getRemoteAddr().trim();
        String labId = "";
        List<SchoolLaboraryEntity> schoolLaboraryEntities = schoolLaboratoryManagerRepository.findBySchoolLaboaryComputerIpsContaining(ip);
        for (int i = 0; i < schoolLaboraryEntities.size(); i++) {
            String[] strIps = schoolLaboraryEntities.get(i).getSchoolLaboaryComputerIps().split(",");
            for (int j = 0; j < strIps.length; j++) {
                if (strIps[j].equals(ip)) {
                    labId = schoolLaboraryEntities.get(i).getId();
                    break;
                }
            }
            break;
        }
        List<SchoolLaboraryDeviceGroup> entities = schoolLaboraryDeviceGroupRepository.findByLaboraryid(labId);
        if (CollectionUtils.isEmpty(entities)) return Collections.emptyList();
        return schoolLaboraryDeviceGroupMapper.entitiesToModels(entities);
    }

    // 根据设备组ID获取所有设备集合信息
    @Override
    public List<SchoolLaboraryDeviceInfo> GetAllDeviceInfoByDeviceGroupId(String deviceGroupId) {
        List<SchoolLaboraryDevice> schoolLaboraryDeviceList = schoolLaboraryDeviceRepository.findAllListLaboraryDeviceGroup(deviceGroupId);
        return schoolLaboraryDeviceMapper.entitiesToModels(schoolLaboraryDeviceList);
    }

    // 增加设备信息
    @Override
    public boolean addDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        SchoolLaboraryDevice schoolLaboraryDevice = schoolLaboraryDeviceMapper.modelToEntity(schoolLaboraryDeviceInfo);
        schoolLaboraryDevice.setId(UUID.randomUUID().toString());
        schoolLaboraryDevice.setCreatetime(new Date());
        schoolLaboraryDeviceRepository.save(schoolLaboraryDevice);
        return true;
    }

    // 编辑设备信息
    @Override
    public boolean modifyDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        SchoolLaboraryDevice schoolLaboraryDevice = schoolLaboraryDeviceRepository.findById(schoolLaboraryDeviceInfo.getId()).orElse(null);
        if (schoolLaboraryDevice == null) {
            return false;
        } else {
            schoolLaboraryDevice = schoolLaboraryDeviceMapper.modelToEntity(schoolLaboraryDeviceInfo);
            schoolLaboraryDevice.setCreatetime(new Date());
            schoolLaboraryDeviceRepository.save(schoolLaboraryDevice);
            return true;
        }
    }

    // 删除设备信息
    @Override
    public boolean deleteDevice(SchoolLaboraryDeviceInfo schoolLaboraryDeviceInfo) {
        SchoolLaboraryDevice schoolLaboraryDevice = schoolLaboraryDeviceRepository.findById(schoolLaboraryDeviceInfo.getId()).orElse(null);
        if (schoolLaboraryDevice == null) {
            return false;
        } else {
            schoolLaboraryDeviceRepository.delete(schoolLaboraryDevice);
            return true;
        }
    }
}
