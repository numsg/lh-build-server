package com.lh.server.service.serviceimpl;

import com.lh.server.common.util.StringUtil;
import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboratoryInfo;
import com.lh.server.contract.service.SchoolLaboratoryManagerService;
import com.lh.server.service.datamappers.SchoolLaboratoryMapper;
import com.lh.server.service.entity.*;
import com.lh.server.service.repository.SchoolLaboratoryComputerRepository;
import com.lh.server.service.repository.SchoolLaboratoryManagerRepository;
import com.lh.server.service.utils.SpecificationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class SchoolLaboratoryManagerServiceImpl implements SchoolLaboratoryManagerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolLaboratoryManagerServiceImpl.class);


    @Autowired
    private SchoolLaboratoryManagerRepository schoolLaboratoryManagerRepository;

    @Autowired
    private SchoolLaboratoryMapper schoolLaboratoryMapper;

    @Autowired
    private SchoolLaboratoryComputerRepository schoolLaboratoryComputerRepository;

    /**
     * 返回所以实验室列表，有可能需要分页
     *
     * @return
     */
    @Override
    public List<SchoolLaboratoryInfo> getAllSchoolLaboratory() {
        Sort sort = new Sort(Sort.Direction.DESC, "weight");
        List<SchoolLaboraryEntity> entities = schoolLaboratoryManagerRepository.findAll(sort);
        if (CollectionUtils.isEmpty(entities)) return Collections.emptyList();
        return schoolLaboratoryMapper.entitiesToModels(entities);
    }

    @Override
    public PageInfo<SchoolLaboratoryInfo> getSchoolLaboratoryByPage(int pageNum, int size, String keyword) {
        Sort sort = new Sort(Sort.Direction.DESC, "weight");
        Pageable pageable =PageRequest.of(pageNum,size,sort);
        Specification specification = createSpecification(keyword);
        Page<SchoolLaboraryEntity> page = schoolLaboratoryManagerRepository.findAll(specification,pageable);
        List<SchoolLaboratoryInfo> schoolLaboratoryInfos=schoolLaboratoryMapper.entitiesToModels(page.getContent());
        PageInfo<SchoolLaboratoryInfo> pageInfo=new PageInfo<SchoolLaboratoryInfo>(schoolLaboratoryInfos,page.getTotalElements(),page.getTotalPages());
        return pageInfo;
    }

    @Override
    public boolean addSchoolLaboratory(SchoolLaboratoryInfo schoolLaboratoryInfo) {
        try {
            SchoolLaboraryEntity schoolLaboraryEntity = schoolLaboratoryMapper.modelToEntity(schoolLaboratoryInfo);
            String laboraryId = UUID.randomUUID().toString();
            schoolLaboraryEntity.setId(laboraryId);
            schoolLaboraryEntity.setCreatetime(new Date());
            schoolLaboratoryManagerRepository.save(schoolLaboraryEntity);
            return true;
        } catch (Exception e) {
            LOGGER.error("SchoolLaboratoryManagerServiceImpl  addSchoolLaboratory ex  {}", e.toString());
            return false;
        }
    }

    /**
     * 根据实验室ID删除实验室信息
     *
     * @param id 实验室ID
     */
    @Override
    public boolean deleteById(String id) {
        if (StringUtil.isEmpty(id)) return false;
        try {
            schoolLaboratoryManagerRepository.deleteById(id);
            return true;

        } catch (Exception e) {
            LOGGER.error(" SchoolLaboratoryManagerServiceImpl deleteById exception: [{}]", e.getMessage(), e);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    /**
     * 修改实验信息
     *
     * @param schoolLaboratoryInfo 实验室信息
     * @return
     */
    @Override
    public boolean modifySchoolLaboratory(SchoolLaboratoryInfo schoolLaboratoryInfo) {
        SchoolLaboraryEntity schoolLaboraryEntity = schoolLaboratoryManagerRepository.findById(schoolLaboratoryInfo.getId()).orElse(null);
        if (schoolLaboraryEntity == null) {
            return false;
        } else {
            schoolLaboraryEntity = schoolLaboratoryMapper.modelToEntity(schoolLaboratoryInfo);
            schoolLaboratoryManagerRepository.save(schoolLaboraryEntity);
            return true;
        }
    }


    private Specification<SchoolLaboraryEntity> createSpecification(String keyWord) {
        SpecificationUtil specificationUtil = new SpecificationUtil<SchoolLaboraryEntity>();
        return specificationUtil.createSpecification("laborary_name", keyWord);
    }
}
