package com.lh.server.service.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 实验室记录项目详情
 */
@Entity
@Table(name = "school_laborary_record_project", schema = "")
public class SchoolLaboraryRecordProject {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    // 实验室记录主键id
    @Column(name = "record_id",length = 36, nullable = false)
    private String recordId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "record_id", nullable = false,insertable=false,updatable = false)
    private SchoolLaboraryRecord schoolLaboraryRecord;

    // 项目名称
    @Column(name = "project_name", length = 120,nullable = false)
    private String projectName;
    // 修改时间
    @Column(name = "modify_time")
    private Date modifyTime;
    // 修改人
    @Column(name = "modify_name", length = 120)
    private String modifyName;
    // 提交时间
    @Column(name = "submit_time")
    private Date submitTime;
    // 提交人
    @Column(name = "submit_name", length = 120)
    private String submitName;
    // 审核时间
    @Column(name = "verify_time")
    private Date verifyTime;
    // 审核状态（0-未审核，1-审核中，2-已审核）
    @Column(name = "verify_result", columnDefinition = "int default 0")
    private int verifyResult;
    // 审核姓名
    @Column(name = "verify_name", length = 120)
    private String verifyName;
    // 批准时间
    @Column(name = "approval_time")
    private Date approvalTime;
    // 批准状态0-未批准，1-批准中，2-已批准）
    @Column(name = "approval_result",columnDefinition = "int default 0")
    private int approvalResult;
    // 批准人姓名
    @Column(name = "approval_name", length = 120)
    private String approvalName;
    // 结果模板主键id
    @Column(name = "result_id", length = 120)
    private String resultId;
    // 监控分组id
    @Column(name = "device_group_id", length = 120)
    private String deviceGroupId;
    // 视频路径数组
    @Column(name = "history_video_urls", length = 1200)
    private String historyVideoUrls;
    // 真实实验室模板内容
    @Column(name = "template_record_content", columnDefinition = "TEXT")
    private String templateRecordContent;
    //真实实验室模板数据
    @Column(name = "template_record_data", columnDefinition = "TEXT")
    private String templateRecordData;


    public SchoolLaboraryRecordProject() {
        // 无参构造
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public SchoolLaboraryRecord getSchoolLaboraryRecord() {
        return schoolLaboraryRecord;
    }

    public void setSchoolLaboraryRecord(SchoolLaboraryRecord schoolLaboraryRecord) {
        this.schoolLaboraryRecord = schoolLaboraryRecord;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyName() {
        return modifyName;
    }

    public void setModifyName(String modifyName) {
        this.modifyName = modifyName;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getSubmitName() {
        return submitName;
    }

    public void setSubmitName(String submitName) {
        this.submitName = submitName;
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public int getVerifyResult() {
        return verifyResult;
    }

    public void setVerifyResult(int verifyResult) {
        this.verifyResult = verifyResult;
    }

    public String getVerifyName() {
        return verifyName;
    }

    public void setVerifyName(String verifyName) {
        this.verifyName = verifyName;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public int getApprovalResult() {
        return approvalResult;
    }

    public void setApprovalResult(int approvalResult) {
        this.approvalResult = approvalResult;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getDeviceGroupId() {
        return deviceGroupId;
    }

    public void setDeviceGroupId(String deviceGroupId) {
        this.deviceGroupId = deviceGroupId;
    }


    public String getHistoryVideoUrls() {
        return historyVideoUrls;
    }

    public void setHistoryVideoUrls(String historyVideoUrls) {
        this.historyVideoUrls = historyVideoUrls;
    }

    public String getTemplateRecordContent() {
        return templateRecordContent;
    }

    public void setTemplateRecordContent(String templateRecordContent) {
        this.templateRecordContent = templateRecordContent;
    }

    public String getTemplateRecordData() {
        return templateRecordData;
    }

    public void setTemplateRecordData(String templateRecordData) {
        this.templateRecordData = templateRecordData;
    }
}
