package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryDeviceInfo;
import com.lh.server.service.entity.SchoolLaboraryDevice;
import org.mapstruct.Mapper;

import java.util.List;

// 实验设备详情映射
@Mapper(componentModel = "spring")
public interface SchoolLaboraryDeviceMapper {
    /**
     * Entity to model menu.
     *
     * @param entity the entity
     * @return the menu
     */
    SchoolLaboraryDeviceInfo entityToModel(SchoolLaboraryDevice entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolLaboraryDeviceInfo> entitiesToModels(List<SchoolLaboraryDevice> entities);

    /**
     * Model to entity menu entity.
     *
     * @param model the model
     * @return the menu entity
     */
    SchoolLaboraryDevice modelToEntity(SchoolLaboraryDeviceInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolLaboraryDevice> modelsToEntities(List<SchoolLaboraryDeviceInfo> models);
}
