package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryRecordInfo;
import com.lh.server.service.entity.SchoolLaboraryRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * The interface School laborary record mapper.
 */
@Mapper(componentModel = "spring")
public interface SchoolLaboraryRecordMapper {
    /**
     * Model to entity school laborary record.
     *
     * @param laboraryRecordInfo the laborary record info
     * @return the school laborary record
     */
    SchoolLaboraryRecord modelToEntity(SchoolLaboraryRecordInfo laboraryRecordInfo);

    /**
     * Entity to model school laborary record info.
     *
     * @param laboraryRecord the laborary record
     * @return the school laborary record info
     */
    SchoolLaboraryRecordInfo entityToModel(SchoolLaboraryRecord laboraryRecord);

    /**
     * Models to entities list.
     *
     * @param laboraryRecordInfos the laborary record infos
     * @return the list
     */
    List<SchoolLaboraryRecord> modelsToEntities(List<SchoolLaboraryRecordInfo> laboraryRecordInfos);

    /**
     * Entities to models list.
     *
     * @param laboraryRecords the laborary records
     * @return the list
     */
    List<SchoolLaboraryRecordInfo> entitiesToModels(List<SchoolLaboraryRecord> laboraryRecords);
}
