package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryRecord;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolUserRepository extends JpaRepository<SchoolLoginEntity, String> {
    /**
     * Find by role id list.
     *
     * @param roleid the role id
     * @return the list
     */
    List<SchoolLoginEntity> findByRoleid(String roleid);

}
