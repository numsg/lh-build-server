package com.lh.server.service.serviceimpl;

import com.gsafety.java.common.utils.JsonUtil;
import com.lh.server.common.exception.BusinessException;
import com.lh.server.common.exception.ErrorCode;
import com.lh.server.common.util.DateUtil;
import com.lh.server.common.util.StringUtil;
import com.lh.server.contract.model.*;
import com.lh.server.contract.service.SchoolLaboraryRecordService;
import com.lh.server.service.datamappers.SchoolLaboraryRecordMapper;
import com.lh.server.service.datamappers.SchoolLaboraryRecordProjectMapper;
import com.lh.server.service.entity.SchoolLaboraryDeviceGroup;
import com.lh.server.service.entity.SchoolLaboraryEntity;
import com.lh.server.service.entity.SchoolLaboraryRecord;
import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import com.lh.server.service.repository.SchoolLaboraryDeviceGroupRepository;
import com.lh.server.service.repository.SchoolLaboraryRecordProjectRepository;
import com.lh.server.service.repository.SchoolLaboraryRecordRepository;
import com.lh.server.service.repository.SchoolLaboratoryManagerRepository;
import com.lh.server.service.utils.SpecificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * The type School laborary record service.
 */
@Service
@Transactional
public class SchoolLaboraryRecordServiceImpl implements SchoolLaboraryRecordService {
    @Autowired
    private SchoolLaboraryRecordMapper recordMapper;
    @Autowired
    private SchoolLaboraryRecordRepository recordRepository;

    @Autowired
    private SchoolLaboraryRecordProjectMapper recordProjectMapper;

    @Autowired
    private SchoolLaboratoryManagerRepository schoolLaboratoryManagerRepository;

    @Autowired
    private SchoolLaboraryRecordProjectRepository recordProjectRepository;

    @Autowired
    private SchoolLaboraryDeviceGroupRepository deviceGroupRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private static Logger logger = LoggerFactory.getLogger(SchoolLaboraryRecordServiceImpl.class);

    /**
     * Gets all records.
     *
     * @return the all records
     */
    @Override
    public PageQueryResultInfo<SchoolLaboraryRecordInfo> pageQueryRecords(StatusConditionQueryInfo statusConditionQueryInfo, String prifex) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String ip = request.getRemoteAddr();
        List<SchoolLaboraryEntity> schoolLaboraryEntities = schoolLaboratoryManagerRepository.findAll();
        SchoolLaboraryEntity schoolLaboraryEntity = new SchoolLaboraryEntity();

        for(int i = 0; i< schoolLaboraryEntities.size() ;i++) {
            if (StringUtil.isNotEmpty(schoolLaboraryEntities.get(i).getSchoolLaboaryComputerIps()) && schoolLaboraryEntities.get(i).getSchoolLaboaryComputerIps().contains(ip)) {
                String[] strIps = schoolLaboraryEntities.get(i).getSchoolLaboaryComputerIps().split(",");
                for(int j= 0;j<strIps.length; j++){
                    if(strIps[j].equals(ip)){
                        schoolLaboraryEntity = schoolLaboraryEntities.get(i);
                        break;
                    }
                }
                break;
            }
        }

        PageQueryResultInfo<SchoolLaboraryRecordInfo> result = new PageQueryResultInfo<>();
        Sort sort = new Sort(Sort.Direction.DESC, "submitTime");
        Pageable pageable = PageRequest.of(statusConditionQueryInfo.getPageNumber() - 1, statusConditionQueryInfo.getPageSize(), sort);
        Specification specification = createSpecification(statusConditionQueryInfo.getCenterCode(), schoolLaboraryEntity, prifex);
        Page<SchoolLaboraryRecord> page = recordRepository.findAll(specification, pageable);
        result.setRecords(recordMapper.entitiesToModels(page.getContent()));
        result.setTotal((int) page.getTotalElements());
        result.getRecords().forEach(recordInfo ->
                recordInfo.setRecordProjectInfos(recordProjectMapper.entitiesToModels(recordProjectRepository.findByRecordId(recordInfo.getId())))
        );
        return result;
    }

    private Specification<SchoolLaboraryRecord> createSpecification(String keyWord,  SchoolLaboraryEntity schoolLaboraryEntity, String prifex) {

        SpecificationUtil specificationUtil = new SpecificationUtil<SchoolLaboraryRecord>();
        Specification specification1 = specificationUtil.createSpecification("centerCode", keyWord);
        String laboraryId = "";
        if(prifex != "all"){
            laboraryId = "@";
            if(schoolLaboraryEntity.getId() != null){
                laboraryId = schoolLaboraryEntity.getId();
            }
        }
        Specification specification2 = specificationUtil.createSpecification("laboraryId", laboraryId);
        return specification1.and(specification2);
    }

    /**
     * Gets one record by id.
     *
     * @param id the id
     * @return the one record by id
     */
    @Override
    public SchoolLaboraryRecordInfo getOneRecordById(String id) {
        if (!recordRepository.existsById(id)) {
            return new SchoolLaboraryRecordInfo();
        }
        SchoolLaboraryRecordInfo result = recordMapper.entityToModel(recordRepository.getOne(id));
        List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordId(id);
        result.setRecordProjectInfos(recordProjectMapper.entitiesToModels(recordProjects));
        return result;
    }

    /**
     * Judge is repeat when add boolean.
     *
     * @param centerCode  the center code
     * @param projectName the project name
     * @return the boolean
     */
    @Override
    public Boolean judgeIsRepeatWhenAdd(String centerCode, String projectName) {
        boolean result = false;
        List<SchoolLaboraryRecord> records = recordRepository.findByCenterCode(centerCode);
        if (!records.isEmpty() && !recordProjectRepository.findByRecordIdAndProjectName(records.get(0).getId(), projectName).isEmpty()) {
            result = true;
        }
        return result;
    }

    /**
     * Judge is repeat when add boolean.
     *
     * @param recordId  recordId
     * @param projectName the project name
     * @return the boolean
     */
    @Override
    public Boolean judgeIsRepeatWhenAddRecord(String recordId, String projectName) {
        boolean result = false;
        SchoolLaboraryRecord schoolLaboraryRecord = recordRepository.findById(recordId).get();
        if(schoolLaboraryRecord!= null){
            if(!recordProjectRepository.findByRecordIdAndProjectName(schoolLaboraryRecord.getId(), projectName).isEmpty()){
                result = true;
            }
        }
        return result;
    }

    /**
     * Add one record school laborary record info.
     *
     * @param addRecordInfo the record info
     * @return the school laborary record info
     */
    @Override
    public AddRecordInfo addOneRecord(AddRecordInfo addRecordInfo) {
        Date time = new Date();
        SchoolLaboraryRecordInfo recordInfo = addRecordInfo.getRecordInfo();
        SchoolLaboraryRecordProjectInfo recordProjectInfo = addRecordInfo.getRecordProjectInfo();

        if (!deviceGroupRepository.existsById(recordProjectInfo.getDeviceGroupId())) {
            throw new BusinessException(ErrorCode.DATA_NOT_FOUND);
        }

        SchoolLaboraryDeviceGroup deviceGroup = deviceGroupRepository.getOne(recordProjectInfo.getDeviceGroupId());

        List<SchoolLaboraryRecord> records = recordRepository.findByCenterCodeAndLaboraryId(recordInfo.getCenterCode(), deviceGroup.getLaborary_id());
        if (records.isEmpty()) {
            // 不存在，新增记录和项目详情
            recordInfo.setId(UUID.randomUUID().toString());
            recordInfo.setLaboraryId(deviceGroup.getLaborary_id());
            recordInfo.setLaboraryName(deviceGroup.getLaborary_name());
            recordInfo.setProjectNum(1);
        } else {
            // 已存在中心编号和实验室相同的记录，只更新提交时间和项目数量，然后判断项目是否存在
            SchoolLaboraryRecord record = records.get(0);
            recordInfo = recordMapper.entityToModel(record);
            List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordIdAndProjectName(recordInfo.getId(), recordProjectInfo.getProjectName());
            if (!recordProjects.isEmpty()) {
                // 记录已存在
                throw new BusinessException(ErrorCode.DATA_ALREADY_EXISTS);
            }
            List<SchoolLaboraryRecordProject> list = recordProjectRepository.findByRecordId(recordInfo.getId());
            recordInfo.setProjectNum(list.size() + 1);
        }

        // 添加或更新实验记录
        recordInfo.setSubmitTime(time);
        recordInfo.setVerifyResult(0);
        recordInfo.setApprovalResult(0);
        recordRepository.save(recordMapper.modelToEntity(recordInfo));
        recordRepository.flush();

        // 添加项目记录
        recordProjectInfo.setRecordId(recordInfo.getId());
        recordProjectInfo.setId(UUID.randomUUID().toString());
        recordProjectInfo.setSubmitTime(time);
        recordProjectRepository.save(recordProjectMapper.modelToEntity(recordProjectInfo));

        addRecordInfo.setRecordInfo(recordInfo);
        addRecordInfo.setRecordProjectInfo(recordProjectInfo);
        return addRecordInfo;
    }

    /**
     * Update one record school laborary record info.
     *
     * @param recordInfo the record info
     * @return the school laborary record info
     */
    @Override
    public SchoolLaboraryRecordInfo updateOneRecord(SchoolLaboraryRecordInfo recordInfo) {
        SchoolLaboraryRecord schoolLaboraryRecord = recordRepository.findById(recordInfo.getId()).orElse(null);
        if (schoolLaboraryRecord == null) {
            return null;
        }
        SchoolLaboraryRecord record = recordMapper.modelToEntity(recordInfo);
        schoolLaboraryRecord = record;
        recordRepository.save(schoolLaboraryRecord);
        return recordMapper.entityToModel(schoolLaboraryRecord);
    }

    /**
     * Judge center code is repeat boolean.
     *
     * @param centerCode the center code
     * @return the boolean
     */
    @Override
    public Boolean judgeCenterCodeIsRepeat(String centerCode) {
        boolean result = false;
        if (!recordRepository.findByCenterCode(centerCode).isEmpty()) {
            result = true;
        }
        return result;
    }

    /**
     * Delete one record boolean.
     *
     * @param id the id
     * @return the boolean
     */
    @Override
    public Boolean deleteOneRecord(String id) {
        boolean result = false;
        if (recordRepository.existsById(id)) {
            List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordId(id);
            if (!recordProjects.isEmpty()) {
                recordProjectRepository.deleteAll(recordProjects);
                recordProjectRepository.flush();
            }
            recordRepository.deleteById(id);
            result = true;
        }
        return result;
    }

    /**
     * Gets records by verify status.
     *
     * @param conditionQueryInfo the condition query info
     * @return the records by verify status
     */
    @Override
    public PageQueryResultInfo<SchoolLaboraryRecordInfo> getRecordsByVerifyStatus(StatusConditionQueryInfo conditionQueryInfo) {
        String baseSql = " FROM school_laborary_record";
        if (conditionQueryInfo.getStatus() == 1) {
            // 已审核
            baseSql += " where verify_result=" + conditionQueryInfo.getStatus();
        } else {
            // 未审核(包含审核中)
            baseSql += " where verify_result!=1";
        }
        String sql = setBaseSql(conditionQueryInfo.getCenterCode(), conditionQueryInfo.getLaboraryId(), baseSql);
        if (conditionQueryInfo.getStatus() == 1) {
            sql += " order by approval_time desc ";
        } else {
            sql += " order by submit_time desc ";
        }
        return queryLaboraryRecord(conditionQueryInfo.getPageNumber(), conditionQueryInfo.getPageSize(), sql);
    }

    /**
     * Gets verified records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the verified records
     */
    @Override
    public PageQueryResultInfo<SchoolLaboraryRecordInfo> getVerifiedRecords(StatusConditionQueryInfo conditionQueryInfo) {
        PageQueryResultInfo<SchoolLaboraryRecordInfo> result = new PageQueryResultInfo<>();

        List<SchoolLaboraryRecordInfo> recordInfos = new ArrayList<>();
        List<SchoolLaboraryRecord> records = recordRepository.findAll();
        records.forEach(record -> {
            List<SchoolLaboraryRecordProject> recordProjects1 = recordProjectRepository.findByRecordId(record.getId());
            List<SchoolLaboraryRecordProject> recordProjects2 = recordProjectRepository.findByRecordIdAndVerifyResult(record.getId(), 1);

            if (isOk(recordProjects1, recordProjects2, conditionQueryInfo, record)) {
                SchoolLaboraryRecordInfo recordInfo = recordMapper.entityToModel(record);
                recordInfo.setRecordProjectInfos(recordProjectMapper.entitiesToModels(recordProjects1));
                recordInfos.add(recordInfo);
            }

        });
        result.setRecords(recordInfos);
        result.setTotal(recordInfos.size());
        return result;
    }

    /**
     * Gets not approval records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the not approval records
     */
    @Override
    public PageQueryResultInfo<SchoolLaboraryRecordInfo> getRecordsByApprovalStatus(StatusConditionQueryInfo conditionQueryInfo) {
        String baseSql = " FROM school_laborary_record where verify_result=1";
        if (conditionQueryInfo.getStatus() == 1) {
            // 已批准
            baseSql += " and approval_result=" + conditionQueryInfo.getStatus();
        } else {
            // 未批准(包含批准中)
            baseSql += " and approval_result!=1";
        }
        String sql = setBaseSql(conditionQueryInfo.getCenterCode(), conditionQueryInfo.getLaboraryId(), baseSql);
        if (conditionQueryInfo.getStatus() == 1) {
            sql += " order by approval_time desc ";
        } else {
            sql += " order by approval_time desc ";
        }

        return queryLaboraryRecord(conditionQueryInfo.getPageNumber(), conditionQueryInfo.getPageSize(), sql);

    }

    /**
     * Gets approved records.
     *
     * @param conditionQueryInfo the condition query info
     * @return the approved records
     */
    @Override
    public PageQueryResultInfo<SchoolLaboraryRecordInfo> getApprovedRecords(StatusConditionQueryInfo conditionQueryInfo) {
        PageQueryResultInfo<SchoolLaboraryRecordInfo> result = new PageQueryResultInfo<>();

        List<SchoolLaboraryRecordInfo> recordInfos = new ArrayList<>();
        List<SchoolLaboraryRecord> records = recordRepository.findAll();
        records.forEach(record -> {
            List<SchoolLaboraryRecordProject> recordProjects1 = recordProjectRepository.findByRecordId(record.getId());
            List<SchoolLaboraryRecordProject> recordProjects2 = recordProjectRepository.findByRecordIdAndApprovalResult(record.getId(), 1);

            if (isOk(recordProjects1, recordProjects2, conditionQueryInfo, record)) {
                SchoolLaboraryRecordInfo recordInfo = recordMapper.entityToModel(record);
                recordInfo.setRecordProjectInfos(recordProjectMapper.entitiesToModels(recordProjects1));
                recordInfos.add(recordInfo);
            }
        });
        result.setRecords(recordInfos);
        result.setTotal(recordInfos.size());
        return result;
    }


    private boolean isOk(List<SchoolLaboraryRecordProject> recordProjects1, List<SchoolLaboraryRecordProject> recordProjects2, StatusConditionQueryInfo conditionQueryInfo, SchoolLaboraryRecord record) {
        boolean result = true;
        if (recordProjects1.isEmpty() || recordProjects1.size() != recordProjects2.size()) {
            result = false;
        }
        if (StringUtil.isNotEmpty(conditionQueryInfo.getLaboraryId()) && !record.getLaboraryId().equals(conditionQueryInfo.getLaboraryId())) {
            result = false;
        }

        if (StringUtil.isNotEmpty(conditionQueryInfo.getCenterCode()) && !record.getCenterCode().equals(conditionQueryInfo.getCenterCode())) {
            result = false;
        }
        return result;
    }


    /**
     * set Base Sql
     *
     * @param centerCode center Code
     * @param laboraryId laborary Id
     * @return String
     */
    private String setBaseSql(String centerCode, String laboraryId, String sql) {
        String result = sql;
        if (StringUtil.isNotEmpty(centerCode)) {
            result += " and center_code like '%" + centerCode + "%'";
        }
        if (StringUtil.isNotEmpty(laboraryId)) {
            result += " and laborary_id='" + laboraryId + "'";
        }
        return result;
    }


    private PageQueryResultInfo<SchoolLaboraryRecordInfo> queryLaboraryRecord(int pageNumber, int pageSize, String sql) {
        PageQueryResultInfo<SchoolLaboraryRecordInfo> result = new PageQueryResultInfo<>();
        if (pageNumber <= 0 || pageSize <= 0) {
            throw new BusinessException(ErrorCode.PARAM_IS_INVALID);
        }

        try {
            // 先查询记录总数
            Query queryCount = entityManager.createNativeQuery("SELECT count(*) " + sql);
            int total = Integer.parseInt(queryCount.getResultList().get(0).toString());
            result.setTotal(total);

            if ((pageNumber - 1) * pageSize >= total) {
                return result;
            }
            Query queryData = entityManager.createNativeQuery("SELECT * " + sql + " LIMIT " + (pageNumber - 1) * pageSize + "," + pageSize);
            List<SchoolLaboraryRecord> records = new ArrayList<>();

            queryData.getResultList().forEach(object -> {
                List list = JsonUtil.fromJson(JsonUtil.toJson(object), List.class);
                SchoolLaboraryRecord record = new SchoolLaboraryRecord();
                record.setId(list.get(0).toString());
                if (list.get(1) != null && list.get(1).toString().length() > 0) {
                    record.setCenterCode(list.get(1).toString());
                }
                if (list.get(2) != null && list.get(2).toString().length() > 0) {
                    record.setCenterName(list.get(2).toString());
                }
                if (list.get(3) != null && list.get(3).toString().length() > 0) {
                    record.setLaboraryId(list.get(3).toString());
                }
                if (list.get(4) != null && list.get(4).toString().length() > 0) {
                    record.setLaboraryName(list.get(4).toString());
                }
                if (list.get(5) != null && list.get(5).toString().length() > 0) {
                    record.setModifyName(list.get(5).toString());
                }
                if (list.get(6) != null && list.get(6).toString().length() > 0) {
                    record.setModifyTime(DateUtil.stringFormat(list.get(6).toString()));
                }
                if (list.get(7) != null && list.get(7).toString().length() > 0) {
                    record.setProjectNum(Integer.parseInt(list.get(7).toString()));
                }
                if (list.get(8) != null && list.get(8).toString().length() > 0) {
                    record.setSubmitTime(DateUtil.stringFormat(list.get(8).toString()));
                }
                if (list.get(9) != null && list.get(9).toString().length() > 0) {
                    record.setApprovalResult(Integer.parseInt(list.get(9).toString()));
                }
                if (list.get(10) != null && list.get(10).toString().length() > 0) {
                    record.setVerifyResult(Integer.parseInt(list.get(10).toString()));
                }
                if (list.get(11) != null && list.get(11).toString().length() > 0) {
                    record.setApprovalTime(DateUtil.stringFormat(list.get(11).toString()));
                }
                if (list.get(12) != null && list.get(12).toString().length() > 0) {
                    record.setVerifyTime(DateUtil.stringFormat(list.get(12).toString()));
                }
                records.add(record);
            });
            result.setRecords(recordMapper.entitiesToModels(records));

            result.getRecords().forEach(recordInfo -> {
                List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordId(recordInfo.getId());
                recordInfo.setRecordProjectInfos(recordProjectMapper.entitiesToModels(recordProjects));
            });

        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

}
