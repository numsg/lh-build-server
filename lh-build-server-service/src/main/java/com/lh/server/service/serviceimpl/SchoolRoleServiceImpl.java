package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.SchoolLaboraryRecordInfo;
import com.lh.server.contract.model.SchoolRoleInfo;
import com.lh.server.contract.model.SchoolUserRoleRelationInfo;
import com.lh.server.contract.service.SchoolRoleService;
import com.lh.server.service.datamappers.SchoolRoleMapper;
import com.lh.server.service.datamappers.SchooleUserRoleRelationMapper;
import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.entity.SchoolRoleEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import com.lh.server.service.repository.SchoolRoleRepository;
import com.lh.server.service.repository.SchoolUserRepository;
import com.lh.server.service.repository.SchoolUserRoleRelationRepository;
import com.lh.server.common.constant.LogMsg;
import com.lh.server.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@Transactional
public class SchoolRoleServiceImpl implements SchoolRoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolRoleServiceImpl.class);
    
    @Autowired
    SchoolRoleRepository roleRepository;

    @Autowired
    SchoolUserRepository userRepository;

    @Autowired
    SchoolUserRoleRelationRepository userRoleRelationRepository;

    @Autowired
    SchoolRoleMapper roleMapper;

    @Autowired
    SchooleUserRoleRelationMapper userRoleRelationMapper;

    /**
     * 获取所有角色信息
     *
     * @return
     */
    @Override
    public List<SchoolRoleInfo> getAllRoles() {
        List<SchoolRoleEntity> roleEntities = roleRepository.findAll();
        if (CollectionUtils.isEmpty(roleEntities))
            return Collections.emptyList();
        return roleMapper.entitiesToModels(roleEntities);
    }

    /**
     * 配置用户角色信息
     *
     * @param userRoleRelations
     * @return
     */
    @Override
    public Boolean configUserRoles(List<SchoolUserRoleRelationInfo> userRoleRelations) {
        if (CollectionUtils.isEmpty(userRoleRelations))
            return false;
        List<SchoolUserRoleRelationEntity> existRelationEntities = userRoleRelationRepository.findByUserId(userRoleRelations.get(0).getUserId());
        userRoleRelationRepository.deleteAll(existRelationEntities);
        List<SchoolUserRoleRelationEntity> entities = userRoleRelationMapper.modelsToEntities(userRoleRelations);
        userRoleRelationRepository.saveAll(entities);
        return true;
    }

    @Override
    public Map<String, Object> addRole(SchoolRoleInfo role) {
        Map<String, Object> map = new HashMap<>();
        map.put(LogMsg.SUCCESS, false);
        if (role == null) {
            map.put(LogMsg.ERROR_CODE, "1001");
            map.put(LogMsg.MESSAGE, "param is empty");
            return map;
        }
        try {
            if (roleExist(role)) {
                map.put(LogMsg.ERROR_CODE, "1003");
                map.put(LogMsg.MESSAGE, "name exist");
                LOGGER.error("addRole failed: 已存在相同的角色名称");
                return map;
            }
            roleRepository.save(roleMapper.modelToEntity(role));
            LOGGER.info("addRole success");
            map.put(LogMsg.SUCCESS, true);
            return map;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            LOGGER.error("addRole failed Reason: [{}]", e.getMessage(), e);
            map.put(LogMsg.ERROR_CODE, "1002");
            map.put(LogMsg.MESSAGE, "server inner exception");
            return map;
        }
    }

    @Override
    public Map<String, Object> updateRole(SchoolRoleInfo role) {
        Map<String, Object> map = new HashMap<>();
        map.put(LogMsg.SUCCESS, false);
        if (role == null) {
            map.put(LogMsg.ERROR_CODE, "1001");
            map.put(LogMsg.MESSAGE, "param is empty");
            return map;
        }
        try {
            SchoolRoleEntity entity = roleRepository.findById(role.getId());
            if (entity == null) {
                map.put(LogMsg.ERROR_CODE, "1004");
                map.put(LogMsg.MESSAGE, "find by id is empty");
                return map;
            }
            if (roleExist(role)) {
                map.put(LogMsg.ERROR_CODE, "1003");
                map.put(LogMsg.MESSAGE, "name exist");
                LOGGER.error("updateRole failed: 已存在相同的角色名称");
                return map;
            }
            entity.setRoleName(role.getRoleName());
            entity.setDescription(role.getDescription());
            roleRepository.save(entity);
            List<SchoolLoginEntity> loginEntities = userRepository.findByRoleid(role.getId());
            loginEntities.forEach(userRoleRelationEntity -> {
                userRoleRelationEntity.setRolename(role.getRoleName());
                userRepository.save(userRoleRelationEntity);
            });

            LOGGER.info("updateRole success");
            map.put(LogMsg.SUCCESS, true);
            return map;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            map.put(LogMsg.ERROR_CODE, "1002");
            map.put(LogMsg.MESSAGE, "server inner exception");
            LOGGER.error("updateRole failed Reason: [{}]", e.getMessage(), e);
            return map;
        }
    }

    @Override
    public Boolean deleteRole(String[] ids) {
        if (ids.length > 0) {
            try {
                List<SchoolRoleEntity> list = roleRepository.findByIdIn(ids);
                if (list.size() <= 0) {
                    return false;
                }
                roleRepository.deleteAll(list);
                LOGGER.info("delete success");
                return true;
            } catch (Exception e) {
                LOGGER.error("delete failed Reason: [{}]", e.getMessage(), e);
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return false;
            }
        } else {
            LOGGER.error("deleteRole:param ids is empty");
            return false;
        }
    }

    @Override
    public List<SchoolRoleInfo> getRolesByUserId(String userId) {
        if (StringUtil.isEmpty(userId)) return Collections.emptyList();
        List<SchoolUserRoleRelationEntity> entities = userRoleRelationRepository.findByUserId(userId);
        if (entities.isEmpty()) return Collections.emptyList();
        List<SchoolUserRoleRelationInfo> roleRelations = userRoleRelationMapper.entitiesToModels(entities);
        List<SchoolRoleInfo> list = new ArrayList<>();
        for (SchoolUserRoleRelationInfo roleRelation : roleRelations) {
            SchoolRoleEntity entity = roleRepository.findById(roleRelation.getRoleId());
            if (entity != null)
                list.add(roleMapper.entityToModel(entity));
        }
        return list;
    }

    @Override
    public Boolean getUserRoleRelationByRoleId(String roleId) {
        return userRoleRelationRepository.findByRoleId(roleId).isEmpty();
    }

    private boolean roleExist(SchoolRoleInfo role) {
        int count;
        if (StringUtils.isEmpty(role.getId())) {
            count = roleRepository.getCountByParams(role.getRoleName());
        } else {
            count = roleRepository.getCountByParams(role.getRoleName(), role.getId());
        }
        if (count > 0) {
            LOGGER.error("角色名称：[{}] 已存在！", role.getRoleName());
            return true;
        } else {
            return false;
        }
    }
}
