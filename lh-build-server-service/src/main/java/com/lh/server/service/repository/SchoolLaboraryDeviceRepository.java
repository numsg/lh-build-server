package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolLaboraryDeviceRepository extends JpaRepository<SchoolLaboraryDevice, String> {
    /**
     * Find all list.
     *
     * @return the list
     */
    @Query(value = "select m from SchoolLaboraryDevice m where m.device_group_id = :deviceid")
    List<SchoolLaboraryDevice> findAllListLaboraryDeviceGroup(@Param("deviceid")String deviceid);
}

