package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLoginEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/3/3.
 */
@Repository
public interface LoginRepository extends JpaRepository<SchoolLoginEntity,Long> {

    /**
     * Gets count by params.
     *
     * @param username the username
     * @return the count by params
     */
    @Query(value = "select count(u) from SchoolLoginEntity u where u.username=?1")
    int getCountByParams(String username);


    /**
     * Find by username user entity.
     *
     * @param username the username
     * @return the user entity
     */
    SchoolLoginEntity findByUsername(String username);

    /**
     * Find by username and password user entity.
     *
     * @param username the username
     * @param password the password
     * @return the user entity
     */
    SchoolLoginEntity findByUsernameAndPassword(String username, String password);
}
