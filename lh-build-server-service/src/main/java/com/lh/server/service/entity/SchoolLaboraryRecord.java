package com.lh.server.service.entity;


import javax.persistence.*;
import java.util.Date;

/**
 * 实验室记录
 */
@Entity
@Table(name = "school_laborary_record", schema = "")
public class SchoolLaboraryRecord {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    // 中心编号
    @Column(name = "center_code", length = 120)
    private String centerCode;
    // 中心名称
    @Column(name = "center_name", length = 120)
    private String centerName;
    // 实验室主键
    @Column(name = "laborary_id",length = 36)
    private String laboraryId;
    // 实验室名称
    @Column(name = "laborary_name", length = 120)
    private String laboraryName;
    // 项目数量
    @Column(name = "project_num", columnDefinition = "int default 0")
    private int projectNum;
    // 修改中心编号时间
    @Column(name = "modify_time")
    private Date modifyTime;
    // 修改中心编号人
    @Column(name = "modify_name", length = 120)
    private String modifyName;

    // 提交时间
    @Column(name = "submit_time")
    private Date submitTime;

    // 审核状态（0-未审核，1-审核中，2-已审核,当所有项目都是已审核状态，记录才是已审核状态）
    @Column(name = "verify_result", columnDefinition = "int default 0")
    private int verifyResult;
    // 审核时间
    @Column(name = "verify_time")
    private Date verifyTime;
    // 批准状态0-未批准，1-批准中，2-已批准，当所有项目都是已批准状态，记录才是已批准状态）
    @Column(name = "approval_result",columnDefinition = "int default 0")
    private int approvalResult;
    @Column(name = "approval_time")
    private Date approvalTime;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getLaboraryId() {
        return laboraryId;
    }

    public void setLaboraryId(String laboraryId) {
        this.laboraryId = laboraryId;
    }

    public String getLaboraryName() {
        return laboraryName;
    }

    public void setLaboraryName(String laboraryName) {
        this.laboraryName = laboraryName;
    }

    public int getProjectNum() {
        return projectNum;
    }

    public void setProjectNum(int projectNum) {
        this.projectNum = projectNum;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyName() {
        return modifyName;
    }

    public void setModifyName(String modifyName) {
        this.modifyName = modifyName;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public int getVerifyResult() {
        return verifyResult;
    }

    public void setVerifyResult(int verifyResult) {
        this.verifyResult = verifyResult;
    }

    public int getApprovalResult() {
        return approvalResult;
    }

    public void setApprovalResult(int approvalResult) {
        this.approvalResult = approvalResult;
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }
}
