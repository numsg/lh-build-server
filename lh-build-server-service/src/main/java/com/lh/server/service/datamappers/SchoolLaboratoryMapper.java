package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboratoryInfo;
import com.lh.server.contract.model.SchoolLaboraryDeviceInfo;
import com.lh.server.service.entity.SchoolLaboaryComputer;
import com.lh.server.service.entity.SchoolLaboraryDevice;
import com.lh.server.service.entity.SchoolLaboraryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolLaboratoryMapper {
    /**
     * Entity to model menu.
     *
     * @param entity the entity
     * @return the menu
     */
    SchoolLaboratoryInfo entityToModel(SchoolLaboraryEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolLaboratoryInfo> entitiesToModels(List<SchoolLaboraryEntity> entities);

    /**
     * Model to entity menu entity.
     *
     * @param model the model
     * @return the menu entity
     */
    SchoolLaboraryEntity modelToEntity(SchoolLaboratoryInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolLaboraryEntity> modelsToEntities(List<SchoolLaboratoryInfo> models);
}
