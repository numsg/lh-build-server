package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface School laborary record project repository.
 */
@Repository
public interface SchoolLaboraryRecordProjectRepository  extends JpaRepository<SchoolLaboraryRecordProject,String> {
    /**
     * Find by record id list.
     *
     * @param recordId the record id
     * @return the list
     */
    List<SchoolLaboraryRecordProject> findByRecordId(String recordId);

    /**
     * Find by record id and approval result list.
     *
     * @param recordId the record id
     * @param status   the status
     * @return the list
     */
    List<SchoolLaboraryRecordProject> findByRecordIdAndApprovalResult(String recordId,int status);


    /**
     * Find by record id and verify result list.
     *
     * @param recordId the record id
     * @param status   the status
     * @return the list
     */
    List<SchoolLaboraryRecordProject> findByRecordIdAndVerifyResult(String recordId,int status);


    /**
     * Find by project name list.
     *
     * @param recordId    the record id
     * @param projectName the project name
     * @return the list
     */
    List<SchoolLaboraryRecordProject> findByRecordIdAndProjectName(String recordId ,String projectName);
}
