package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryComputerRelationInfo;
import com.lh.server.contract.model.SchoolUserRoleRelationInfo;
import com.lh.server.service.entity.SchoolLaboaryComputer;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolLaboaryComputerMapper {

    /**
     * Entity to model user role relation.
     *
     * @param entity the entity
     * @return the user role relation
     */
    SchoolLaboraryComputerRelationInfo entityToModel(SchoolLaboaryComputer entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolLaboraryComputerRelationInfo> entitiesToModels(List<SchoolLaboaryComputer> entities);

    /**
     * Model to entity user role relation entity.
     *
     * @param model the model
     * @return the user role relation entity
     */
    SchoolLaboaryComputer modelToEntity(SchoolLaboraryComputerRelationInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolLaboaryComputer> modelsToEntities(List<SchoolLaboraryComputerRelationInfo> models);
}
