package com.lh.server.service.entity;

import javax.persistence.*;
import javax.print.DocFlavor;

/**
 * 实验室电脑
 */
@Entity
@Table(name = "school_laborary_computer", schema = "")
public class SchoolLaboaryComputer {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;

    // 电脑ip
    @Column(name = "computer_ip", length = 255, nullable = false,unique = true)
    private String computer_ip;

    @Column(name = "laborary_id",length = 36, nullable = false,unique = true)
    private String laboraryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getComputer_ip() {
        return computer_ip;
    }

    public void setComputer_ip(String computer_ip) {
        this.computer_ip = computer_ip;
    }

    public String getLaboraryId() {
        return laboraryId;
    }

    public void setLaboraryId(String laboraryId) {
        this.laboraryId = laboraryId;
    }
}
