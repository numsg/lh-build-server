package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolDataRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SchoolDataRecordRepository extends JpaRepository<SchoolDataRecordEntity, String>, JpaSpecificationExecutor<SchoolDataRecordEntity> {
}
