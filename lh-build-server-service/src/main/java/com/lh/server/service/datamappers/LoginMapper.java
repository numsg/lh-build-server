package com.lh.server.service.datamappers;


import com.lh.server.contract.model.SchoolLoginInfo;
import com.lh.server.service.entity.SchoolLoginEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * Created by admin
 */
@Mapper(componentModel = "spring")
public interface LoginMapper {

    SchoolLoginInfo entityToModel(SchoolLoginEntity schoolLoginEntity);

    SchoolLoginEntity modelToEntity(SchoolLoginInfo schoolLoginInfo);

    List<SchoolLoginInfo> entitiestoModels(List<SchoolLoginEntity> schoolLoginEntities);

    List<SchoolLoginEntity> modelstoEntities(List<SchoolLoginInfo> schoolLoginInfos);
}
