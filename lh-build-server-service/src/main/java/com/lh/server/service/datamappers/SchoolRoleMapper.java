package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolRoleInfo;
import com.lh.server.service.entity.SchoolRoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {SchoolBizTypeMapper.class, SchoolRoleMenuRelationMapper.class, SchooleUserRoleRelationMapper.class})
public interface SchoolRoleMapper {
    /**
     * Entity to model role.
     *
     * @param entity the entity
     * @return the role
     */
    @Mapping(target = "roleMenuRelations", source = "roleMenuRelationEntities")
    @Mapping(target = "userRoleRelations", source = "userRoleRelationEntities")
    SchoolRoleInfo entityToModel(SchoolRoleEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolRoleInfo> entitiesToModels(List<SchoolRoleEntity> entities);

    /**
     * Model to entity role entity.
     *
     * @param model the model
     * @return the role entity
     */
    @Mapping(target = "roleMenuRelationEntities", source = "roleMenuRelations")
    @Mapping(target = "userRoleRelationEntities", source = "userRoleRelations")
    SchoolRoleEntity modelToEntity(SchoolRoleInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolRoleEntity> modelsToEntities(List<SchoolRoleInfo> models);
}
