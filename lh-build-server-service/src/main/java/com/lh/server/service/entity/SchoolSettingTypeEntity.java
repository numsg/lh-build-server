package com.lh.server.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "school_setting_type", schema = "")
public class SchoolSettingTypeEntity {
    @Id
    @Column(name = "ID", length = 36, nullable = false)
    private String id;

    /**
     * setting表中的type
     */
    @Column(name = "TYPE_ID")
    private Integer typeId;

    /**
     * type的名称
     */
    @Column(name = "TYPE_NAME", length = 200)
    private String typeName;

    /**
     * 启用
     */
    @Column(name = "ENABLE")
    private Integer enable;

    /**
     * 是否可编辑
     */
    @Column(name = "EDITABLE")
    private Integer editable;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }

}
