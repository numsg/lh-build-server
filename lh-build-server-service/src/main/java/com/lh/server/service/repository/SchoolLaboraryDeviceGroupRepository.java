package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryDeviceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolLaboraryDeviceGroupRepository extends JpaRepository<SchoolLaboraryDeviceGroup, String> {

    /**
     * Find all list.
     *
     * @return the list
     */
    @Query(value = "select m from SchoolLaboraryDeviceGroup m")
    List<SchoolLaboraryDeviceGroup> findAllListLaboraryDeviceGroup();

//    /**
//     * 根据科室ID获取设备集合分组
//     * @param laboraryId 实验室ID
//     * @return 设备集合分组
//     */
//    @Query(value = "select m from SchoolLaboraryDeviceGroup m where m.laborary_id=?1")
//    List<SchoolLaboraryDeviceGroup> findByLaboraryid(String laboraryId);

    /**
     * 根据科室ID获取设备集合分组
     * @param laboraryId 实验室ID
     * @return 设备集合分组
     */
    @Query(value = "select m from SchoolLaboraryDeviceGroup m")
    List<SchoolLaboraryDeviceGroup> findByLaboraryid(String laboraryId);
}

