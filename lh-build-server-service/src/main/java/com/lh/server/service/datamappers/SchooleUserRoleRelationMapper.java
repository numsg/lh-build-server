package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolUserRoleRelationInfo;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {SchoolLoginEntity.class, SchoolRoleMapper.class})
public interface SchooleUserRoleRelationMapper {
    /**
     * Entity to model user role relation.
     *
     * @param entity the entity
     * @return the user role relation
     */
    SchoolUserRoleRelationInfo entityToModel(SchoolUserRoleRelationEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolUserRoleRelationInfo> entitiesToModels(List<SchoolUserRoleRelationEntity> entities);

    /**
     * Model to entity user role relation entity.
     *
     * @param model the model
     * @return the user role relation entity
     */
    SchoolUserRoleRelationEntity modelToEntity(SchoolUserRoleRelationInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolUserRoleRelationEntity> modelsToEntities(List<SchoolUserRoleRelationInfo> models);
}
