package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolLaboratoryManagerRepository extends JpaRepository<SchoolLaboraryEntity, String>, JpaSpecificationExecutor<SchoolLaboraryEntity> {
    List<SchoolLaboraryEntity> findAll();

    SchoolLaboraryEntity save(SchoolLaboraryEntity schoolLaboraryEntity);

    List<SchoolLaboraryEntity>  findBySchoolLaboaryComputerIpsContaining(String schoolLaboaryComputerIps);
}
