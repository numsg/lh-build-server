package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryResultTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface School laborary result template repository.
 */
@Repository
public interface SchoolLaboraryResultTemplateRepository extends JpaRepository<SchoolLaboraryResultTemplate,String> {
}
