package com.lh.server.service.serviceimpl;


import com.lh.server.contract.model.SchoolLoginInfo;
import com.lh.server.contract.service.LoginService;
import com.lh.server.service.datamappers.LoginMapper;
import com.lh.server.service.entity.SchoolLoginEntity;
import com.lh.server.service.repository.LoginRepository;
import com.lh.server.common.constant.LogMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/3.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class LoginServiceImpl implements LoginService {

    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    LoginMapper loginMapper;

    @Autowired
    SchoolRoleServiceImpl schoolRoleService;


    /**
     * 查询所有用户
     *
     * @return 包含所有用户的集合
     */
    @Override
    public List<SchoolLoginInfo> findAll() {
        // 如果找不到数据,默认返回空集合
        logger.info("begin fandUserInfo method findAll");
        List<SchoolLoginEntity> userEntityList = loginRepository.findAll();
        logger.info("end fandUserInfo method findAll");
        return loginMapper.entitiestoModels(userEntityList);
    }

    /**
     * 登录验证
     *
     * @param schoolLoginInfo
     * @return
     */
    @Override
    public Map<String, Object> login(SchoolLoginInfo schoolLoginInfo) {
        Map<String, Object> map = new HashMap<>();
        // 1.用户验证
        SchoolLoginEntity schoolLoginEntity = loginRepository.findByUsernameAndPassword(schoolLoginInfo.getUsername(), schoolLoginInfo.getPassword());
        if (schoolLoginEntity == null) {
            map.put(LogMsg.USERNAME_NOT_EXIST, false);
        } else {
            map.put(LogMsg.SUCCESS, true);
            // 2. 角色数据获取
            schoolLoginInfo.setRoles(schoolRoleService.getRolesByUserId(schoolLoginEntity.getUserId()));
            schoolLoginInfo.setUserId(schoolLoginEntity.getUserId());

            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            String ip = request.getRemoteAddr();
            schoolLoginInfo.setStrIp(ip);

            map.put(LogMsg.MESSAGE, schoolLoginInfo);
        }
        return map;
    }
}
