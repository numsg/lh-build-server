package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolLaboraryLogInfo;
import com.lh.server.contract.service.SchoolLaboraryLogService;
import com.lh.server.service.datamappers.SchoolLaboraryLogMapper;
import com.lh.server.service.entity.SchoolLaboraryLog;
import com.lh.server.service.repository.SchoolLaboraryLogRepository;
import com.lh.server.service.utils.SpecificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SchoolLaboraryLogServiceImpl implements SchoolLaboraryLogService {

    @Autowired
    private SchoolLaboraryLogRepository schoolLaboraryLogRepository;

    @Autowired
    private SchoolLaboraryLogMapper schoolLaboraryLogMapper;

    /**
     * 日志保存
     * @param schoolLaboraryLogInfo  前端需要传递centercode  projectname  proojectid opeationpage日志记录信息
     * @return
     */
    @Override
    public boolean addLaboraryLog(SchoolLaboraryLogInfo schoolLaboraryLogInfo) {
        SchoolLaboraryLog schoolLaboraryLog = schoolLaboraryLogMapper.modelToEntity(schoolLaboraryLogInfo);
        schoolLaboraryLog.setId(UUID.randomUUID().toString());

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String uri = request.getRequestURI();
        String ip = request.getRemoteAddr();
        schoolLaboraryLog.setOperationip(ip);
//        schoolLaboraryLog.setOperationdesc(uri);
        schoolLaboraryLog.setOperationtime(new Date());
        schoolLaboraryLogRepository.save(schoolLaboraryLog);
        return true;
    }

    /**
     * 分页获取日志记录
     * @param pageNum
     * @param size
     * @param keyword
     * @return
     */
    @Override
    public PageInfo<SchoolLaboraryLogInfo> getLogByPage(int pageNum, int size, String keyword) {
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        Pageable pageable = PageRequest.of(pageNum,size,sort);
        Specification specification = createSpecification(keyword);
        Page<SchoolLaboraryLog> page = schoolLaboraryLogRepository.findAll(specification,pageable);
        List<SchoolLaboraryLogInfo> schoolLaboratoryInfos=schoolLaboraryLogMapper.entitiesToModels(page.getContent());
        PageInfo<SchoolLaboraryLogInfo> pageInfo=new PageInfo<SchoolLaboraryLogInfo>(schoolLaboratoryInfos,page.getTotalElements(),page.getTotalPages());
        return pageInfo;
    }

    private Specification<SchoolLaboraryLog> createSpecification(String keyWord) {
        SpecificationUtil specificationUtil = new SpecificationUtil<SchoolLaboraryLog>();
        return specificationUtil.createSpecification("projectname", keyWord);
    }
}
