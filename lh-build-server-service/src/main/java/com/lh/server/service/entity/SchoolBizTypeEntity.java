package com.lh.server.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "school_biz_type", schema = "")
public class SchoolBizTypeEntity {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private int id;
    /**
     * 业务类型名称
     */
    @Column(name = "name", nullable = false)
    private String name;
    /**
     * 是否启用（0：否，1：是）
     */
    @Column(name = "is_enable")
    private Integer enable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
}
