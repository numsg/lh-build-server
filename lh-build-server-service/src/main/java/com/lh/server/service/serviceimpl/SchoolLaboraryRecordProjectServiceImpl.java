package com.lh.server.service.serviceimpl;

import com.lh.server.common.exception.BusinessException;
import com.lh.server.common.exception.ErrorCode;
import com.lh.server.contract.model.SchoolLaboraryRecordProjectInfo;
import com.lh.server.contract.service.SchoolLaboraryRecordProjectService;
import com.lh.server.service.datamappers.SchoolLaboraryRecordProjectMapper;
import com.lh.server.service.entity.SchoolLaboraryRecord;
import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import com.lh.server.service.repository.SchoolLaboraryRecordProjectRepository;
import com.lh.server.service.repository.SchoolLaboraryRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class SchoolLaboraryRecordProjectServiceImpl implements SchoolLaboraryRecordProjectService {
    @Autowired
    private SchoolLaboraryRecordProjectMapper recordProjectMapper;
    @Autowired
    private SchoolLaboraryRecordProjectRepository recordProjectRepository;
    @Autowired
    private SchoolLaboraryRecordRepository recordRepository;
    /**
     * Gets record projects by record id.
     *
     * @param recordId the record id
     * @return the record projects by record id
     */
    @Override
    public List<SchoolLaboraryRecordProjectInfo> getRecordProjectsByRecordId(String recordId) {
        return recordProjectMapper.entitiesToModels(recordProjectRepository.findByRecordId(recordId));
    }

    /**
     * Update one record project school laborary record project info.
     *
     * @param id                the id
     * @param recordProjectInfo the record project info
     * @return the school laborary record project info
     */
    @Override
    public SchoolLaboraryRecordProjectInfo updateOneRecordProject(String id, SchoolLaboraryRecordProjectInfo recordProjectInfo) {
        if(!id.equals(recordProjectInfo.getId())||!recordProjectRepository.existsById(id)){
             throw new BusinessException(ErrorCode.DATA_NOT_FOUND);
        }
        SchoolLaboraryRecordProject recordProject = recordProjectMapper.modelToEntity(recordProjectInfo);
        SchoolLaboraryRecordProjectInfo result =recordProjectMapper.entityToModel(recordProjectRepository.save(recordProject));
        recordProjectRepository.flush();

        // 如果记录下的所有项目都已批准或者已审核，变更记录的状态也为已批准或者已审核
        SchoolLaboraryRecord record = recordRepository.getOne(recordProject.getRecordId());
        List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordId(result.getRecordId());
        List<SchoolLaboraryRecordProject> approvalRecordProjects = recordProjectRepository.findByRecordIdAndApprovalResult(result.getRecordId(), 2);
        List<SchoolLaboraryRecordProject> verifyRecordProjects = recordProjectRepository.findByRecordIdAndVerifyResult(result.getRecordId(),2);

        if(!recordProjects.isEmpty() && recordProjects.size()==approvalRecordProjects.size()){
            // 已批准
            record.setVerifyTime(new Date());
            record.setApprovalResult(1);
        }else{
            // 未批准
            record.setApprovalResult(0);
        }

        if(!recordProjects.isEmpty() && recordProjects.size()==verifyRecordProjects.size()){
            // 已审核
            if(recordProjectInfo.getVerifyResult() == 2 && recordProjectInfo.getApprovalResult() != 2){
                record.setApprovalTime(new Date());
                record.setVerifyResult(1);
            }
        }else{
            // 未审核
            record.setVerifyResult(0);
        }

        recordRepository.save(record);
        recordRepository.flush();
        return result;
    }

    /**
     * Delete one record project boolean.
     *
     * @param id the id
     * @return the boolean
     */
    @Override
    public Boolean deleteOneRecordProject(String id) {
        boolean result = false;
        SchoolLaboraryRecordProject schoolLaboraryRecordProject = recordProjectRepository.findById(id).get();
        SchoolLaboraryRecord schoolLaboraryRecord = recordRepository.findById(schoolLaboraryRecordProject.getRecordId()).get() ;
        schoolLaboraryRecord.setProjectNum(schoolLaboraryRecord.getProjectNum()-1);
        recordRepository.save(schoolLaboraryRecord);
        if(recordProjectRepository.existsById(id)){
            recordProjectRepository.deleteById(id);
            result = true;
        }

        // 如果记录下的所有项目都已批准或者已审核，变更记录的状态也为已批准或者已审核
        SchoolLaboraryRecord record = recordRepository.getOne(schoolLaboraryRecordProject.getRecordId());
        List<SchoolLaboraryRecordProject> recordProjects = recordProjectRepository.findByRecordId(schoolLaboraryRecordProject.getRecordId());

        List<SchoolLaboraryRecordProject> approvalRecordProjects = recordProjectRepository.findByRecordIdAndApprovalResult(schoolLaboraryRecordProject.getRecordId(), 2);
        List<SchoolLaboraryRecordProject> approvalRecordProjectsSort = approvalRecordProjects.stream().sorted(Comparator.comparing(SchoolLaboraryRecordProject::getApprovalTime).reversed()).collect(Collectors.toList());

        List<SchoolLaboraryRecordProject> verifyRecordProjects = recordProjectRepository.findByRecordIdAndVerifyResult(schoolLaboraryRecordProject.getRecordId(),2);
        List<SchoolLaboraryRecordProject> verifyRecordProjectsSort = verifyRecordProjects.stream().sorted(Comparator.comparing(SchoolLaboraryRecordProject::getVerifyTime).reversed()).collect(Collectors.toList());

        if(!recordProjects.isEmpty() && recordProjects.size()==approvalRecordProjects.size()){
            // 已批准
            record.setVerifyTime(approvalRecordProjectsSort.get(0).getApprovalTime());
            record.setApprovalResult(1);
        }else{
            // 未批准
            record.setApprovalResult(0);
        }

        if(!recordProjects.isEmpty() && recordProjects.size()==verifyRecordProjects.size()){
            // 已审核
            record.setApprovalTime(verifyRecordProjectsSort.get(0).getVerifyTime());
            record.setVerifyResult(1);
        }else{
            // 未审核
            record.setVerifyResult(0);
        }

        recordRepository.save(record);
        recordRepository.flush();
        return result;
    }
}
