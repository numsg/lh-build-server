package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolLaboraryLogRepository extends JpaRepository<SchoolLaboraryLog, String> , JpaSpecificationExecutor<SchoolLaboraryLog> {
    List<SchoolLaboraryLog> findAll();
}

