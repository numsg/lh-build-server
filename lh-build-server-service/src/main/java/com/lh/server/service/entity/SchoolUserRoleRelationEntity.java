package com.lh.server.service.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "school_user_role", schema = "")
public class SchoolUserRoleRelationEntity {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    /**
     * 用户ID
     */
    @Column(name = "user_id", length = 36)
    private String userId;
    /**
     * 角色ID
     */
    @Column(name = "role_id", length = 36)
    private String roleId;
    /**
     * 外键关联用户ID
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private SchoolLoginEntity userEntity;
    /**
     * 外键关联角色ID
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private SchoolRoleEntity roleEntity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public SchoolLoginEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(SchoolLoginEntity userEntity) {
        this.userEntity = userEntity;
    }

    public SchoolRoleEntity getRoleEntity() {
        return roleEntity;
    }

    public void setRoleEntity(SchoolRoleEntity roleEntity) {
        this.roleEntity = roleEntity;
    }
}
