package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryDeviceGroupInfo;
import com.lh.server.service.entity.SchoolLaboraryDeviceGroup;
import org.mapstruct.Mapper;

import java.util.List;

// 实验设备分组映射
@Mapper(componentModel = "spring")
public interface SchoolLaboraryDeviceGroupMapper {
    /**
     * Entity to model menu.
     *
     * @param entity the entity
     * @return the menu
     */
    SchoolLaboraryDeviceGroupInfo entityToModel(SchoolLaboraryDeviceGroup entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolLaboraryDeviceGroupInfo> entitiesToModels(List<SchoolLaboraryDeviceGroup> entities);

    /**
     * Model to entity menu entity.
     *
     * @param model the model
     * @return the menu entity
     */
    SchoolLaboraryDeviceGroup modelToEntity(SchoolLaboraryDeviceGroupInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolLaboraryDeviceGroup> modelsToEntities(List<SchoolLaboraryDeviceGroupInfo> models);
}
