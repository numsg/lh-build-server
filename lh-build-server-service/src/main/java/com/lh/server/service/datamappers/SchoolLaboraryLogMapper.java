package com.lh.server.service.datamappers;


import com.lh.server.contract.model.SchoolLaboraryLogInfo;
import com.lh.server.service.entity.SchoolLaboraryLog;
import org.mapstruct.Mapper;

import java.util.List;

// 日志映射
@Mapper(componentModel = "spring")
public interface SchoolLaboraryLogMapper {
    /**
     * Entity to model menu.
     *
     * @param entity the entity
     * @return the menu
     */
    SchoolLaboraryLogInfo entityToModel(SchoolLaboraryLog entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolLaboraryLogInfo> entitiesToModels(List<SchoolLaboraryLog> entities);

    /**
     * Model to entity menu entity.
     *
     * @param model the model
     * @return the menu entity
     */
    SchoolLaboraryLog modelToEntity(SchoolLaboraryLogInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolLaboraryLog> modelsToEntities(List<SchoolLaboraryLogInfo> models);
}
