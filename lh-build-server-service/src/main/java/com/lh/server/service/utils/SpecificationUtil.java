package com.lh.server.service.utils;

import com.lh.server.service.entity.SchoolLaboraryEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public  class SpecificationUtil<T> {
   public Specification<T> createSpecification(String filedName, String keyWords) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            assembleLikePredicates(predicates, root, criteriaBuilder, filedName, keyWords);
            query.distinct(true);
            return  criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    private void  assembleLikePredicates(List<Predicate> predicates, Root<T> root, CriteriaBuilder criteriaBuilder, String fieldName, String fieldValue) {
        if (!StringUtils.isEmpty(fieldValue)) {
            predicates.add(criteriaBuilder.like(root.get(fieldName), "%" + fieldValue + "%"));
        }
    }
}
