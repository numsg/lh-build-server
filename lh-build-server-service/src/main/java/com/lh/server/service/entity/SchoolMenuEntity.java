package com.lh.server.service.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "school_menu", schema = "")
public class SchoolMenuEntity {
    /**
     * 主键Id
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    /**
     * 编码
     */
    @Column(name = "code", nullable = false, length = 36)
    private String code;
    /**
     * 菜单名
     */
    @Column(name = "name", length = 128, nullable = false)
    private String name;
    /**
     * 描述
     */
    @Column(name = "description")
    private String description;
    /**
     * 父级ID
     */
    @Column(name = "parent_id", length = 36)
    private String parentId;
    /**
     * 菜单路由
     */
    @Column(name = "url", length = 128, nullable = false)
    private String url;
    /**
     * 菜单标题
     */
    @Column(name = "title", length = 128, nullable = false)
    private String title;
    /**
     * 菜单图标
     */
    @Column(name = "icon", length = 256)
    private String icon;
    /**
     * 菜单排序
     */
    @Column(name = "menu_order")
    private Integer order;
    /**
     * 是否启用(0：否，1：是)
     */
    @Column(name = "available")
    private Integer available;
    /**
     * 菜单层级
     */
    @Column(name = "menu_level")
    private Integer level;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
