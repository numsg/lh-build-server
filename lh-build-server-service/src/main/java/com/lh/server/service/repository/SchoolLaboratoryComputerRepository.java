package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboaryComputer;
import com.lh.server.service.entity.SchoolLaboraryLog;
import com.lh.server.service.entity.SchoolMenuEntity;
import com.lh.server.service.entity.SchoolUserRoleRelationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SchoolLaboratoryComputerRepository extends JpaRepository<SchoolLaboaryComputer, String> {
//    @Modifying
//    @Query(value = "select m from SchoolLaboaryComputer m where m.laborary_id = ?1")
//   List<SchoolLaboaryComputer>  getByLaboraryId(String  laboraryId);

    /**
     * Find by role id list.
     *
     * @param laboraryId the role id
     * @return the list
     */
    List<SchoolLaboaryComputer> findBylaboraryId(String laboraryId);
}
