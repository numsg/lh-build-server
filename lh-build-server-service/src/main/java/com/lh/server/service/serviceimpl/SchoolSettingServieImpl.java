package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.SchoolSettingInfo;
import com.lh.server.contract.model.SchoolSettingTypeInfo;
import com.lh.server.contract.service.SchoolSettingService;
import com.lh.server.service.datamappers.SchoolSettingMapper;
import com.lh.server.service.datamappers.SchoolSettingTypeMapper;
import com.lh.server.service.entity.SchoolSettingEntity;
import com.lh.server.service.entity.SchoolSettingTypeEntity;
import com.lh.server.service.repository.SchoolSettingRepository;
import com.lh.server.service.repository.SchoolSettingTypeRepository;
import com.lh.server.common.constant.LogMsg;
import com.lh.server.common.util.BeanUtil;
import com.lh.server.common.util.CheckUtils;
import com.lh.server.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class SchoolSettingServieImpl implements SchoolSettingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolSettingServieImpl.class);

    @Autowired
    private SchoolSettingMapper settingMapper;
    @Autowired
    private SchoolSettingRepository settingRepository;
    @Autowired
    private SchoolSettingTypeRepository settingTypeRepository;
    @Autowired
    private SchoolSettingTypeMapper settingTypeMapper;

    @Override
    public Map<String, Object> save(SchoolSettingInfo settingInfo) {
        CheckUtils.checkNotNull(settingInfo);
        CheckUtils.checkNotEmpty(
                settingInfo.getName()
        );
        Map<String, Object> map = new HashMap<>();
        map.put(LogMsg.SUCCESS, false);
        if (nameExist(settingInfo)) {
            map.put(LogMsg.ERROR_CODE, "1003");
            map.put(LogMsg.MESSAGE, LogMsg.NAME_EXIST);
            LOGGER.error("saveSetting failed: 已存在相同的配置名称");
            return map;
        }
        settingInfo.setSettingId(UUID.randomUUID().toString());
        SchoolSettingEntity entity = settingRepository.save(settingMapper.modelToEntity(settingInfo));
        LOGGER.info("saveSettingType success");
        map.put(LogMsg.SUCCESS, true);
        map.put("settingInfo", settingMapper.entityToModel(entity));
        return map;
    }

    @Override
    public List<SchoolSettingInfo> findAll() {
        // 如果找不到数据,默认返回空集合
        List<SchoolSettingEntity> settingEntities = settingRepository.findAll();
        return settingMapper.entitiesToModels(settingEntities);
    }

    @Override
    public SchoolSettingInfo findById(String id) {
        // 根据ID查找用户,如果找不到则返回null
        SchoolSettingEntity settingEntity = settingRepository.QuerySchoolSettingEntity(id);
        return settingMapper.entityToModel(settingEntity);
    }

    @Override
    public Boolean deleteById(String id) {
        if (StringUtil.isNotEmpty(id)) {
            try {
                SchoolSettingEntity settingEntity = settingRepository.findById(id).orElse(null);
                if (settingEntity == null) {
                    return false;
                }
                settingRepository.deleteById(id);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Boolean deleteTypeById(String id) {
        SchoolSettingTypeEntity entity = settingTypeRepository.findById(id).orElse(null);
        if (entity != null) {
            try {
                settingRepository.deleteAll(settingRepository.findByParamType(entity.getTypeId()));
                settingTypeRepository.delete(entity);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Map<String, Object> update(SchoolSettingInfo settingInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put(LogMsg.SUCCESS, false);
        if (nameExist(settingInfo)) {
            map.put(LogMsg.ERROR_CODE, "1003");
            map.put(LogMsg.MESSAGE, LogMsg.NAME_EXIST);
            LOGGER.error("updateSetting failed: 已存在相同的配置名称");
            return map;
        }
        SchoolSettingInfo targetSettingInfo = findById(settingInfo.getSettingId());
        CheckUtils.checkExist(targetSettingInfo);
        BeanUtils.copyProperties(settingInfo, targetSettingInfo, BeanUtil.getNullPropertyNames(settingInfo));
        SchoolSettingEntity settingEntity = settingMapper.modelToEntity(targetSettingInfo);

        SchoolSettingEntity entity = settingRepository.saveAndFlush(settingEntity);
        LOGGER.info("updateSettingType success");
        map.put(LogMsg.SUCCESS, true);
        map.put("settingInfo", settingMapper.entityToModel(entity));
        return map;
    }

    /**
     * 根据参数类型查找配置项集合
     *
     * @param paramType 配置参数类型
     * @return 配置项集合
     */
    @Override
    public List<SchoolSettingInfo> findEnableByParamType(Integer paramType) {
        SchoolSettingTypeEntity entity = settingTypeRepository.findByTypeId(paramType);
        if (entity == null || entity.getEnable() == 0) return Collections.emptyList();
        List<SchoolSettingEntity> settingEntities = settingRepository.findByParamType(paramType);
        return settingMapper.entitiesToModels(settingEntities);
    }

    @Override
    public List<SchoolSettingInfo> findByParamType(Integer paramType) {
        List<SchoolSettingEntity> settingEntities = settingRepository.findByParamType(paramType);
        return settingMapper.entitiesToModels(settingEntities);
    }


    @Override
    public List<SchoolSettingInfo> findByParamTypes(List<Integer> paramTypes) {
        List<SchoolSettingEntity> settingEntities = settingRepository.findByParamTypeList(paramTypes);
        return settingMapper.entitiesToModels(settingEntities);
    }

    @Override
    public List<SchoolSettingInfo> findBySettingName(String settingName) {
        List<SchoolSettingEntity> settingEntities = settingRepository.findByName(settingName);
        return settingMapper.entitiesToModels(settingEntities);
    }

    /**
     * 查询所有设置的type名称
     *
     * @return
     */
    @Override
    public List<SchoolSettingTypeInfo> findAllSettingType() {
        return settingTypeMapper.entitiesToModels(settingTypeRepository.findAll());
    }

    /**
     * 保存设置类型
     *
     * @param settingTypeInfo
     * @return
     */
    @Override
    public Map<String, Object> saveSettingType(SchoolSettingTypeInfo settingTypeInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put(LogMsg.SUCCESS, false);
        if (settingTypeInfo == null) {
            map.put(LogMsg.ERROR_CODE, "1001");
            map.put(LogMsg.MESSAGE, "param is empty");
            return map;
        }

        if (typeNameExist(settingTypeInfo)) {
            map.put(LogMsg.ERROR_CODE, "1003");
            map.put(LogMsg.MESSAGE, LogMsg.NAME_EXIST);
            LOGGER.error("saveSettingType failed: 已存在相同的名称");
            return map;
        }
        if (typeIdExist(settingTypeInfo)) {
            map.put(LogMsg.ERROR_CODE, "1005");
            map.put(LogMsg.MESSAGE, "code exist");
            LOGGER.error("saveSettingType failed: 已存在相同的编码");
            return map;
        }

        // 新增
        if (StringUtil.isEmpty(settingTypeInfo.getId())) {
            settingTypeInfo.setId(UUID.randomUUID().toString());
            List<SchoolSettingTypeEntity> list = settingTypeRepository.QuerySettingCountNumber();
            if (list.isEmpty()) {
                settingTypeInfo.setTypeId(1);
            } else {
                settingTypeInfo.setTypeId(settingTypeRepository.findMaxCode() + 1);
            }
        }
        SchoolSettingTypeEntity entity = settingTypeRepository.save(settingTypeMapper.modelToEntity(settingTypeInfo));
        LOGGER.info("saveSettingType success");
        map.put(LogMsg.SUCCESS, true);
        map.put("settingType", settingTypeMapper.entityToModel(entity));
        return map;
    }

    private boolean nameExist(SchoolSettingInfo settingInfo) {
        int count;
        if (StringUtils.isEmpty(settingInfo.getSettingId())) {
            count = settingRepository.getCountByParams(settingInfo.getName());
        } else {
            count = settingRepository.getCountByParams(settingInfo.getName(), settingInfo.getSettingId());
        }
        if (count > 0) {
            LOGGER.error("配置名称：[{}] 已存在！", settingInfo.getName());
            return true;
        } else {
            return false;
        }
    }

    private boolean typeNameExist(SchoolSettingTypeInfo settingTypeInfo) {
        int count;
        if (StringUtils.isEmpty(settingTypeInfo.getId())) {
            count = settingTypeRepository.getCountByNameParams(settingTypeInfo.getTypeName());
        } else {
            count = settingTypeRepository.getCountByNameParams(settingTypeInfo.getTypeName(), settingTypeInfo.getId());
        }
        if (count > 0) {
            LOGGER.error("类型名称：[{}] 已存在！", settingTypeInfo.getTypeName());
            return true;
        } else {
            return false;
        }
    }

    private boolean typeIdExist(SchoolSettingTypeInfo settingTypeInfo) {
        int count;
        if (StringUtils.isEmpty(settingTypeInfo.getId())) {
            count = settingTypeRepository.getCountByCodeParams(settingTypeInfo.getTypeId());
        } else {
            count = settingTypeRepository.getCountByCodeParams(settingTypeInfo.getTypeId(), settingTypeInfo.getId());
        }
        if (count > 0) {
            LOGGER.error("类型编码：[{}] 已存在！", settingTypeInfo.getTypeId());
            return true;
        } else {
            return false;
        }
    }
}
