package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolLaboraryRecordProjectInfo;
import com.lh.server.service.entity.SchoolLaboraryRecordProject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * The interface School laborary record project mapper.
 */
@Mapper(componentModel = "spring")
public interface SchoolLaboraryRecordProjectMapper {
    /**
     * Model to entity school laborary record project.
     *
     * @param recordProjectInfo the record project info
     * @return the school laborary record project
     */
    @Mapping(source = "recordId",target = "schoolLaboraryRecord.id")
    SchoolLaboraryRecordProject modelToEntity(SchoolLaboraryRecordProjectInfo recordProjectInfo);

    /**
     * Entity to model school laborary record project info.
     *
     * @param recordProject the record project
     * @return the school laborary record project info
     */
    SchoolLaboraryRecordProjectInfo entityToModel(SchoolLaboraryRecordProject recordProject);

    /**
     * Models to entities list.
     *
     * @param recordProjectInfos the record project infos
     * @return the list
     */
    List<SchoolLaboraryRecordProject> modelsToEntities(List<SchoolLaboraryRecordProjectInfo> recordProjectInfos);

    /**
     * Entities to models list.
     *
     * @param recordProjects the record projects
     * @return the list
     */
    List<SchoolLaboraryRecordProjectInfo> entitiesToModels( List<SchoolLaboraryRecordProject> recordProjects);
}
