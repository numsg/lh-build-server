package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolSettingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolSettingRepository extends JpaRepository<SchoolSettingEntity, String> {

    /**
     * 通过参数类型获取设置
     *
     * @param paramType
     * @return
     */
    @Query("select c from SchoolSettingEntity c where c.paramType=:paramType")
    List<SchoolSettingEntity> findByParamType(@Param("paramType") Integer paramType);

    /**
     * 通过参数类型获取设置
     *
     * @param paramTypeList
     * @return
     */
    @Query("select c from SchoolSettingEntity c where c.paramType in :paramTypeList")
    List<SchoolSettingEntity> findByParamTypeList(@Param("paramTypeList") List<Integer> paramTypeList);

    @Query("select c from SchoolSettingEntity c where c.Name=:settingName")
    List<SchoolSettingEntity> findByName(@Param("settingName") String settingName);

    @Query(value = "select count(s) from SchoolSettingEntity s where s.Name=:name")
    int getCountByParams(@Param("name") String name);

    @Query(value = "select count(s) from SchoolSettingEntity s where s.Name=:name and s.SettingId<>:id")
    int getCountByParams(@Param("name") String name, @Param("id") String id);

    @Query(value = "select s from SchoolSettingEntity s where s.SettingId=:id")
    SchoolSettingEntity QuerySchoolSettingEntity( @Param("id") String id);
}
