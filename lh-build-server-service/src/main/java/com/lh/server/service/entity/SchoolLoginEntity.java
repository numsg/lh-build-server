package com.lh.server.service.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2017/3/3.
 */
@Entity
@Table(name = "school_user", schema = "")
public class SchoolLoginEntity {
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String userId;
    // 账号
    @Column(name = "username", length = 36, nullable = false)
    private String username;
    // 密码
    @Column(name = "password", length = 120)
    private String password;
    // 所属人
    @Column(name = "name", length = 120)
    private String name;
    // 角色ID
    @Column(name = "roleid", length = 36)
    private String roleid;
    // 角色名
    @Column(name = "rolename", length = 120)
    private String rolename;
    // 账户状态
    @Column(name = "status")
    private int status;
    // 创建时间
    @Column(name = "createtime")
    private Date createtime;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }
}
