package com.lh.server.service.serviceimpl;

import com.lh.server.contract.model.SchoolMenuInfo;
import com.lh.server.contract.model.SchoolRoleMenuRelationInfo;
import com.lh.server.contract.service.SchoolMenuService;
import com.lh.server.service.datamappers.SchoolMenuMapper;
import com.lh.server.service.datamappers.SchoolRoleMenuRelationMapper;
import com.lh.server.service.entity.SchoolMenuEntity;
import com.lh.server.service.entity.SchoolRoleMenuRelationEntity;
import com.lh.server.service.repository.SchoolMenuRepository;
import com.lh.server.service.repository.SchoolRoleMenuRelationRepository;
import com.lh.server.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SchoolMenuServiceImpl implements SchoolMenuService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolMenuServiceImpl.class);

    @Autowired
    private SchoolMenuRepository menuRepository;

    @Autowired
    private SchoolMenuMapper menuMapper;

    @Autowired
    private SchoolRoleMenuRelationRepository roleMenuRelationRepository;

    @Autowired
    private SchoolRoleMenuRelationMapper roleMenuRelationMapper;

    @Override
    public List<SchoolMenuInfo> getAllMenu() {
        List<SchoolMenuEntity> entities = menuRepository.findAllEnableMenu();
        if (CollectionUtils.isEmpty(entities)) return Collections.emptyList();
        return menuMapper.entitiesToModels(entities);
    }

    @Override
    public List<SchoolMenuInfo> getMenuTree() {
        List<SchoolMenuEntity> menuEntityList = menuRepository.findAllEnableMenu();
        List<SchoolMenuInfo> rootMenu = new ArrayList<>();
        List<SchoolMenuInfo> menus = menuMapper.entitiesToModels(menuEntityList);
        return convertTreeList(rootMenu, menus);
    }

    private List<SchoolMenuInfo> convertTreeList(List<SchoolMenuInfo> treeList, List<SchoolMenuInfo> source) {
        TreeMap<String, Object> map = new TreeMap<>();
        if (!CollectionUtils.isEmpty(source)) {
            for (SchoolMenuInfo menu : source) {
                map.put(menu.getId(), menu);
            }
            for (SchoolMenuInfo menu : source) {
                if (!StringUtil.isEmpty(menu.getParentId())) {
                    SchoolMenuInfo m = (SchoolMenuInfo) map.get(menu.getParentId());
                    if (m != null) {
                        List<SchoolMenuInfo> children = m.getChildren();
                        children.add(menu);
                        map.put(menu.getParentId(), m);
                    }
                } else {
                    treeList.add(menu);
                }
            }
        }
        for (SchoolMenuInfo menu : treeList) {
            menu.setChildren(menu.getChildren().stream().sorted(Comparator.comparing(SchoolMenuInfo::getOrder)).collect(Collectors.toList()));
        }
        return treeList.stream().sorted(Comparator.comparing(SchoolMenuInfo::getOrder)).collect(Collectors.toList());
    }

    @Override
    public List<SchoolMenuInfo> getTreeMenuByRoleId(String roleId) {
        List<SchoolMenuInfo> menus = getMenu(roleId);
        List<SchoolMenuInfo> rootMenu = new ArrayList<>();
        return convertTreeList(rootMenu, menus);
    }

    @Override
    public List<SchoolMenuInfo> getMenuByRoleId(String roleId) {
        return getMenu(roleId);
    }

    private List<SchoolMenuInfo> getMenu(String roleId) {
        List<SchoolRoleMenuRelationEntity> entities = roleMenuRelationRepository.findByRoleId(roleId);
        if (CollectionUtils.isEmpty(entities)) return Collections.emptyList();
        List<SchoolRoleMenuRelationInfo> roleMenuRelations = roleMenuRelationMapper.entitiesToModels(entities);
        List<SchoolMenuInfo> menus = new ArrayList<>();
        for (SchoolRoleMenuRelationInfo relation : roleMenuRelations) {
            Optional<SchoolMenuEntity> entity = menuRepository.findById(relation.getMenuId());
            if (entity.isPresent()) {
                SchoolMenuEntity menuEntity = entity.get();
                if (menuEntity.getAvailable() == 1) menus.add(menuMapper.entityToModel(menuEntity));
            }
        }
        return menus;
    }


    @Override
    public boolean deleteRelationsByRoleId(String roleId) {
        if (StringUtil.isEmpty(roleId)) return false;
        try {
            roleMenuRelationRepository.deleteByRoleId(roleId);
            return true;
        } catch (Exception e) {
            LOGGER.error("deleteRelationsByRoleId exception: [{}]", e.getMessage(), e);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Override
    public boolean configRoleMenus(List<SchoolRoleMenuRelationInfo> roleMenuRelations) {
        if (CollectionUtils.isEmpty(roleMenuRelations))
            return false;
        // 1.删除角色菜单关联
        List<SchoolRoleMenuRelationEntity> existRelationEntities = roleMenuRelationRepository.findByRoleId(roleMenuRelations.get(0).getRoleId());
        roleMenuRelationRepository.deleteAll(existRelationEntities);
        List<SchoolRoleMenuRelationEntity> entities = roleMenuRelationMapper.modelsToEntities(roleMenuRelations);
        roleMenuRelationRepository.saveAll(entities);
        return true;
    }

    @Override
    public boolean addMenu(SchoolMenuInfo menu) {
        menuRepository.save(menuMapper.modelToEntity(menu));
        return true;
    }
}
