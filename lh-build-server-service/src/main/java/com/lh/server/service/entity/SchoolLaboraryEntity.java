package com.lh.server.service.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 实验室管理
 */
@Entity
@Table(name = "school_laborary", schema = "")
public class SchoolLaboraryEntity {

    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    // 实验室名称
    @Column(name = "laborary_name", length = 120,unique = true)
    private String laborary_name;
    // 权重
    @Column(name = "weight", columnDefinition = "int default 0")
    private int weight;
    // 说明
    @Column(name = "laborary_desc", length = 1200)
    private String laborary_desc;
    // 创建时间
    @Column(name = "createtime", length = 120)
    private Date createtime;

    public String getSchoolLaboaryComputerIps() {
        return schoolLaboaryComputerIps;
    }

    public void setSchoolLaboaryComputerIps(String schoolLaboaryComputerIps) {
        this.schoolLaboaryComputerIps = schoolLaboaryComputerIps;
    }

    // 实验室ip
    @Column(name = "laborary_computerIps", length = 4800)
    private  String schoolLaboaryComputerIps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLaborary_name() {
        return laborary_name;
    }

    public void setLaborary_name(String laborary_name) {
        this.laborary_name = laborary_name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getLaborary_desc() {
        return laborary_desc;
    }

    public void setLaborary_desc(String laborary_desc) {
        this.laborary_desc = laborary_desc;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }


}
