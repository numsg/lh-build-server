package com.lh.server.service.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "school_role", schema = "")
public class SchoolRoleEntity {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", length = 36, nullable = false)
    private String id;

    /**
     * 角色名称
     */
    @Column(name = "role_name", length = 256, unique = true, nullable = false)
    private String roleName;

    /**
     * 角色编码
     */
    @Column(name = "code")
    private int code;

    /**
     * 外键关联业务类型ID
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "code", insertable = false, updatable = false)
    private SchoolBizTypeEntity bizTypeEntity;

    /**
     * 角色描述
     */
    @Column(name = "description", length = 512)
    private String description;

    /**
     * 是否可删除（0：否，1：是）
     */
    @Column(name = "deletable")
    private Integer deletable = 1;

    /**
     * 角色——菜单关联关系
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private List<SchoolRoleMenuRelationEntity> roleMenuRelationEntities;

    /**
     * 用户——角色关联关系
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private List<SchoolUserRoleRelationEntity> userRoleRelationEntities;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Integer getDeletable() {
        return deletable;
    }

    public void setDeletable(Integer deletable) {
        this.deletable = deletable;
    }

    public SchoolBizTypeEntity getBizTypeEntity() {
        return bizTypeEntity;
    }

    public void setBizTypeEntity(SchoolBizTypeEntity bizTypeEntity) {
        this.bizTypeEntity = bizTypeEntity;
    }

    public List<SchoolRoleMenuRelationEntity> getRoleMenuRelationEntities() {
        return roleMenuRelationEntities;
    }

    public void setRoleMenuRelationEntities(List<SchoolRoleMenuRelationEntity> roleMenuRelationEntities) {
        this.roleMenuRelationEntities = roleMenuRelationEntities;
    }

    public List<SchoolUserRoleRelationEntity> getUserRoleRelationEntities() {
        return userRoleRelationEntities;
    }

    public void setUserRoleRelationEntities(List<SchoolUserRoleRelationEntity> userRoleRelationEntities) {
        this.userRoleRelationEntities = userRoleRelationEntities;
    }
}
