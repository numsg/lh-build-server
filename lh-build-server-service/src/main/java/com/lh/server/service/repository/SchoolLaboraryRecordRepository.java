package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolLaboraryRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface School laborary record repository.
 */
@Repository
public interface SchoolLaboraryRecordRepository extends JpaRepository<SchoolLaboraryRecord,String>, JpaSpecificationExecutor<SchoolLaboraryRecord> {

    /**
     * Exists by center code boolean.
     *
     * @param centerCode the center code
     * @return the boolean
     */
    Boolean existsByCenterCode(String centerCode);

    /**
     * Find by center code list.
     *
     * @param centerCode the center code
     * @return the list
     */
    List<SchoolLaboraryRecord> findByCenterCode(String centerCode);


    /**
     * Find by center code and l and laborary id list.
     *
     * @param centerCode the center code
     * @param laboraryId the laborary id
     * @return the list
     */
    List<SchoolLaboraryRecord> findByCenterCodeAndLaboraryId(String centerCode,String laboraryId);
}
