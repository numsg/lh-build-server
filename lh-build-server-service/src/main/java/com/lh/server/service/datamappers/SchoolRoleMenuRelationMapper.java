package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolRoleMenuRelationInfo;
import com.lh.server.service.entity.SchoolRoleMenuRelationEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {SchoolRoleMapper.class, SchoolMenuMapper.class})
public interface SchoolRoleMenuRelationMapper {
    /**
     * Entity to model role menu relation.
     *
     * @param entity the entity
     * @return the role menu relation
     */
    SchoolRoleMenuRelationInfo entityToModel(SchoolRoleMenuRelationEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolRoleMenuRelationInfo> entitiesToModels(List<SchoolRoleMenuRelationEntity> entities);

    /**
     * Model to entity role menu relation entity.
     *
     * @param model the model
     * @return the role menu relation entity
     */
    SchoolRoleMenuRelationEntity modelToEntity(SchoolRoleMenuRelationInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolRoleMenuRelationEntity> modelsToEntities(List<SchoolRoleMenuRelationInfo> models);
}
