package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolDataRecordInfo;
import com.lh.server.service.entity.SchoolDataRecordEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolDataRecordMapper {
    /**
     * Entity to model biz type.
     *
     * @param entity the entity
     * @return the biz type
     */
    SchoolDataRecordInfo entityToModel(SchoolDataRecordEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolDataRecordInfo> entitiesToModels(List<SchoolDataRecordEntity> entities);

    /**
     * Model to entity biz type entity.
     *
     * @param model the model
     * @return the biz type entity
     */
    SchoolDataRecordEntity modelToEntity(SchoolDataRecordInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolDataRecordEntity> modelsToEntities(List<SchoolDataRecordInfo> models);
}
