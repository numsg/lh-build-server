package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolMenuInfo;
import com.lh.server.service.entity.SchoolMenuEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolMenuMapper {
    /**
     * Entity to model menu.
     *
     * @param entity the entity
     * @return the menu
     */
    SchoolMenuInfo entityToModel(SchoolMenuEntity entity);

    /**
     * Entities to models list.
     *
     * @param entities the entities
     * @return the list
     */
    List<SchoolMenuInfo> entitiesToModels(List<SchoolMenuEntity> entities);

    /**
     * Model to entity menu entity.
     *
     * @param model the model
     * @return the menu entity
     */
    SchoolMenuEntity modelToEntity(SchoolMenuInfo model);

    /**
     * Models to entities list.
     *
     * @param models the models
     * @return the list
     */
    List<SchoolMenuEntity> modelsToEntities(List<SchoolMenuInfo> models);
}
