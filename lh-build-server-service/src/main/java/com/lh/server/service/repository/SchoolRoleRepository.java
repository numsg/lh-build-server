package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolRoleRepository extends JpaRepository<SchoolRoleEntity, Integer> {
    /**
     * Gets count by params.
     *
     * @param roleName the role name
     * @return the count by params
     */
    @Query(value = "select count(r) from SchoolRoleEntity r where  r.roleName = ?1")
    int getCountByParams(String roleName);

    /**
     * Gets count by params.
     *
     * @param roleName the role name
     * @param id       the id
     * @return the count by params
     */
    @Query(value = "select count(r) from SchoolRoleEntity r where  r.roleName = ?1 and r.id <> ?2")
    int getCountByParams(String roleName, String id);

    /**
     * Delete by ids int.
     *
     * @param ids the ids
     * @return the int
     */
    @Modifying
    @Query(value = "delete from SchoolRoleEntity where id in (?1)")
    int deleteByIds(String[] ids);

    /**
     * Find by id role entity.
     *
     * @param id the id
     * @return the role entity
     */
    SchoolRoleEntity findById(String id);

    /**
     * Find by code role entity.
     *
     * @param code the code
     * @return the role entity
     */
    SchoolRoleEntity findByCode(Integer code);

    /**
     * Find all by code list.
     *
     * @param code the code
     * @return the list
     */
    List<SchoolRoleEntity> findAllByCode(Integer code);

    /**
     * Find max code int.
     *
     * @return the int
     */
    @Query(value = "select max (code) from SchoolRoleEntity ")
    int findMaxCode();

    /**
     * Find by id in list.
     *
     * @param ids the ids
     * @return the list
     */
    List<SchoolRoleEntity> findByIdIn(String[] ids);
}