package com.lh.server.service.entity;

import javax.persistence.*;

@Entity
@Table(name = "school_setting", schema = "")
public class SchoolSettingEntity {
    /**
     * 参数Id
     */
    @Id
    @Column(name = "ID", length = 36, nullable = false)
    private String SettingId;

    /**
     * 之前节点是枚举，新增一个字段保持一致不是枚举，保证兼容性（以后会修改）
     */
    @Column(name = "TYPE")
    private Integer paramType;

    /**
     * 名称
     */
    @Column(name = "NAME", length = 128, nullable = false)
    private String Name;

    /**
     * value
     */
    @Column(name = "VALUE", length = 512)
    private String Value;

    /**
     * 默认值
     */
    @Column(name = "default_value", length = 512)
    private String defaultValue;

    @Column(name = "description")
    private String description;

    /**
     * 并发乐观锁标识列
     */
    @Version
    private int RowVersion;

    public String getSettingId() {
        return SettingId;
    }

    public void setSettingId(String settingId) {
        SettingId = settingId;
    }

    public Integer getParamType() {
        return paramType;
    }

    public void setParamType(Integer paramType) {
        this.paramType = paramType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(int rowVersion) {
        RowVersion = rowVersion;
    }
}
