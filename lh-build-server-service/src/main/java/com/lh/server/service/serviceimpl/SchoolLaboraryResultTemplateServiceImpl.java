package com.lh.server.service.serviceimpl;

import com.lh.server.common.exception.BusinessException;
import com.lh.server.common.exception.ErrorCode;
import com.lh.server.contract.model.SchoolLaboraryResultTemplateInfo;
import com.lh.server.contract.service.SchoolLaboraryResultTemplateService;
import com.lh.server.service.datamappers.SchoolLaboraryResultTemplateMapper;
import com.lh.server.service.entity.SchoolLaboraryResultTemplate;
import com.lh.server.service.repository.SchoolLaboraryResultTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SchoolLaboraryResultTemplateServiceImpl implements SchoolLaboraryResultTemplateService {

    @Autowired
    private SchoolLaboraryResultTemplateMapper resultTemplateMapper;

    @Autowired
    private SchoolLaboraryResultTemplateRepository resultTemplateRepository;

    /**
     * Gets all result templates.
     *
     * @return the all result templates
     */
    @Override
    public List<SchoolLaboraryResultTemplateInfo> getAllResultTemplates() {
        List<SchoolLaboraryResultTemplate> resultTemplates =resultTemplateRepository.findAll();
        return resultTemplateMapper.entitiesToModels(resultTemplates);
    }

    /**
     * Gets one result template by id.
     *
     * @param id the id
     * @return the one result template by id
     */
    @Override
    public SchoolLaboraryResultTemplateInfo getOneResultTemplateById(String id) {
        if(!resultTemplateRepository.existsById(id)){
            return  new SchoolLaboraryResultTemplateInfo();
        }
        return resultTemplateMapper.entityToModel(resultTemplateRepository.getOne(id));
    }

    /**
     * Add one result template school laborary result template info.
     *
     * @param resultTemplateInfo the result template info
     * @return the school laborary result template info
     */
    @Override
    public SchoolLaboraryResultTemplateInfo addOneResultTemplate(SchoolLaboraryResultTemplateInfo resultTemplateInfo) {
        SchoolLaboraryResultTemplate resultTemplate = resultTemplateMapper.modelToEntity(resultTemplateInfo);
        return resultTemplateMapper.entityToModel(resultTemplateRepository.save(resultTemplate));
    }

    /**
     * Update one result template school laborary result template info.
     *
     * @param id                 the id
     * @param resultTemplateInfo the result template info
     * @return the school laborary result template info
     */
    @Override
    public SchoolLaboraryResultTemplateInfo updateOneResultTemplate(String id, SchoolLaboraryResultTemplateInfo resultTemplateInfo) {
        if(!id.equals(resultTemplateInfo.getId())|| !resultTemplateRepository.existsById(id)){
            throw new BusinessException(ErrorCode.DATA_NOT_FOUND);
        }
        return addOneResultTemplate(resultTemplateInfo);
    }

    /**
     * Delete one result template boolean.
     *
     * @param id the id
     * @return the boolean
     */
    @Override
    public Boolean deleteOneResultTemplate(String id) {
        boolean result = false;
        if(resultTemplateRepository.existsById(id)){
            resultTemplateRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
