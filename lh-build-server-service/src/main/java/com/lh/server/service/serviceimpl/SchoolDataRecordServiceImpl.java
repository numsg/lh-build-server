package com.lh.server.service.serviceimpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lh.server.common.util.DateUtil;
import com.lh.server.contract.model.PageInfo;
import com.lh.server.contract.model.SchoolDataRecordInfo;
import com.lh.server.contract.service.SchoolDataRecordService;
import com.lh.server.service.datamappers.SchoolDataRecordMapper;
import com.lh.server.service.entity.SchoolDataRecordEntity;
import com.lh.server.service.repository.SchoolDataRecordRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class SchoolDataRecordServiceImpl implements SchoolDataRecordService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolDataRecordServiceImpl.class);

    @Autowired
    private SchoolDataRecordRepository schoolDataRecordRepository;
    @Autowired
    private SchoolDataRecordMapper schoolDataRecordMapper;

    @Override
    public Boolean save(SchoolDataRecordInfo dataRecordInfo) {
        if (dataRecordInfo.getDatatype() != 1 && dataRecordInfo.getDatatype() != 2 && dataRecordInfo.getDatatype() != 3 && dataRecordInfo.getDatatype() != 4) {
            return false; //TODO 后续优化
        }
        if (dataRecordInfo.getData().equals("")) {
            return false; //TODO 后续优化
        }
        try {
            JSONArray jsonArray = JSONArray.parseArray(dataRecordInfo.getData());
            jsonArray.forEach((s) -> {
                SchoolDataRecordEntity schoolDataRecordEntity = schoolDataRecordMapper.modelToEntity(dataRecordInfo);
                schoolDataRecordEntity.setDataid(UUID.randomUUID().toString());
                JSONObject jsonObject = (JSONObject) s;
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    int dataType = Integer.parseInt(jsonObject.get("type").toString());
//                    String sno = jsonObject.get("tag").toString();
                    Date createTime = sdf.parse(jsonObject.get("time").toString());
                    schoolDataRecordEntity.setDatatype(dataType);
//                    schoolDataRecordEntity.setSno(sno);
//                    schoolDataRecordEntity.setDatatime(createTime);new Date()
                    schoolDataRecordEntity.setDatatime(new Date());

                } catch (ParseException e) {
                    LOGGER.error("SchoolDataRecordServiceImpl  save ex  {}", e.toString());
                    e.printStackTrace();
                }
                schoolDataRecordEntity.setData(s.toString());
                schoolDataRecordRepository.save(schoolDataRecordEntity);
            });
            schoolDataRecordRepository.flush();
            return true;
        } catch (Exception e) {
            LOGGER.error("SchoolDataRecordServiceImpl  save ex  {}", e.toString());
            return false;
        }

    }

    @Override
    public String getByDeviceId(String deviceId) {
        Sort sort = new Sort(Sort.Direction.DESC, "datatime");
        SchoolDataRecordEntity schoolDataRecordEntity = new SchoolDataRecordEntity();
        schoolDataRecordEntity.setDeviceId(deviceId);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("deviceId", ExampleMatcher.GenericPropertyMatchers.exact())//模糊查询匹配开头，即{username}%
                .withIgnorePaths("datatype");//忽略字段，即不管datatype是什么值都不加入查询条件
        List<SchoolDataRecordEntity> entities = schoolDataRecordRepository.findAll(Example.of(schoolDataRecordEntity, matcher), sort);
        if (!CollectionUtils.isEmpty(entities)) {
            return entities.get(0).getSno();
        }
        return "";
    }

    @Override
    public List<SchoolDataRecordInfo> getAllSchoolDataRecordInfo() {
        List<SchoolDataRecordEntity> entities = schoolDataRecordRepository.findAll();
        if (CollectionUtils.isEmpty(entities)) return Collections.emptyList();
        return schoolDataRecordMapper.entitiesToModels(entities);
    }

    @Override
    public PageInfo<SchoolDataRecordInfo> getLatestSchoolDataRecordInfoList(int dataType, int pageNum, int size, String deviceId) {
        Sort sort = new Sort(Sort.Direction.DESC, "datatime");//TODO 后续修改成datetime
        Pageable pageable = PageRequest.of(pageNum, size, sort);
        Specification specification = createSpecification(dataType, deviceId);
        Page<SchoolDataRecordEntity> page = schoolDataRecordRepository.findAll(specification, pageable);
        List<SchoolDataRecordInfo> schoolDataRecordInfos = schoolDataRecordMapper.entitiesToModels(page.getContent());
        PageInfo<SchoolDataRecordInfo> pageInfo = new PageInfo<SchoolDataRecordInfo>(schoolDataRecordInfos, page.getTotalElements(), page.getTotalPages());
        return pageInfo;
    }

    @Override
    public boolean updateDataRecords(List<SchoolDataRecordInfo> schoolDataRecordInfos) {
        List<SchoolDataRecordEntity> entities = schoolDataRecordMapper.modelsToEntities(schoolDataRecordInfos);
        if (CollectionUtils.isEmpty(entities)) return false;
        entities.forEach((schoolDataRecordEntity -> {
            schoolDataRecordRepository.save(schoolDataRecordEntity);
        }));
        return true;
    }

    private Specification<SchoolDataRecordEntity> createSpecification(int dataType, String deviceId) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("datatype").as(Integer.class), dataType));
            predicates.add(criteriaBuilder.equal(root.get("deviceId").as(String.class), deviceId));
            predicates.add(criteriaBuilder.equal(root.get("status").as(Integer.class), 0));
            predicates.add(criteriaBuilder.between(root.get("datatime").as(Date.class), DateUtil.getStartTime(), DateUtil.getEndTime()));
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }


}
