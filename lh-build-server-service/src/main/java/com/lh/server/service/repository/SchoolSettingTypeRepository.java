package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolSettingTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolSettingTypeRepository extends JpaRepository<SchoolSettingTypeEntity, String> {
    @Query(value = "select count(s) from SchoolSettingTypeEntity s where s.typeName=:name")
    int getCountByNameParams(@Param("name") String name);

    @Query(value = "select count(s) from SchoolSettingTypeEntity s where s.typeName=:name and s.id<>:id")
    int getCountByNameParams(@Param("name") String name, @Param("id") String id);

    @Query(value = "select count(s) from SchoolSettingTypeEntity s where s.typeId=:typeId")
    int getCountByCodeParams(@Param("typeId") Integer typeId);

    @Query(value = "select count(s) from SchoolSettingTypeEntity s where s.typeId=:typeId and s.id<>:id")
    int getCountByCodeParams(@Param("typeId") Integer typeId, @Param("id") String id);

    @Query(value = "select max (typeId) from SchoolSettingTypeEntity ")
    int findMaxCode();

    SchoolSettingTypeEntity findByTypeId(Integer typeId);

    @Query(value = "select c from SchoolSettingTypeEntity c")
    List<SchoolSettingTypeEntity> QuerySettingCountNumber();
}