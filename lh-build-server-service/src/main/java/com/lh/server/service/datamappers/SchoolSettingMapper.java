package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolSettingInfo;
import com.lh.server.service.entity.SchoolSettingEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolSettingMapper {
    /**
     * Entity to model setting info.
     *
     * @param settingEntity the setting entity
     * @return the setting info
     */
    SchoolSettingInfo entityToModel(SchoolSettingEntity settingEntity);

    /**
     * Model to entity setting entity.
     *
     * @param settingInfo the setting info
     * @return the setting entity
     */
    SchoolSettingEntity modelToEntity(SchoolSettingInfo settingInfo);

    /**
     * Entities to models list.
     *
     * @param settingEntity the setting entity
     * @return the list
     */
    List<SchoolSettingInfo> entitiesToModels(List<SchoolSettingEntity> settingEntity);

    /**
     * Models to entities list.
     *
     * @param settingEntity the setting entity
     * @return the list
     */
    List<SchoolSettingEntity> modelsToEntities(List<SchoolSettingInfo> settingEntity);
}
