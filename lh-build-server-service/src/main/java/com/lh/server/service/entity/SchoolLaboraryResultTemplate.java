package com.lh.server.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 实验室项目结果模板
 */
@Entity
@Table(name = "school_laborary_result_template", schema = "")
public class SchoolLaboraryResultTemplate {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    // 名称
    @Column(name = "result_name", length = 120)
    private String resultName;
    // 权重
    @Column(name = "weight", columnDefinition = "int default 0")
    private int weight;
    // 状态
    @Column(name = "result_status", length = 120)
    private String resultStatus;
    // 描述
    @Column(name = "result_desc", length = 1200)
    private String resultDesc;
    // 创建时间
    @Column(name = "create_time")
    private Date createTime;
    // 模板内容
    @Column(name = "content")
    private String content;
    // 模板数据
    @Column(name = "data")
    private String data;

    // 模板对应的word模板
//    @Column(name = "wordurl", length = 120)
//    private String wordurl;
    // 对应项目类型
    @Column(name = "project_type", length = 120)
    private String projectType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
