package com.lh.server.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 日志表
 */
@Entity
@Table(name = "school_laborary_log", schema = "")
public class SchoolLaboraryLog {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    /**
     * 中心编号
     */
    @Column(name = "centercode", length = 120)
    private String centercode;
    /**
     * 项目名称
     */
    @Column(name = "projectname", length = 120)
    private String projectname;

    /**
     * 项目Id
     */
    @Column(name = "proojectid", length = 36)
    private String proojectid;
    /**
     * 操作页面
     */
    @Column(name = "opeationpage", length = 120)
    private String opeationpage;
    /**
     * 操作描述
     */
    @Column(name = "operationdesc", length = 1200)
    private String operationdesc;
    /**
     * 操作IP
     */
    @Column(name = "operationip", length = 120)
    private String operationip;
    /**
     * 操作时间
     */
    @Column(name = "operationtime")
    private Date operationtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCentercode() {
        return centercode;
    }

    public void setCentercode(String centercode) {
        this.centercode = centercode;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getOpeationpage() {
        return opeationpage;
    }

    public void setOpeationpage(String opeationpage) {
        this.opeationpage = opeationpage;
    }

    public String getOperationdesc() {
        return operationdesc;
    }

    public void setOperationdesc(String operationdesc) {
        this.operationdesc = operationdesc;
    }

    public String getOperationip() {
        return operationip;
    }

    public void setOperationip(String operationip) {
        this.operationip = operationip;
    }

    public Date getOperationtime() {
        return operationtime;
    }

    public void setOperationtime(Date operationtime) {
        this.operationtime = operationtime;
    }

    public String getProojectid() {
        return proojectid;
    }

    public void setProojectid(String proojectid) {
        this.proojectid = proojectid;
    }
}
