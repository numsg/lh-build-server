package com.lh.server.service.datamappers;

import com.lh.server.contract.model.SchoolSettingTypeInfo;
import com.lh.server.service.entity.SchoolSettingTypeEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolSettingTypeMapper {
    /**
     * Entity to model setting type info.
     *
     * @param settingEntity the setting entity
     * @return the setting type info
     */
    SchoolSettingTypeInfo entityToModel(SchoolSettingTypeEntity settingEntity);

    /**
     * Model to entity setting type entity.
     *
     * @param settingInfo the setting info
     * @return the setting type entity
     */
    SchoolSettingTypeEntity modelToEntity(SchoolSettingTypeInfo settingInfo);

    /**
     * Entities to models list.
     *
     * @param settingEntity the setting entity
     * @return the list
     */
    List<SchoolSettingTypeInfo> entitiesToModels(List<SchoolSettingTypeEntity> settingEntity);

    /**
     * Models to entities list.
     *
     * @param settingEntity the setting entity
     * @return the list
     */
    List<SchoolSettingTypeEntity> modelsToEntities(List<SchoolSettingTypeInfo> settingEntity);
}
