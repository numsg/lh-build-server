package com.lh.server.service.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 实验室设备管理
 */
@Entity
@Table(name = "school_laborary_device", schema = "")
public class SchoolLaboraryDevice {
    /**
     * 主键
     */
    @Id
    @Column(name = "id", length = 36, nullable = false)
    private String id;
    // 监控名
    @Column(name = "monitor_name", length = 120)
    private String monitor_name;
    // 权重
    @Column(name = "weight", columnDefinition = "int default 0")
    private int weight;
    // 监控编号
    @Column(name = "monitor_num", length = 120)
    private String monitor_num;
    // 监控IP
    @Column(name = "monitor_ip", length = 120)
    private String monitor_ip;
    // 监控账户
    @Column(name = "monitor_username", length = 120)
    private String monitor_username;
    // 监控密码
    @Column(name = "monitor_password", length = 120)
    private String monitor_password;
    // 流媒体地址
    @Column(name = "monitor_url", length = 120)
    private String monitor_url;
    // 创建时间
    @Column(name = "createtime")
    private Date createtime;
    //  设备组主键ID
    @Column(name = "device_group_id", length = 36)
    private String device_group_id;


    public String getMonitor_name() {
        return monitor_name;
    }

    public void setMonitor_name(String monitor_name) {
        this.monitor_name = monitor_name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getMonitor_num() {
        return monitor_num;
    }

    public void setMonitor_num(String monitor_num) {
        this.monitor_num = monitor_num;
    }

    public String getMonitor_ip() {
        return monitor_ip;
    }

    public void setMonitor_ip(String monitor_ip) {
        this.monitor_ip = monitor_ip;
    }

    public String getMonitor_username() {
        return monitor_username;
    }

    public void setMonitor_username(String monitor_username) {
        this.monitor_username = monitor_username;
    }

    public String getMonitor_password() {
        return monitor_password;
    }

    public void setMonitor_password(String monitor_password) {
        this.monitor_password = monitor_password;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getMonitor_url() {
        return monitor_url;
    }

    public void setMonitor_url(String monitor_url) {
        this.monitor_url = monitor_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice_group_id() {
        return device_group_id;
    }

    public void setDevice_group_id(String device_group_id) {
        this.device_group_id = device_group_id;
    }
}
