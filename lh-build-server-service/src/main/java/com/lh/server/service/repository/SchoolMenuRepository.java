package com.lh.server.service.repository;

import com.lh.server.service.entity.SchoolMenuEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolMenuRepository extends JpaRepository<SchoolMenuEntity, String> {
    /**
     * Find all enable menu list.
     *
     * @return the list
     */
    @Query(value = "select m from SchoolMenuEntity m where m.available =1 ")
    List<SchoolMenuEntity> findAllEnableMenu();

    /**
     * Gets menu by parent ids.
     *
     * @param parentIds the parent ids
     * @return the menu by parent ids
     */
    @Query(value = "select m from SchoolMenuEntity m where m.parentId in (?1)")
    List<SchoolMenuEntity> getMenuByParentIds(String[] parentIds);
}
